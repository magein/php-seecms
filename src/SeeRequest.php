<?php

namespace magein\seecms;

class SeeRequest
{
    /**
     * @var SeeConfig|null
     */
    protected static $instance = null;

    /**
     * @var array
     */
    protected $params = [];

    protected function __construct()
    {

    }

    protected function __clone()
    {

    }

    /**
     * @return SeeRequest
     */
    public static function instance(): SeeRequest
    {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * @param $method
     * @param $params
     * @return void
     */
    public function set($method, $params)
    {
        $this->params[strtolower($method)] = $params;
    }

    /**
     * @param $name
     * @param $default
     * @return array|mixed|null
     */
    public function get($name = null, $default = null)
    {
        $data = $this->params['get'] ?? [];

        if (!is_null($name)) {
            return $data[$name] ?? $default;
        }

        return $data;
    }

    /**
     * @param $name
     * @param $default
     * @return array|mixed|null
     */
    public function post($name = null, $default = null)
    {
        $data = $this->params['post'] ?? [];

        if (!is_null($name)) {
            return $data[$name] ?? $default;
        }

        return $data;
    }

    /**
     * @param $name
     * @param $default
     * @return array|mixed|null
     */
    public function delete($name = null, $default = null)
    {
        $data = $this->params['delete'] ?? [];

        if (!is_null($name)) {
            return $data[$name] ?? $default;
        }

        return $data;
    }

    /**
     * @param $name
     * @param $default
     * @return array|mixed|null
     */
    public function patch($name = null, $default = null)
    {
        $data = $this->params['patch'] ?? [];

        if (!is_null($name)) {
            return $data[$name] ?? $default;
        }

        return $data;
    }

    /**
     * @param $name
     * @param $default
     * @return array|mixed|null
     */
    public function put($name = null, $default = null)
    {
        $data = $this->params['put'] ?? [];

        if (!is_null($name)) {
            return $data[$name] ?? $default;
        }

        return $data;
    }

    /**
     * @param $name
     * @param $default
     * @return array|mixed|null
     */
    public function param($name, $default = null)
    {
        return $this->get($name, $default) ?: $this->post($name, $default);
    }

    public function isAjax(): bool
    {
        $value = $_SERVER['HTTP_X_REQUESTED_WITH'] ?? '';
        return $value && 'xmlhttprequest' == strtolower($value);
    }
}
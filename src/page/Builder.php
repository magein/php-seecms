<?php

namespace magein\seecms\page;

use magein\seecms\page\unit\column\ActionColumn;
use magein\seecms\page\unit\control\InputControl;
use magein\seecms\page\unit\modal\Dialog;

/**
 * @user magein
 * @description 用户控制页面版本公共变量信息
 */
class Builder
{

    /**
     * 页面名称
     * 用于取调度器中取出构建页面参数
     * 在页面服务提供者中根据此值获取
     * @var string
     */
    protected $page_name = '';

    /**
     * 数据库表名称
     * @var string
     */
    protected $table_name = '';

    /**
     * 页面标题
     * @var string
     */
    protected $title = '';

    /**
     * 页面提醒
     * @var array
     */
    protected $alerts = [];

    /**
     * 左侧工具栏按钮
     * @var array
     */
    protected $left_toolbars = ['create', 'del'];

    /**
     * 右侧工具栏按钮
     * @var array
     */
    protected $right_toolbars = ['filter', 'print', 'exports'];

    /**
     * 搜索字段
     * @var array
     */
    protected $queries = [];

    /**
     * 表格字段
     * @var array
     */
    protected $columns = [];

    /**
     * 表格中的操作按钮
     * @var array
     */
    protected $actions = ['read', 'edit', 'del'];

    /**
     * @var Dialog|null
     */
    protected $dialog = null;

    /**
     * 表单控件
     * @var array
     */
    protected $controls = [];

    /**
     * 展示的字段信息
     * @var array
     */
    protected $shows = [];

    /**
     * 创建的表单控件
     * @var array
     */
    protected $creates = [];

    /**
     * 字典文件
     * @var array|string
     */
    protected $dictionary = null;

    /**
     * @var bool
     */
    protected $tree = false;


    /**
     * @param string $title
     * @return $this
     */
    public function setTitle(string $title): Builder
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title ?: '';
    }

    /**
     * 获取表名称
     * @return string
     */
    public function getTableName(): string
    {
        return $this->table_name ?: '';
    }

    /**
     * @return string
     */
    public function getPageName(): string
    {
        return $this->page_name;
    }

    /**
     * @param string $page_name
     * @return $this
     */
    public function setPageName(string $page_name): Builder
    {
        $this->page_name = $page_name;

        return $this;
    }

    /**
     * @param array|string $dictionary
     * @return $this
     */
    public function setDictionary($dictionary): Builder
    {
        $this->dictionary = $dictionary;

        return $this;
    }

    /**
     * @return array|string
     */
    public function getDictionary()
    {
        return $this->dictionary;
    }

    /**
     * @param $alerts
     * @return $this
     */
    public function setAlerts($alerts): Builder
    {
        $this->alerts = $alerts;

        return $this;
    }

    /**
     * @return array
     */
    public function getAlerts(): array
    {
        return $this->alerts;
    }

    /**
     * @param array $query
     * @return $this
     */
    public function setQueries(array $query): Builder
    {
        $this->queries = $query;

        return $this;
    }

    /**
     * @return array
     */
    public function getQueries(): array
    {
        return $this->queries;
    }

    /**
     * @param array $columns
     * @return $this
     */
    public function setColumns(array $columns): Builder
    {
        $this->columns = $columns;

        return $this;
    }

    /**
     * @return array
     */
    public function getColumns(): array
    {
        return $this->columns;
    }

    /**
     * @return array|mixed
     */
    public function getActionColumns()
    {
        $columns = $this->getColumns();

        if ($this->getActions()) {
            $columns[] = ActionColumn::init($this->getActions());
        }

        return $columns;
    }

    /**
     * @param array $actions
     * @return $this
     */
    public function setActions(array $actions): Builder
    {
        $this->actions = $actions;

        return $this;
    }

    /**
     * @return array
     */
    public function getActions(): array
    {
        return $this->actions;
    }

    /**
     * @param array $toolbars
     * @return $this
     */
    public function setLeftToolbars(array $toolbars): Builder
    {
        $this->left_toolbars = $toolbars;

        return $this;
    }

    /**
     * @return array
     */
    public function getLeftToolbars(): array
    {
        return $this->left_toolbars;
    }

    /**
     * @param array $toolbars
     * @return $this
     */
    public function setRightToolbars(array $toolbars): Builder
    {
        $this->right_toolbars = $toolbars;

        return $this;
    }

    /**
     * @return array|string[]
     */
    public function getRightToolbars(): array
    {
        return $this->right_toolbars;
    }

    /**
     * @param array $controls
     * @return $this
     */
    public function setControls(array $controls): Builder
    {
        $this->controls = $controls;

        return $this;
    }

    /**
     * @return array
     */
    public function getControls(): array
    {
        return $this->controls;
    }

    /**
     * @param array $controls
     * @return $this
     */
    public function setCreates(array $controls): Builder
    {
        $this->creates = $controls;

        return $this;
    }

    /**
     * @return array
     */
    public function getCreates(): array
    {
        return $this->creates ?: $this->getControls();
    }

    /**
     * @param array $controls
     * @return $this
     */
    public function setShows(array $controls): Builder
    {
        $this->shows = $controls;

        return $this;
    }

    /**
     * @return array
     */
    public function getShows(): array
    {
        $shows = $this->shows ?: $this->getControls();
        $shows[] = InputControl::init('created_at');
        $shows[] = InputControl::init('updated_at');
        return $shows;
    }

    /**
     * @return bool
     */
    public function isTree(): bool
    {
        return $this->tree;
    }

    /**
     * @param bool $tree
     * @return void
     */
    public function setTree(bool $tree): void
    {
        $this->tree = $tree;
    }

    /**
     * @param Dialog $dialog
     * @return $this
     */
    public function setDialog(Dialog $dialog): Builder
    {
        $this->dialog = $dialog;

        return $this;
    }

    /**
     * @return Dialog
     */
    public function getDialog(): Dialog
    {
        return ($this->dialog instanceof Dialog) ? $this->dialog : Dialog::init();
    }

    public function event(): Event
    {
        return new Event();
    }
}
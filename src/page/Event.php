<?php

namespace magein\seecms\page;


use magein\seecms\page\event\Hook;

/**
 * @property Hook $read
 * @property Hook $index
 * @property Hook $insert
 * @property Hook $update
 * @property Hook $save
 * @property Hook $patch
 * @property Hook $delete
 */
class Event
{
    public function __set($name, $callback)
    {
        $this->$name = $callback;
    }

    public function __get($name)
    {
        $this->$name = new Hook();

        return $this->$name;
    }
}
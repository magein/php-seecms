<?php

declare(strict_types=1);

namespace magein\seecms\page;

use magein\seecms\Factory;
use magein\seecms\page\unit\alert\Alert;
use magein\seecms\page\unit\alert\DangerAlert;
use magein\seecms\page\unit\button\Button;
use magein\seecms\page\unit\button\DeleteButton;
use magein\seecms\page\unit\button\EditButton;
use magein\seecms\page\unit\button\PageButton;
use magein\seecms\page\unit\button\ReadButton;
use magein\seecms\page\unit\column\Column;
use magein\seecms\page\unit\column\ImageColumn;
use magein\seecms\page\unit\column\SwitchColumn;
use magein\seecms\page\unit\column\TextColumn;
use magein\seecms\page\unit\control\Control;
use magein\seecms\page\unit\control\DatetimeControl;
use magein\seecms\page\unit\control\FileControl;
use magein\seecms\page\unit\control\HiddenControl;
use magein\seecms\page\unit\control\ImageControl;
use magein\seecms\page\unit\control\InputControl;
use magein\seecms\page\unit\control\PasswordControl;
use magein\seecms\page\unit\control\RadioControl;
use magein\seecms\page\unit\query\DateQuery;
use magein\seecms\page\unit\query\Query;
use magein\seecms\page\unit\query\TextQuery;
use magein\seecms\page\unit\toolbar\CreateToolbar;
use magein\seecms\page\unit\toolbar\DeleteToolbar;
use magein\seecms\page\unit\toolbar\NormalToolbar;
use magein\seecms\SeeLang;

class Parse
{
    protected $dictionary = [];

    protected $btn = [];

    public function __construct($dictionary = null)
    {
        if ($dictionary) {
            $this->setDictionary($dictionary);
            $this->btn = SeeLang::instance()->getGroup('btn');
        }
    }

    public static function global($dictionary = null): Parse
    {
        return new self(SeeLang::instance()->global($dictionary));
    }

    /**
     * 设置字典
     * @param $dictionary
     * @return $this
     */
    public function setDictionary($dictionary): Parse
    {
        if (!is_array($dictionary)) {
            $dictionary = [];
        }

        $this->dictionary = $dictionary;

        return $this;
    }

    /**
     * 转化label
     * @param string $name
     * @return mixed|string
     */
    public function transferLabel(string $name)
    {
        $text = $this->dictionary[$name] ?? '';
        if (empty($text) && preg_match('/_text$/', $name)) {
            $name = substr($name, 0, -5);
            $text = $this->dictionary[$name] ?? '';
        }
        return $text;
    }

    /**
     * @param $queries
     * @return Query[]
     */
    public function query($queries): array
    {
        $data = [];

        if (empty($queries)) {
            return $data;
        }

        $enter = SeeLang::instance()->get('enter');
        $select = SeeLang::instance()->get('select');

        foreach ($queries as $field) {
            if (is_string($field)) {
                if ($field === 'created_at') {
                    $query = new DateQuery();
                } else {
                    $query = new TextQuery();
                }
                $query->setName($field);
            } else {
                $query = $field;
            }

            if (!$query instanceof Query) {
                continue;
            }

            $label = $this->transferLabel($query->getName());
            $query->setLabel($label);

            if (empty($query->getPlaceholder())) {
                if ($query->getType() === 'select') {
                    $placeholder = $select . $label;
                } else {
                    $placeholder = $enter . $label;
                }
                $query->setPlaceholder($placeholder);
            }

            $data[] = $query;
        }

        return $data;
    }

    /**
     * @param $columns
     * @return Column[]
     */
    public function columns($columns): array
    {
        $data = [];

        $page = Factory::config()->page;
        $images = $page->columnImg();
        $switch = $page->columnSwitch();

        foreach ($columns as $item) {
            /**
             * @var Column $column
             */
            if (is_string($item)) {
                $item = explode('.', $item);
                $field = $item[0];
                $width = $item[1] ?? 0;
                if (in_array($field, $images)) {
                    $column = new ImageColumn();
                } elseif (in_array($field, $switch)) {
                    $column = new SwitchColumn();
                } else {
                    $column = new TextColumn();
                    if ($field == 'sort') {
                        $column->setEdit();
                    }
                }

                $title = $this->transferLabel($field);
                $column->setField($field);
                $column->setTitle($title);

                if ($field == 'created_at' && $width == 0) {
                    $width = 180;
                }

                if ($width) {
                    $column->setWidth(intval($width));
                }

            } else {
                $column = $item;
            }

            $field = $column->getField();
            $title = $column->getTitle() ?: $this->transferLabel($field);
            $column->setTitle($title);

            $data[$field] = $column;
        }

        return $data;
    }

    /**
     * @param $columns
     * @return array
     */
    public function columnsTransfer($columns): array
    {
        $columns = $this->columns($columns);

        $data = [];
        foreach ($columns as $column) {
            $data[] = [
                'type' => $column->getType(),
                'field' => $column->getField(),
                'title' => $column->getTitle(),
                'sort' => $column->getSortable(),
                'templet' => $column->getTemplate(),
                'width' => $column->getWidth(),
                'extra' => $column->getExtra(),
                'fixed' => $column->getFixed(),
                'edit' => $column->getEdit(),
            ];
        }

        return $data;
    }

    /**
     * @param $alerts
     * @return Alert[]
     */
    public function alerts($alerts): array
    {
        $data = [];
        if (empty($alerts)) {
            return $data;
        }

        if (is_string($alerts)) {
            $alerts = [$alerts];
        }

        foreach ($alerts as $alert) {
            if (is_string($alert)) {
                $alt = new DangerAlert();
                $alt->setText($alert);
            } else {
                $alt = $alert;
            }

            if ($alt instanceof Alert) {
                $data[] = $alt;
            }
        }

        return $data;
    }

    /**
     * @param $name
     * @return DeleteButton|EditButton|PageButton|ReadButton|null
     */
    public function button($name)
    {
        if (empty($name)) {
            return null;
        }
        if ($name === 'read') {
            $btn = new ReadButton();
        } elseif ($name === 'edit') {
            $btn = new EditButton();
        } elseif ($name === 'del') {
            $btn = new DeleteButton();
        } else {
            $btn = new PageButton();
        }

        return $btn;
    }

    /**
     * 按钮
     * @param $buttons
     * @return Button[]
     */
    public function buttons($buttons): array
    {
        $data = [];

        if (empty($buttons)) {
            return $data;
        }

        if (is_string($buttons)) {
            $buttons = [$buttons];
        }

        foreach ($buttons as $button) {
            if (is_string($button)) {
                $button = $this->button($button);
            }
            if ($button instanceof Button) {
                $data[] = $button;
            }
        }

        return $data;
    }

    /**
     * @param $buttons
     * @return array
     */
    public function btnTransfer($buttons): array
    {
        $buttons = $this->buttons($buttons);

        $data = [];

        foreach ($buttons as $btn) {
            $confirm = $btn->getConfirm();
            $data[] = [
                'text' => $this->btn[$btn->getText()] ?? $btn->getText(),
                'style' => $btn->getStyle(),
                'map' => $btn->getMap(),
                'query' => $btn->getQuery(),
                'event' => $btn->getEvent(),
                'url' => $btn->getUrl(),
                'dialog' => $btn->getDialog()->toArray(),
                'confirm' => $confirm ? $confirm->toArray() : null
            ];
        }

        return $data;
    }

    /**
     * @param $toolbars
     * @return Button[]
     */
    public function toolbars($toolbars): array
    {
        $data = [];

        foreach ($toolbars as $toolbar) {
            if ($toolbar === 'create') {
                $toolbar = new CreateToolbar();
            } elseif ($toolbar === 'del') {
                $toolbar = new DeleteToolbar();
            } elseif (is_string($toolbar)) {
                $toolbar = new NormalToolbar();
            }

            $toolbar->setText($this->btn[$toolbar->getText()] ?? $toolbar->getText());

            $data[] = $toolbar;
        }

        return $data;
    }

    /**
     * @param $controls
     * @return Control[]
     */
    public function controls($controls): array
    {
        $data = [];

        $page = Factory::config()->page;

        $images = $page->controlImg();
        $password = $page->controlPassword();
        $file = $page->controlFile();
        $hidden = $page->controlHidden();

        foreach ($controls as $control) {

            if (is_string($control)) {
                $required = true;
                if (preg_match('/\.false/', $control)) {
                    $name = str_replace('.false', '', $control);
                    $required = false;
                } else {
                    $name = $control;
                }

                if ($name == 'status') {
                    $control = new RadioControl();
                    $control->setOptions([0 => SeeLang::switch('forbid'), 1 => SeeLang::switch('open')]);
                    $control->setValue(1);
                } elseif ($name == 'sort') {
                    $control = new InputControl();
                    $control->setValue(99);
                } elseif (in_array($name, ['start_time', 'begin_time', 'end_time'])) {
                    $control = new DatetimeControl();
                    $control->setValue(date('Y-m-d H:i:s'));
                } elseif (in_array($name, $images)) {
                    $control = new ImageControl();
                } elseif (in_array($name, $file)) {
                    $control = new FileControl();
                } elseif (in_array($name, $password)) {
                    $control = new PasswordControl();
                } elseif (in_array($name, $hidden)) {
                    $control = new HiddenControl();
                } else {
                    $control = new InputControl();
                }
                $control->setName($name);
                $control->setRequired($required);

            } elseif (is_array($control)) {
                $len = count($control);
                $name = $control[0];
                $second = $control[1] ?? null;
                $third = $control[2] ?? null;
                $control = new RadioControl();
                $control->setName($name);
                if ($len == 2 && is_array($second)) {
                    $control->setOptions($second);
                } elseif ($len == 3 && is_array($third)) {
                    $control->setType($second);
                    $control->setOptions($third);
                }
            }

            $name = $control->getName();
            $label = $control->getLabel() ?: $this->transferLabel($name);

            $control->setLabel($label);

            $data[] = $control;
        }

        return $data;
    }
}
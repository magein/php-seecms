<?php

declare(strict_types=1);

namespace magein\seecms\page\unit\query;

class BetweenQuery extends Query
{
    protected $type = 'between';

    protected $size = 3;

    protected $express = 'between';
}
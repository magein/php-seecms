<?php

declare(strict_types=1);

namespace magein\seecms\page\unit\query;

class DatetimeQuery extends DateQuery
{
    protected $format = 'datetime';
}
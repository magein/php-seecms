<?php

declare(strict_types=1);

namespace magein\seecms\page\unit\query;

class TextQuery extends Query
{
    protected $type = 'text';
}
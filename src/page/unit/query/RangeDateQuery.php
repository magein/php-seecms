<?php

declare(strict_types=1);

namespace magein\seecms\page\unit\query;

class RangeDateQuery extends DateQuery
{
    protected $range = true;

    protected $size = 3;
}
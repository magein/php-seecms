<?php

declare(strict_types=1);

namespace magein\seecms\page\unit\query;

class HiddenQuery extends Query
{
    protected $type = 'hidden';
}
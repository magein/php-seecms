<?php

declare(strict_types=1);

namespace magein\seecms\page\unit\query;

class SelectQuery extends Query
{
    protected $type = 'select';
}
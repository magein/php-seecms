<?php

declare(strict_types=1);

namespace magein\seecms\page\unit\query;

class RegionQuery extends Query
{
    protected $type = 'region';

    protected $level = 3;

    /**
     * @return int
     */
    public function getSize(): int
    {
        $level = $this->getLevel();
        $size = 2;
        if ($level == 2) {
            $size = 4;
        } elseif ($level == 3) {
            $size = 6;
        }

        return $size;
    }

    /**
     * @return int
     */
    public function getLevel(): int
    {
        return $this->level;
    }

    /**
     * @param int $level
     * @return $this
     */
    public function setLevel(int $level): RegionQuery
    {
        if ($level > 3 || $level <= 0) {
            $level = 3;
        }

        $this->level = $level;

        return $this;
    }
}
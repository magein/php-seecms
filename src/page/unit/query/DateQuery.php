<?php

declare(strict_types=1);

namespace magein\seecms\page\unit\query;

class DateQuery extends Query
{
    /**
     * @var string
     */
    protected $type = 'datetime';

    protected $format = 'date';

    /**
     * @var bool
     */
    protected $range = false;

    /**
     * 表达式
     * @var string
     */
    protected $express = 'date';

    /**
     * @return bool
     */
    public function isRange(): bool
    {
        return $this->range;
    }

    /**
     * @param bool $range
     * @return $this
     */
    public function setRange(bool $range = true): DateQuery
    {
        $this->range = $range;
        $this->size = 4;

        return $this;
    }

    /**
     * @return mixed|string
     */
    public function getFormat()
    {
        return $this->format;
    }
}
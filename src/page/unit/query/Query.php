<?php

declare(strict_types=1);

namespace magein\seecms\page\unit\query;

use ArrayAccess;

abstract class Query implements ArrayAccess
{
    /**
     * 类型
     * @var string
     */
    protected $type = 'text';

    /**
     * @var string
     */
    protected $name = '';

    /**
     * @var string
     */
    protected $label = '';

    /**
     * @var string
     */
    protected $placeholder = '';

    /**
     * @var int
     */
    protected $size = 2;

    /**
     * 默认值
     * @var mixed
     */
    protected $value = '';

    /**
     * @var array
     */
    protected $options = [];

    /**
     * @var array
     */
    protected $data = [];

    /**
     * 表达式
     * @var string
     */
    protected $express = '';

    public function offsetSet($offset, $value)
    {
        $this->$offset = $value;
    }

    public function offsetExists($offset)
    {
        return isset($this->$offset);
    }

    public function offsetGet($offset)
    {
        return $this->$offset ?? null;
    }

    public function offsetUnset($offset)
    {
        unset($this->$offset);
    }

    /**
     * @param $name
     * @param string|null $express
     * @return static
     */
    public static function init($name, string $express = null)
    {
        $self = new static();
        $self->name = $name;
        $self->express = $express ?: ($self->express ?: '');
        return $self;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @return string
     */
    public function getLabel(): string
    {
        return $this->label;
    }

    /**
     * @param $label
     * @return $this
     */
    public function setLabel($label): Query
    {
        $this->label = $label;

        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return $this
     */
    public function setName(string $name): Query
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string
     */
    public function getPlaceholder(): string
    {
        return $this->placeholder;
    }

    /**
     * @param string $placeholder
     * @return $this
     */
    public function setPlaceholder(string $placeholder): Query
    {
        $this->placeholder = $placeholder;

        return $this;
    }

    /**
     * @return int
     */
    public function getSize(): int
    {
        return $this->size ?: 2;
    }

    /**
     * @param int $size
     * @return $this
     */
    public function setSize(int $size): Query
    {
        if ($size > 4) {
            $size = 4;
        } elseif ($size < 1) {
            $size = 1;
        }

        $this->size = $size;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param mixed $value
     * @return $this
     */
    public function setValue($value): Query
    {
        $this->value = $value;

        return $this;
    }

    /**
     * @return array
     */
    public function getOptions(): array
    {
        return $this->options;
    }

    /**
     * @param array $options
     * @return $this
     */
    public function setOptions(array $options): Query
    {
        $this->options = $options;

        return $this;
    }

    /**
     * @return array
     */
    public function getData(): array
    {
        return $this->data;
    }

    /**
     * @param string $field
     * @param string $value
     * @return void
     */
    public function setData(string $field, string $value = '')
    {
        $this->data[$field] = $value;
    }

    /**
     * @return string
     */
    public function getExpress(): string
    {
        return $this->express ?: '=';
    }

    public function setExpress(string $express)
    {
        $this->express = $express;

        return $this;
    }

    public function setExpressIn(): Query
    {
        $this->express = 'in';

        return $this;
    }

    public function setExpressLike(): Query
    {
        $this->express = 'like';

        return $this;
    }
}
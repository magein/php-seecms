<?php

declare(strict_types=1);

namespace magein\seecms\page\unit\button;

/**
 * display 0 删除  1 显示
 */
class AjaxButton extends Button
{
    protected $text = 'ajax';

    protected $style = 'layui-btn layui-btn-sm layui-btn-normal';

    protected $event = '__ajax__';
}
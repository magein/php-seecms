<?php

declare(strict_types=1);

namespace magein\seecms\page\unit\button;

class EditButton extends Button
{
    protected $text = 'edit';

    protected $style = 'layui-btn-normal';

    protected $event = '__edit__';
}
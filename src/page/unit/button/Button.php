<?php

declare(strict_types=1);

namespace magein\seecms\page\unit\button;

use ArrayAccess;
use magein\seecms\page\unit\modal\Confirm;
use magein\seecms\page\unit\modal\Dialog;
use magein\seecms\SeeLang;

abstract class Button implements ArrayAccess
{
    /**
     * 按钮文字
     * @var string
     */
    protected $text = '';

    /**
     * 按钮样式
     * @var string
     */
    protected $style = 'layui-btn layui-btn-sm layui-btn-normal';

    /**
     * 按钮图标
     * @var string
     */
    protected $icon = '';

    /**
     * @var string
     */
    protected $url = '';

    /**
     * @var string
     */
    protected $target = '_blank';

    /**
     * @var string
     */
    protected $event = '';

    /**
     * map字段是需要二次处理的，用于提取表格中的字段
     * @var string[]
     */
    protected $map = [];

    /**
     * 设置链接中的参数,这里是设定固定值
     * @var array
     */
    protected $query = [];

    /**
     * @var Dialog |null
     */
    protected $dialog = null;

    /**
     * @var Confirm|null
     */
    protected $confirm = null;

    /**
     * 是否多个选项
     * @var array
     */
    protected $options = [];

    public function offsetSet($offset, $value)
    {
        $this->$offset = $value;
    }

    public function offsetExists($offset)
    {
        return isset($this->$offset);
    }

    public function offsetGet($offset)
    {
        return $this->$offset ?? null;
    }

    public function offsetUnset($offset)
    {
        unset($this->$offset);
    }


    /**
     * @param string $text
     * @return static
     */
    public static function init(string $text = '')
    {
        $self = new static();
        $self->setText(SeeLang::btn($text));
        return $self;
    }

    /**
     * @return string
     */
    public function getText(): string
    {
        return $this->text;
    }

    /**
     * @param string $text
     */
    public function setText(string $text)
    {
        if ($text) {
            $this->text = $text;
        }
    }

    /**
     * @return string
     */
    public function getUrl(): string
    {
        return $this->url;
    }

    /**
     * @param string $url
     * @return Button
     */
    public function setUrl(string $url): Button
    {
        $this->url = $url;

        return $this;
    }

    /**
     * @return array
     */
    public function getQuery(): array
    {
        return $this->query ?: [];
    }

    /**
     * @param string $field
     * @param string|int|null $value
     * @return Button
     */
    public function setQuery(string $field, string $value = null): Button
    {
        $this->query[$field] = $value;

        return $this;
    }

    /**
     * @return string[]
     */
    public function getMap(): array
    {
        if (!isset($this->map['id'])) {
            $this->map['id'] = 'id';
        }
        return $this->map;
    }

    /**
     * 这设置的是data-xx的值   会从记录中替换字段对应的值
     * setData则不会，直接设置成前端dom中的数据
     * @param string $field
     * @param string|null $map_field
     * @return Button
     */
    public function setMap(string $field, string $map_field = null): Button
    {
        if ($field) {
            $this->map[$field] = $map_field ?: $field;
        }

        return $this;
    }

    /**
     * @return string
     */
    public function getStyle(): string
    {
        return $this->style ?: 'btn-primary';
    }

    /**
     * @param string $style
     */
    public function setStyle(string $style)
    {
        $this->style = $style;
    }

    /**
     * @return string
     */
    public function getEvent(): string
    {
        return $this->event;
    }

    /**
     * @param string $event
     * @return void
     */
    public function setEvent(string $event): void
    {
        $this->event = $event;
    }

    /**
     * @param Dialog $dialog
     * @return $this
     */
    public function setDialog(Dialog $dialog): Button
    {
        $this->dialog = $dialog;

        return $this;
    }

    /**
     * @return Dialog
     */
    public function getDialog(): Dialog
    {
        return ($this->dialog instanceof Dialog) ? $this->dialog : Dialog::init();
    }

    /**
     * @return Confirm
     */
    public function getConfirm(): ?Confirm
    {
        return $this->confirm;
    }

    /**
     * @param Confirm|null $confirm
     * @return $this
     */
    public function setConfirm(Confirm $confirm = null): Button
    {
        $this->confirm = $confirm ?: Confirm::init();

        return $this;
    }

    /**
     * @return string
     */
    public function getIcon(): string
    {
        return $this->icon;
    }

    /**
     * @param string $icon
     * @return Button
     */
    public function setIcon(string $icon): Button
    {
        $this->icon = $icon;

        return $this;
    }

    /**
     * @return string
     */
    public function getTarget(): string
    {
        return $this->target ?: '';
    }

    /**
     * @param string $target
     * @return Button
     */
    public function setTarget(string $target): Button
    {
        $this->target = $target;

        return $this;
    }

    /**
     * @return array
     */
    public function getOptions(): array
    {
        return $this->options;
    }

    /**
     * @param array $options
     * @return Button
     */
    public function setOptions(array $options): Button
    {
        $this->options = $options;

        return $this;
    }
}
<?php

declare(strict_types=1);

namespace magein\seecms\page\unit\button;

/**
 * display 0 删除  1 显示
 */
class RedirectButton extends Button
{
    protected $text = 'redirect';

    protected $event = '__redirect__';
}
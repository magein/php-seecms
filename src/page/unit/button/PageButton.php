<?php

/**
 * @user magein
 * @date 2024/1/26 18:47
 */

declare(strict_types=1);

namespace magein\seecms\page\unit\button;

class PageButton extends Button
{
    protected $text = 'page';

    protected $event = '__page__';

    public static function init(string $text = '')
    {
        $self = parent::init($text);
        $self->setText($text);
        $self->setMap('id');

        return $self;
    }
}
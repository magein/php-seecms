<?php

declare(strict_types=1);

namespace magein\seecms\page\unit\button;

class ReadButton extends Button
{
    protected $text = 'read';

    protected $style = 'layui-btn-normal';

    protected $event = '__read__';
}
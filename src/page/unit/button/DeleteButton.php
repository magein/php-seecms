<?php

declare(strict_types=1);

namespace magein\seecms\page\unit\button;

/**
 * display 0 删除  1 显示
 */
class DeleteButton extends Button
{
    protected $text = 'delete';

    protected $style = 'layui-btn-danger';

    protected $event = '__del__';
}
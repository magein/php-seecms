<?php

declare(strict_types=1);

namespace magein\seecms\page\unit\modal;

class Confirm
{
    /**
     * @var string
     */
    protected $icon = 3;

    /**
     * @var string
     */
    protected $title = '友情提示';

    /**
     * @var string
     */
    protected $content = '请再次确认是否继续操作?';

    /**
     * @param string $content
     * @return static
     */
    public static function init(string $content = ''): Confirm
    {
        $self = new static();
        $self->content = $content ?: '友情提示';
        return $self;
    }

    /**
     * @return int|string
     */
    public function getIcon()
    {
        return $this->icon;
    }

    /**
     * @param $icon
     * @return $this
     */
    public function setIcon($icon): Confirm
    {
        $this->icon = $icon;

        return $this;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     * @return $this
     */
    public function setTitle(string $title): Confirm
    {
        $this->title = $title;

        return $this;
    }

    public function getContent(): string
    {
        return $this->content;
    }

    /**
     * @param string $content
     * @return $this
     */
    public function setContent(string $content): Confirm
    {
        $this->content = $content;

        return $this;
    }

    public function toArray(): array
    {
        return [
            'icon' => $this->getIcon(),
            'title' => $this->getTitle(),
            'content' => $this->getContent(),
        ];
    }
}
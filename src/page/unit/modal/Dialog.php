<?php

declare(strict_types=1);

namespace magein\seecms\page\unit\modal;

use magein\seecms\SeeLang;

class Dialog
{
    protected $title = '';

    /**
     * @var array|string|null
     */
    protected $area = 'large';

    /**
     * @param string $title
     * @return static
     */
    public static function init(string $title = ''): Dialog
    {
        $self = new static();
        $self->title = SeeLang::dialog($title ?: '数据表单');
        return $self;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     * @return $this
     */
    public function setTitle(string $title): Dialog
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @return array|string|null
     */
    public function getArea()
    {
        return $this->area;
    }

    /**
     * @param int $width
     * @param int $height
     * @return $this
     */
    public function setArea(int $width, int $height): Dialog
    {

        $width = max($width, 200);
        $height = max($height, 200);

        $this->area = [$width, $height];

        return $this;
    }

    public function areaSmall(): Dialog
    {
        $this->area = 'small';

        return $this;
    }

    public function areaMiddle(): Dialog
    {
        $this->area = 'middle';

        return $this;
    }

    /**
     * @return $this
     */
    public function areaLarge(): Dialog
    {
        $this->area = 'large';

        return $this;
    }

    public function toArray(): array
    {
        return [
            'title' => $this->getTitle(),
            'area' => $this->getArea(),
        ];
    }
}
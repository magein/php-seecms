<?php

declare(strict_types=1);

namespace magein\seecms\page\unit\toolbar;

class NormalToolbar extends Toolbar
{
    protected $text = 'normal';

    protected $icon = 'layui-icon layui-icon-set';

    protected $style = 'layui-btn layui-btn-normal layui-btn-sm';
}
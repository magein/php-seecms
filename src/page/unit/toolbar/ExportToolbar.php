<?php

declare(strict_types=1);

namespace magein\seecms\page\unit\toolbar;

class ExportToolbar extends Toolbar
{
    protected $text = 'export';

    protected $icon = 'layui-icon layui-icon-export';

    protected $style = 'layui-btn layui-btn-normal layui-btn-sm';

    protected $action = 'export';

    protected $options = [
        [
            'name' => 'xlsx',
            'icon' => 'la la-file-excel-o'
        ],
        [
            'name' => 'csv',
            'icon' => 'la la-file-text-o'
        ]
    ];
}
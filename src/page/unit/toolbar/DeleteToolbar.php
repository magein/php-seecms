<?php

declare(strict_types=1);

namespace magein\seecms\page\unit\toolbar;

class DeleteToolbar extends Toolbar
{
    protected $text = 'delete';

    protected $style = 'layui-btn layui-btn-danger layui-btn-sm';

    protected $icon = 'layui-icon layui-icon-delete';

    protected $event = '__delete__';
}
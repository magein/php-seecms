<?php

declare(strict_types=1);

namespace magein\seecms\page\unit\toolbar;

class PageToolbar extends Toolbar
{
    protected $icon = 'layui-icon layui-icon-set';

    protected $event = '__page__';
}
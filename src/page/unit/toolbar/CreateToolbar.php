<?php

declare(strict_types=1);

namespace magein\seecms\page\unit\toolbar;

class CreateToolbar extends Toolbar
{
    protected $text = 'create';

    protected $icon = 'layui-icon layui-icon-addition';

    protected $event = '__create__';
}
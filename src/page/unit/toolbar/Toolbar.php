<?php

declare(strict_types=1);

namespace magein\seecms\page\unit\toolbar;

use ArrayAccess;
use magein\seecms\page\unit\modal\Dialog;
use magein\seecms\SeeLang;

abstract class Toolbar implements ArrayAccess
{
    /**
     * 按钮名称
     * @var string
     */
    protected $text = '';

    /**
     * 按钮图标
     * @var string
     */
    protected $icon = '';

    /**
     * 按钮样式
     * @var string
     */
    protected $style = 'layui-btn layui-btn-normal layui-btn-sm';

    /**
     * 是否多个选项
     * @var array
     */
    protected $options = [];

    /**
     * 绑定事件
     * @var string
     */
    protected $event = '';

    /**
     * @var string
     */
    protected $url = '';

    /**
     * @var string
     */
    protected $target = '_blank';

    /**
     * 设置链接中的参数,这里是设定固定值
     * @var array
     */
    protected $query = [];

    /**
     * @var Dialog|null
     */
    protected $dialog = null;

    public function offsetSet($offset, $value)
    {
        $this->$offset = $value;
    }

    public function offsetExists($offset)
    {
        return isset($this->$offset);
    }

    public function offsetGet($offset)
    {
        return $this->$offset ?? null;
    }

    public function offsetUnset($offset)
    {
        unset($this->$offset);
    }

    /**
     * @param string $text
     * @return static
     */
    public static function init(string $text = ''): Toolbar
    {
        $self = new static();
        $self->setText(SeeLang::btn($text));
        return $self;
    }

    /**
     * @return string
     */
    public function getText(): string
    {
        return $this->text;
    }

    /**
     * @param string $text
     * @return Toolbar
     */
    public function setText(string $text): Toolbar
    {
        $this->text = $text;

        return $this;
    }

    /**
     * @return string
     */
    public function getIcon(): string
    {
        return $this->icon;
    }

    /**
     * @param string $icon
     * @return Toolbar
     */
    public function setIcon(string $icon): Toolbar
    {
        $this->icon = $icon;

        return $this;
    }

    /**
     * @return string
     */
    public function getStyle(): string
    {
        return $this->style;
    }

    /**
     * @param string $style
     * @return Toolbar
     */
    public function setStyle(string $style): Toolbar
    {
        $this->style = $style;

        return $this;
    }

    /**
     * @return array
     */
    public function getOptions(): array
    {
        return $this->options;
    }

    /**
     * @param array $options
     * @return Toolbar
     */
    public function setOptions(array $options): Toolbar
    {
        $this->options = $options;

        return $this;
    }

    /**
     * @return string
     */
    public function getEvent(): string
    {
        return $this->event;
    }

    /**
     * @param string $event
     * @return Toolbar
     */
    public function setEvent(string $event): Toolbar
    {
        $this->event = $event;

        return $this;
    }

    /**
     * @return string
     */
    public function getUrl(): string
    {
        return $this->url;
    }

    /**
     * @param string $url
     * @return Toolbar
     */
    public function setUrl(string $url): Toolbar
    {
        $this->url = $url;

        return $this;
    }

    /**
     * @return string
     */
    public function getTarget(): string
    {
        return $this->target ?: '';
    }

    /**
     * @param string $target
     * @return Toolbar
     */
    public function setTarget(string $target): Toolbar
    {
        $this->target = $target;

        return $this;
    }

    /**
     * @return array
     */
    public function getQuery(): array
    {
        return $this->query;
    }

    /**
     * @param array $query
     * @return $this
     */
    public function setQuery(array $query): Toolbar
    {
        $this->query = $query;

        return $this;
    }

    public function getDialog(): Dialog
    {
        return $this->dialog ?: new Dialog();
    }

    public function setDialog(Dialog $form): Toolbar
    {
        $this->dialog = $form;

        return $this;
    }
}
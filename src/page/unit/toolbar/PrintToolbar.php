<?php

declare(strict_types=1);

namespace magein\seecms\page\unit\toolbar;

class PrintToolbar extends Toolbar
{
    protected $text = 'print';

    protected $icon = 'layui-icon layui-icon-print';

    protected $style = 'layui-btn layui-btn-normal layui-btn-sm';

    protected $action = 'export';

    protected $options = [
        [
            'name' => '打印',
            'icon' => 'la la-print',
        ],
        [
            'name' => 'xlsx',
            'icon' => 'la la-file-excel-o'
        ],
        [
            'name' => 'csv',
            'icon' => 'la la-file-text-o'
        ]
    ];
}
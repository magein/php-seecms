<?php

declare(strict_types=1);

namespace magein\seecms\page\unit\alert;

class DangerAlert extends Alert
{
    /**
     * @var string
     */
    protected $type = self::DANGER;
}
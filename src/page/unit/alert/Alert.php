<?php

declare(strict_types=1);

namespace magein\seecms\page\unit\alert;

use ArrayAccess;

abstract class Alert implements ArrayAccess
{

    const PRIMARY = 'primary';

    const SUCCESS = 'success';

    const INFO = 'info';

    const WARNING = 'warning';

    const DANGER = 'danger';

    /**
     * @var string
     */
    protected $type = self::PRIMARY;

    /**
     * @var bool
     */
    protected $light = true;

    /**
     * @var string
     */
    protected $text = '';

    public function offsetSet($offset, $value)
    {
        $this->$offset = $value;
    }

    public function offsetExists($offset): bool
    {
        return isset($this->$offset);
    }

    public function offsetGet($offset)
    {
        return $this->$offset ?? null;
    }

    public function offsetUnset($offset)
    {
        unset($this->$offset);
    }

    public static function init($text = null): Alert
    {
        $self = new static();
        $self->setText($text ?: '');
        return $self;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     * @return void
     */
    public function setType(string $type): void
    {
        $this->type = $type;
    }

    /**
     * @return string
     */
    public function getText(): string
    {
        return $this->text;
    }

    /**
     * @param string $text
     * @return void
     */
    public function setText(string $text): void
    {
        $this->text = $text;
    }

    /**
     * @return bool
     */
    public function isLight(): bool
    {
        return $this->light;
    }

    /**
     * @param bool $light
     * @return void
     */
    public function setLight(bool $light): void
    {
        $this->light = $light;
    }
}
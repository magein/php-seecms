<?php

declare(strict_types=1);

namespace magein\seecms\page\unit\alert;

class InfoAlert extends Alert
{
    /**
     * @var string
     */
    protected $type = self::INFO;
}
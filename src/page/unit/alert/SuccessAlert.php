<?php

declare(strict_types=1);

namespace magein\seecms\page\unit\alert;

class SuccessAlert extends PrimaryAlert
{
    /**
     * @var string
     */
    protected $type = self::SUCCESS;
}
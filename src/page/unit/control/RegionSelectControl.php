<?php

declare(strict_types=1);

namespace magein\seecms\page\unit\control;

class RegionSelectControl extends Control
{
    protected $type = 'region';

    protected $province_field = 'province_id';
    protected $city_field = 'city_id';
    protected $area_field = 'area_id';

    /**
     * @return string
     */
    public function getProvinceField(): string
    {
        return $this->province_field;
    }

    /**
     * @param string $province_field
     */
    public function setProvinceField(string $province_field)
    {
        $this->province_field = $province_field;
    }

    /**
     * @return string
     */
    public function getCityField(): string
    {
        return $this->city_field;
    }

    /**
     * @param string $city_field
     */
    public function setCityField(string $city_field)
    {
        $this->city_field = $city_field;
    }

    /**
     * @return string
     */
    public function getAreaField(): string
    {
        return $this->area_field;
    }

    /**
     * @param string $area_field
     */
    public function setAreaField(string $area_field)
    {
        $this->area_field = $area_field;
    }
}
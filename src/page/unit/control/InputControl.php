<?php

declare(strict_types=1);

namespace magein\seecms\page\unit\control;

class InputControl extends Control
{
    protected $type = 'text';
}
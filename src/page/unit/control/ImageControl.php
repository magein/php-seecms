<?php

declare(strict_types=1);

namespace magein\seecms\page\unit\control;

class ImageControl extends FileControl
{
    /**
     * @var string
     */
    protected $accept = 'images';

    /**
     * 默认的扩展文件
     * @var array
     */
    protected $extension = ['jpg', 'png', 'gif', 'jpeg'];
}
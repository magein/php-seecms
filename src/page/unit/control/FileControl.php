<?php

declare(strict_types=1);

namespace magein\seecms\page\unit\control;

use magein\seecms\Factory;

class FileControl extends Control
{
    protected $type = 'file';

    /**
     * 默认的扩展文件
     * @var array
     */
    protected $extension = ['xls', 'xlsx', 'csv', 'txt', 'doc', 'docx', 'ppt', 'pptx', 'pdf', 'jpg', 'png', 'gif', 'jpeg'];

    /**
     * 上传数量
     * @var int
     */
    protected $limit = 1;

    /**
     * 上传文件大小 默认2M
     * @var int
     */
    protected $size = 2097152;

    /**
     * @var string
     */
    protected $accept = 'file';

    /**
     * 磁盘
     * @var string
     */
    protected $disk = '';

    /**
     * 上传路径
     * @var string
     */
    protected $url = '';

    /**
     * 允许从附件中选择
     * @var bool
     */
    protected $choose = true;

    /**
     * @return array
     */
    public function getExtension(): array
    {
        return $this->extension;
    }

    /**
     * @param array $extension
     * @return FileControl
     */
    public function setExtension(array $extension): FileControl
    {
        $this->extension = $extension;

        return $this;
    }

    /**
     * @return int
     */
    public function getLimit(): int
    {
        return $this->limit;
    }

    /**
     * @param int $limit
     * @return FileControl
     */
    public function setLimit(int $limit): FileControl
    {
        $this->limit = $limit;

        return $this;
    }

    /**
     * @return int
     */
    public function getSize(): int
    {
        return $this->size;
    }

    /**
     * @param int $size
     * @return $this
     */
    public function setSize(int $size): FileControl
    {
        $this->size = $size;

        return $this;
    }

    public function getAccept(): string
    {
        return $this->accept;
    }

    /**
     * @param string $accept
     * @return $this
     */
    public function setAccept(string $accept): FileControl
    {
        $this->accept = $accept;

        return $this;
    }

    public function getDisk(): string
    {
        return $this->disk ?: '';
    }

    /**
     * @param string $disk
     * @return $this
     */
    public function setDisk(string $disk): FileControl
    {
        $this->disk = $disk;

        return $this;
    }

    public function getUrl(): string
    {
        return $this->url ?: Factory::config()->upload->url();
    }

    public function setUrl(string $url): FileControl
    {
        $this->url = $url;

        return $this;
    }

    public function isChoose(): bool
    {
        return $this->choose;
    }

    public function setChoose(bool $choose = false): FileControl
    {
        $this->choose = $choose;

        return $this;
    }
}
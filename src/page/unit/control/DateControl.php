<?php

declare(strict_types=1);

namespace magein\seecms\page\unit\control;

class DateControl extends Control
{
    protected $type = 'date';

    protected $format = 'date';

    /**
     * @var bool
     */
    protected $range = false;

    /**
     * @param $name
     * @return DateControl|static
     */
    public static function init($name = null)
    {
        $self = new static();
        $self->name = $name;
        return $self;
    }

    /**
     * @return string
     */
    public function getFormat(): string
    {
        return $this->format;
    }

    /**
     * @param string $format
     * @return DateControl
     */
    public function setFormat(string $format): DateControl
    {
        $this->format = $format;

        return $this;
    }

    /**
     * @return bool
     */
    public function isRange(): bool
    {
        return $this->range;
    }

    /**
     * @param bool $range
     * @return DateControl
     */
    public function setRange(bool $range = true): DateControl
    {
        $this->range = $range;

        return $this;
    }
}
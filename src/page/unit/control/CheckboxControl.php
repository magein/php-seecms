<?php

declare(strict_types=1);

namespace magein\seecms\page\unit\control;

class CheckboxControl extends Control
{
    protected $type = 'checkbox';
}
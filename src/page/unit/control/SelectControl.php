<?php

declare(strict_types=1);

namespace magein\seecms\page\unit\control;

class SelectControl extends Control
{
    protected $type = 'select';

    protected $multiple = false;

    /**
     * @return bool
     */
    public function isMultiple(): bool
    {
        return $this->multiple;
    }

    /**
     * @param bool $multiple
     * @return SelectControl
     */
    public function setMultiple(bool $multiple): SelectControl
    {
        $this->multiple = $multiple;

        return $this;
    }
}
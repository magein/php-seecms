<?php

declare(strict_types=1);

namespace magein\seecms\page\unit\control;

class HiddenControl extends Control
{
    protected $type = 'hidden';
}
<?php

declare(strict_types=1);

namespace magein\seecms\page\unit\control;

class DatetimeControl extends DateControl
{
    protected $type = 'date';

    protected $format = 'datetime';
}
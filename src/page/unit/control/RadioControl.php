<?php

declare(strict_types=1);

namespace magein\seecms\page\unit\control;

class RadioControl extends Control
{
    protected $type = 'radio';

    protected $options = [
        0 => '否',
        1 => '是'
    ];
}
<?php

declare(strict_types=1);

namespace magein\seecms\page\unit\control;

class SwitchControl extends Control
{
    protected $type = 'switch';

    protected $options = [
        0 => '开启',
        1 => '关闭'
    ];
}
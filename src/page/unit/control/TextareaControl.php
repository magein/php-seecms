<?php

declare(strict_types=1);

namespace magein\seecms\page\unit\control;

use magein\seecms\Factory;

class TextareaControl extends Control
{
    protected $type = 'textarea';

    protected $edit = false;

    /**
     * 磁盘
     * @var string
     */
    protected $disk = '';

    /**
     * 提交路径
     * @var string
     */
    protected $url = '';

    public function isEdit(): bool
    {
        return $this->edit;
    }

    /**
     * @param bool $edit
     * @return $this
     */
    public function setEdit(bool $edit = true): TextareaControl
    {
        $this->edit = $edit;

        return $this;
    }

    public function getDisk(): string
    {
        return $this->disk ?: '';
    }

    /**
     * @param string $disk
     * @return $this
     */
    public function setDisk(string $disk): TextareaControl
    {
        $this->disk = $disk;

        return $this;
    }

    public function getUrl(): string
    {
        return $this->url ?: Factory::config()->upload->url();
    }

    public function setUrl(string $url): TextareaControl
    {
        $this->url = $url;

        return $this;
    }
}
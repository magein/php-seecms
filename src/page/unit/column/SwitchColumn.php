<?php

declare(strict_types=1);

namespace magein\seecms\page\unit\column;

use magein\seecms\SeeLang;

class SwitchColumn extends Column
{
    protected $template = 'switch';

    protected $show_text = [];

    public function setShowText(array $show_text)
    {
        $this->show_text = $show_text;
    }

    public function getExtra(): array
    {
        $forbid = SeeLang::switch('forbid');
        $open = SeeLang::switch('open');

        $show_text = $this->show_text ?: [
            0 => $forbid,
            1 => $open
        ];

        return [
            'show_text' => implode('|', $show_text)
        ];
    }
}
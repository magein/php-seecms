<?php

declare(strict_types=1);

namespace magein\seecms\page\unit\column;

class HrefColumn extends Column
{
    /**
     * 跳转的地址
     * 1. 字段本身是一个链接
     * 2. 不是一个链接，但是要跳转到某一个链接
     * @var string
     */
    protected $href = '';

    /**
     * 打开方式 _blank、_self
     * @var string
     */
    protected $target = '_blank';

    /**
     * 显示的文本类型 _self、固定值
     * @var string
     */
    protected $text = '_self';

    /**
     * 定义模板类型
     * @var string
     */
    protected $template = 'href';

    /**
     * @var bool
     */
    protected $sortable = false;

    /**
     * @var array
     */
    protected $query = [];

    /**
     * @return string
     */
    public function getHref(): string
    {
        return $this->href;
    }

    /**
     * @param string $href
     */
    public function setHref(string $href)
    {
        $this->href = $href;
    }

    /**
     * @return string
     */
    public function getTarget(): string
    {
        return $this->target;
    }

    /**
     * @param string $target
     */
    public function setTarget(string $target)
    {
        $this->target = $target;
    }

    /**
     * @return string
     */
    public function getText(): string
    {
        return $this->text;
    }

    /**
     * @param string $text
     */
    public function setText(string $text)
    {
        $this->text = $text;
    }

    /**
     * @return array
     */
    public function getQuery(): array
    {
        return $this->query;
    }

    public function setQuery(array $query)
    {
        if ($query) {
            $this->query = $query;
        }
    }

    /**
     * @param string $field
     * @param string $map_field
     * @return void
     */
    public function setQueryField(string $field, string $map_field)
    {
        if ($field) {
            $this->query[$field] = $map_field;
        }
    }
}
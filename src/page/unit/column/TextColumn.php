<?php

declare(strict_types=1);

namespace magein\seecms\page\unit\column;

class TextColumn extends Column
{
    protected $type = 'normal';
}
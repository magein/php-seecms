<?php

declare(strict_types=1);

namespace magein\seecms\page\unit\column;

class ImageColumn extends Column
{
    protected $template = 'image';
}
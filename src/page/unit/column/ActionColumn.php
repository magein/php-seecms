<?php

declare(strict_types=1);

namespace magein\seecms\page\unit\column;

use magein\seecms\page\Parse;
use magein\seecms\SeeLang;

/**
 * 操作列
 */
class ActionColumn extends Column
{
    protected $fixed = 'right';

    protected $template = 'action';

    protected $field = '__action__';

    protected $title = '操作';

    protected $width = 140;

    protected $extra = [];

    public static function init($field = null)
    {
        $self = parent::init();

        $width = 0;
        $btn = SeeLang::instance()->getGroup('btn');
        if (is_array($field)) {
            foreach ($field as $item) {
                if (is_string($item)) {
                    $item = (new Parse())->button($item);
                }
                if (empty($item)) {
                    continue;
                }

                $text = $btn[$item->getText()] ?? $item->getText();
                $item->setText($text);

                if (preg_match('/\w/', $text)) {
                    $unit = 12;
                } else {
                    $unit = 24;
                }

                $len = mb_strlen($item->getText());
                $width += $len * $unit;
                $self->extra[] = $item;
            }
        }

        $self->width = $width;
        $self->title = $btn['operation'] ?? $self->title;

        return $self;
    }

    public function getExtra(): array
    {
        return (new Parse())->btnTransfer($this->extra);
    }
}
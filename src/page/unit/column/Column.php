<?php

declare(strict_types=1);

namespace magein\seecms\page\unit\column;

use ArrayAccess;

abstract class Column implements ArrayAccess
{

    /**
     * @var string
     */
    protected $type = 'normal';

    /**
     * 字段名称
     * @var string
     */
    protected $field = '';

    /**
     * 列名称
     * @var string
     */
    protected $title = '';

    /**
     * 列宽度
     * @var int
     */
    protected $width = 0;

    /**
     * 列排序
     * @var bool
     */
    protected $sortable = false;

    /**
     * 固定列
     * @var string|bool
     */
    protected $fixed = false;

    /**
     * @var bool
     */
    protected $edit = false;

    /**
     * 列模板
     * @var string
     */
    protected $template = '';

    /**
     * @var array
     */
    protected $extra = [];

    public function offsetSet($offset, $value)
    {
        $this->$offset = $value;
    }

    public function offsetExists($offset)
    {
        return isset($this->$offset);
    }

    public function offsetGet($offset)
    {
        return $this->$offset ?? null;
    }

    public function offsetUnset($offset)
    {
        unset($this->$offset);
    }

    /**
     * @param $field
     * @return static|Column
     */
    public static function init($field = null)
    {
        $self = new static();
        if ($field && is_string($field)) {
            $self->field = $field;
        }
        return $self;
    }


    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @return string
     */
    public function getField(): string
    {
        return $this->field;
    }

    /**
     * @param string $field
     * @return Column
     */
    public function setField(string $field): Column
    {
        $this->field = $field;

        return $this;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     * @return Column
     */
    public function setTitle(string $title): Column
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @return bool|string
     */
    public function getFixed()
    {
        return $this->fixed;
    }

    /**
     * @param bool|string $fixed
     * @return Column
     */
    public function setFixed($fixed = false): Column
    {
        $this->fixed = $fixed;

        return $this;
    }

    /**
     * @return bool|string
     */
    public function getEdit(): bool
    {
        return $this->edit;
    }

    /**
     * @param bool|string $edit
     * @return $this
     */
    public function setEdit($edit = 'text'): Column
    {
        $this->edit = $edit;

        return $this;
    }

    /**
     * @return int
     */
    public function getWidth(): int
    {
        return $this->width;
    }

    /**
     * @param int $width
     * @return Column
     */
    public function setWidth(int $width): Column
    {
        $this->width = $width;

        return $this;
    }

    /**
     * @return string
     */
    public function getTemplate(): string
    {
        return $this->template;
    }

    /**
     * @param string $template
     * @return $this
     */
    public function setTemplate(string $template): Column
    {
        $this->template = $template;

        return $this;
    }

    /**
     * @return bool
     */
    public function getSortable(): bool
    {
        return $this->sortable;
    }

    /**
     * @param bool $sortable
     * @return $this
     */
    public function setSortable(bool $sortable): Column
    {
        $this->sortable = $sortable;

        return $this;
    }

    public function getExtra(): array
    {
        return $this->extra;
    }
}
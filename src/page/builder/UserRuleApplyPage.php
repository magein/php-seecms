<?php

declare(strict_types=1);

namespace magein\seecms\page\builder;

use magein\seecms\Factory;
use magein\seecms\library\Auth;
use magein\seecms\library\UserPermission;
use magein\seecms\library\UserRuleApply;
use magein\seecms\page\Builder;
use magein\seecms\page\Event;
use magein\seecms\page\unit\button\EditButton;
use magein\seecms\page\unit\control\InputControl;
use magein\seecms\page\unit\modal\Dialog;
use magein\seecms\page\unit\query\DatetimeQuery;
use magein\seecms\page\unit\query\SelectQuery;
use magein\seecms\SeeLang;
use magein\utils\Result;

class UserRuleApplyPage extends Builder
{
    protected $page_name = 'user_rule_apply';

    protected $table_name = 'seecms_user_rule_apply';

    protected $title = '权限申请列表';

    protected $dictionary = '__user_rule_apply';

    protected $left_toolbars = [];

    public function getQueries(): array
    {
        $users = Factory::table()->user->order('id desc')->column('nickname', 'id');

        return [
            SelectQuery::init('user_id')->setOptions($users)->setPlaceholder(SeeLang::user('select_user')),
            SelectQuery::init('status')->setOptions(UserRuleApply::translateStatus())->setValue(UserRuleApply::STATUS_PADDING)->setPlaceholder(SeeLang::rule('select_verify_status')),
            DatetimeQuery::init('created_at'),
        ];
    }

    public function getColumns(): array
    {
        return [
            'id.100',
            'user_id.100',
            'title',
            'url.300',
            'remark',
            'status_text',
            'reason',
            'audited_at.180',
            'audit_user',
            'created_at',
        ];
    }

    public function getActions(): array
    {
        return [
            EditButton::init('verify')
        ];
    }

    public function getControls(): array
    {
        return [
            InputControl::init('title')->setDisabled(),
            InputControl::init('url')->setDisabled(),
            InputControl::init('remark')->setDisabled(),
            ['status', UserRuleApply::translateStatus()],
            'reason.false',
        ];
    }

    public function getDialog(): Dialog
    {
        return Dialog::init('角色表单')->areaMiddle();
    }

    public function event(): Event
    {
        $event = new Event();

        $event->index->after = function ($data) {
            if ($data) {
                foreach ($data['data'] as &$item) {
                    $item['status_text'] = UserRuleApply::translateStatus($item['status']);
                }
                unset($item);
            }
            return $data;
        };

        $event->update->before = function ($data) {

            $id = $data['id'];
            $status = $data['status'];

            $user_apply = Factory::table()->user_rule_apply->find($id);
            if (empty($user_apply)) {
                return Result::error(SeeLang::rule('apply_not_found'));
            }

            if ($user_apply['status'] == UserRuleApply::STATUS_PASS) {
                return Result::error(SeeLang::rule('verify_passed'));
            }

            if ($user_apply['status'] == $status) {
                return Result::error(SeeLang::rule('verify_invalid'));
            }

            $url = $user_apply['url'];
            $rule = Factory::table()->rule->where('url', $url)->find();
            if (empty($rule)) {
                return Result::error(SeeLang::rule('not_found'));
            }

            $user_id = $user_apply['user_id'];
            $user_permission = UserPermission::init($user_id);

            $user_rule = $user_permission->rule();
            $user_rule[] = $rule['id'];

            // 增加用户权限
            $user_rule = array_unique($user_rule);
            $user_rule = implode(',', $user_rule);

            Factory::table()->user_permission->updateOrCreate(compact('user_id'), ['rule' => $user_rule]);

            $data = [];
            $data['id'] = $id;
            $data['status'] = $status;
            $data['reason'] = Factory::request()->post('reason');
            $data['audited_at'] = date('Y-m-d H:i:s');
            $data['audit_user'] = Auth::user()->uuid();

            return Result::success($data);
        };

        return $event;
    }
}
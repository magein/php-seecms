<?php

declare(strict_types=1);

namespace magein\seecms\page\builder;

use magein\seecms\Factory;
use magein\seecms\page\Builder;
use magein\seecms\page\Event;
use magein\seecms\page\unit\button\PageButton;
use magein\seecms\page\unit\control\TextareaControl;
use magein\seecms\page\unit\modal\Dialog;
use magein\seecms\page\unit\query\SelectQuery;
use magein\seecms\SeeLang;
use magein\utils\Result;

class UserRolePage extends Builder
{
    protected $page_name = 'user_role';

    protected $table_name = 'seecms_user_role';

    protected $title = '角色管理';

    protected $dictionary = '__user_role';

    public function getQueries(): array
    {
        $users = Factory::table()->user_role->column('name', 'id');

        return [
            SelectQuery::init('name')->setOptions($users)->setPlaceholder(SeeLang::user('role_name')),
        ];
    }

    public function getColumns(): array
    {
        return [
            'id.100',
            'name.140',
            'menu',
            'rule',
            'description',
            'created_at',
        ];
    }

    public function getActions(): array
    {
        return [
            PageButton::init('role_grant')
                ->setUrl(Factory::route('s/permission/roleGrant'))
                ->setDialog(Dialog::init('role_grant')->areaLarge()),
            'edit',
            'del',
        ];
    }

    public function getControls(): array
    {
        return [
            'name',
            TextareaControl::init('description')->setRequired(),
        ];
    }

    public function getDialog(): Dialog
    {
        return Dialog::init('role_grant')->areaMiddle();
    }

    public function event(): Event
    {
        $event = new Event();

        $event->delete->before = function () {

            $id = Factory::request()->delete('id');

            if ($id == 1 || $id == 2) {
                return Result::error(SeeLang::user('delete_role'));
            }

            return Result::success();
        };

        return $event;
    }
}
<?php

declare(strict_types=1);

/**
 * @user magein
 * @date 2024/2/21 15:58
 */

namespace magein\seecms\page\builder;

use magein\seecms\Factory;
use magein\seecms\library\Transfer;
use magein\seecms\page\Builder;
use magein\seecms\page\Event;
use magein\seecms\page\unit\column\ImageColumn;
use magein\seecms\page\unit\control\ImageControl;
use magein\seecms\page\unit\modal\Dialog;
use magein\seecms\page\unit\query\SelectQuery;
use magein\seecms\page\unit\query\TextQuery;
use magein\seecms\page\unit\toolbar\PageToolbar;
use magein\seecms\SeeLang;

class AttachmentPage extends Builder
{
    protected $page_name = 'attachment';

    protected $table_name = 'seecms_attachment';

    protected $dictionary = '__attachment';

    public function getLeftToolbars(): array
    {
        return [
            PageToolbar::init(SeeLang::btn('upload'))->setUrl(Factory::route('s/attachment/form'))->setDialog(Dialog::init(SeeLang::attachment('upload'))->areaMiddle())
        ];
    }

    public function getQueries(): array
    {
        return [
            'id',
            TextQuery::init('filename', 'like'),
            SelectQuery::init('ext')->setOptions(['png' => 'png', 'jpg' => 'jpg', 'jpeg' => 'jpeg', 'gif' => 'gif']),
        ];
    }

    public function getColumns(): array
    {
        return [
            'id.80',
            ImageColumn::init('thumb'),
            'filename.240',
            'ext',
            'size_text',
            'driver',
            'mime',
            'created_at',
        ];
    }

    public function getActions(): array
    {
        return [
            'read',
            'del',
        ];
    }

    public function getControls(): array
    {
        return [
            ImageControl::init('filepath')->setLabel(SeeLang::attachment('image'))->setDisk('attachment')->setLimit(20)->setChoose(),
        ];
    }

    public function getDialog(): Dialog
    {
        return Dialog::init(SeeLang::attachment('upload'))->areaMiddle();
    }

    public function event(): \magein\seecms\page\Event
    {
        $event = new Event();
        $event->index->after = function ($data) {
            foreach ($data['data'] as &$item) {
                $item['size_text'] = Transfer::filesizeFormatter($item['size']);
            }
            unset($item);
            return $data;
        };

        return $event;
    }
}
<?php

declare(strict_types=1);

namespace magein\seecms\page\builder;

use magein\seecms\Factory;
use magein\seecms\library\Menu;
use magein\seecms\library\TreeSelect;
use magein\seecms\page\Builder;
use magein\seecms\page\unit\column\TextColumn;
use magein\seecms\page\unit\control\InputControl;
use magein\seecms\page\unit\control\RadioControl;
use magein\seecms\page\unit\control\TextareaControl;
use magein\seecms\SeeLang;

class MenuPage extends Builder
{
    protected $page_name = 'menu';

    protected $table_name = 'seecms_menu';

    protected $title = '菜单列表';

    protected $dictionary = '__menu';

    protected $tree = true;

    public function getColumns(): array
    {
        return [
            'id.100',
            TextColumn::init('icon')->setTemplate('icon')->setWidth(80),
            'title.240',
            'name.280',
            'type_text',
            'sort',
            'status',
            'created_at',
        ];
    }

    public function getControls(): array
    {
        $records = Factory::table()->menu->select();
        $tree = new TreeSelect($records);
        $options = $tree->options();

        return [
            ['parent_id', 'select', $options],
            'title',
            InputControl::init('url')->setDescription(SeeLang::menu('redirect_url_tip')),
            InputControl::init('icon'),
            RadioControl::init('type')->setOptions(Menu::translateType())->setValue(1),
            RadioControl::init('open_type')->setOptions(Menu::translateOpenType())->setDescription(SeeLang::menu('open_type_tip'))->setValue(Menu::OPEN_TYPE_IFRAME),
            InputControl::init('remark')->setRequired(),
            TextareaControl::init('extent')->setRequired(),
            InputControl::init('sort')->setValue(999),
            'status',
        ];
    }
}
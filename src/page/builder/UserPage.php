<?php

declare(strict_types=1);

namespace magein\seecms\page\builder;

use magein\seecms\Factory;
use magein\seecms\page\Builder;
use magein\seecms\page\Event;
use magein\seecms\page\unit\button\PageButton;
use magein\seecms\page\unit\column\ImageColumn;
use magein\seecms\page\unit\control\ImageControl;
use magein\seecms\page\unit\control\InputControl;
use magein\seecms\page\unit\control\PasswordControl;
use magein\seecms\page\unit\modal\Dialog;
use magein\seecms\page\unit\query\SelectQuery;
use magein\seecms\SeeLang;
use magein\utils\Result;

class UserPage extends Builder
{
    protected $page_name = 'user';

    protected $table_name = 'seecms_user';

    protected $title = '用户列表';

    protected $dictionary = '__user';

    public function getQueries(): array
    {
        return [
            'username',
            'nickname',
            'phone',
            SelectQuery::init('status')->setOptions(Factory::extension()->user->statusOptions()),
        ];
    }

    public function getActions(): array
    {
        return [
            PageButton::init('grant')->setUrl(Factory::route('s/permission/userGrant'))->setDialog(Dialog::init('user_grant')->areaLarge()),
            'read',
            'edit',
            'del'
        ];
    }

    public function getColumns(): array
    {
        return [
            'id',
            ImageColumn::init('avatar'),
            'username',
            'nickname',
            'phone',
            'email',
            'status',
            'last_login_time',
            'last_login_ip',
            'created_at',
        ];
    }

    public function getShows(): array
    {
        return [
            'nickname',
            'avatar.false',
            InputControl::init('phone')->setPrefix('layui-icon-cellphone'),
            InputControl::init('email')->setPrefix('layui-icon-email'),
            'created_at',
            'updated_at'
        ];
    }

    public function getCreates(): array
    {
        return [
            InputControl::init('username')->setDescription(SeeLang::user('username_tip')),
            PasswordControl::init('password')->setDescription(SeeLang::user('password_tip')),
            PasswordControl::init('confirm_password')->setDescription(SeeLang::user('confirm_password_tip')),
            InputControl::init('nickname')->setDescription(SeeLang::user('nickname_tip')),
            ImageControl::init('avatar')->setRequired(),
            InputControl::init('phone')->setRequired()->setPrefix('layui-icon-cellphone'),
            InputControl::init('email')->setRequired()->setPrefix('layui-icon-email'),
        ];
    }

    public function getControls(): array
    {
        return [
            'nickname',
            PasswordControl::init('new_password')->setLabel(SeeLang::user('new_password'))->setDescription(SeeLang::user('new_password_tip'))->setRequired(),
            ImageControl::init('avatar')->setRequired()->setChoose(),
            InputControl::init('phone')->setPrefix('layui-icon-cellphone'),
            InputControl::init('email')->setPrefix('layui-icon-email'),
        ];
    }

    public function getDialog(): Dialog
    {
        return Dialog::init('user_create')->areaMiddle();
    }

    public function event(): Event
    {
        $event = new Event();
        $event->delete->before = function () {
            $id = Factory::request()->delete('id');
            if ($id == 1) {
                return Result::error(SeeLang::user('delete_root'));
            }
            return Result::success();
        };

        return $event;
    }
}
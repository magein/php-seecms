<?php

declare(strict_types=1);

namespace magein\seecms\page\builder;

use magein\seecms\Factory;
use magein\seecms\page\Builder;
use magein\seecms\page\unit;
use magein\seecms\page\unit\control\InputControl;
use magein\seecms\page\unit\modal\Dialog;
use magein\seecms\page\unit\query\TextQuery;
use magein\seecms\page\unit\toolbar\PageToolbar;
use magein\seecms\SeeLang;

class RulePage extends Builder
{
    protected $page_name = 'rule';

    protected $table_name = 'seecms_rule';

    protected $title = '权限管理';

    protected $dictionary = '__rule';

    public function getLeftToolbars(): array
    {
        return [
            'create',
            PageToolbar::init('resource_rule')->setUrl(Factory::route('s/permission/resourceRule'))->setIcon('layui-icon layui-icon-console')->setDialog(Dialog::init('resource_rule')->areaMiddle()),
            'del'
        ];
    }

    public function getQueries(): array
    {
        return [
            TextQuery::init('title')->setExpressLike(),
            TextQuery::init('url')->setExpressLike(),
        ];
    }

    public function getColumns(): array
    {
        return [
            'id',
            'title',
            'url',
            'description',
            'created_at'
        ];
    }

    public function getCreates(): array
    {
        return [
            InputControl::init('group')->setDescription(SeeLang::rule('group_tip')),
            InputControl::init('title')->setDescription(SeeLang::rule('title_tip')),
            InputControl::init('url')->setDescription(SeeLang::rule('url_tip')),
            'description.false',
        ];
    }

    public function getControls(): array
    {
        return [
            InputControl::init('group')->setDescription(SeeLang::rule('group_tip')),
            InputControl::init('title')->setDescription(SeeLang::rule('title_tip')),
            InputControl::init('url')->setDescription(SeeLang::rule('url_tip')),
            'description.false',
        ];
    }

    public function getDialog(): Dialog
    {
        return Dialog::init('rule_create')->areaMiddle();
    }

    public function beforeInsert()
    {

    }
}
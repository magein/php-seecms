<?php

declare(strict_types=1);

namespace magein\seecms\page\builder;

use magein\seecms\Factory;
use magein\seecms\page\Builder;
use magein\seecms\page\Event;
use magein\seecms\page\unit\query\DatetimeQuery;
use magein\seecms\page\unit\query\SelectQuery;
use magein\seecms\SeeLang;

class UserLoginPage extends Builder
{

    protected $page_name = 'user_login';

    protected $table_name = 'seecms_user_login';

    protected $title = '登录日志';

    protected $dictionary = '__user_login';

    protected $left_toolbars = [];

    protected $actions = [];

    public function getQueries(): array
    {
        $users = Factory::table()->user->order('id desc')->column('nickname', 'id');

        return [
            SelectQuery::init('user_id')->setOptions($users)->setPlaceholder(SeeLang::user('select_user')),
            DatetimeQuery::init('created_at'),
        ];
    }

    public function getColumns(): array
    {
        return [
            'id',
            'user_id',
            'ip',
            'browser',
            'device',
            'platform',
            'is_mobile_text',
            'status_text',
            'reason',
            'user_agent',
            'created_at',
        ];
    }

    public function getShows(): array
    {
        return [
            'nickname',
            'avatar.false',
            'phone.false',
            'email.false',
        ];
    }

    public function getCreates(): array
    {
        return [
            'username',
            'password',
            'confirm_password',
            'nickname',
            'avatar.false',
            'phone.false',
            'email.false',
        ];
    }

    public function getControls(): array
    {
        return [];
    }

    public function event(): Event
    {
        $event = new Event();

        $event->index->after = function ($data) {
            if ($data) {
                foreach ($data['data'] as &$item) {
                    $item['status_text'] = $item['status'] ? SeeLang::login('success') : SeeLang::login('fail');
                    $item['is_mobile_text'] = $item['is_mobile'] ? SeeLang::switch('yes') : SeeLang::switch('no');
                }
                unset($item);
            }
            return $data;
        };

        return $event;
    }
}
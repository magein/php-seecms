<?php

declare(strict_types=1);

namespace magein\seecms\page;

class Provider
{
    /**
     * 系统配置
     * @var string[]
     */
    protected $pages = [];

    public function getPages(): array
    {
        return $this->pages;
    }
}
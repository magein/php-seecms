<?php
declare(strict_types=1);

namespace magein\seecms\controller;

use magein\seecms\Factory;
use magein\seecms\library\Mailer;
use magein\seecms\library\Operate;
use magein\seecms\library\View;
use magein\seecms\response\HtmlResponse;
use magein\seecms\response\JsonResponse;
use magein\utils\Validator;


class General
{
    /**
     * @rule_title 常规配置页面展示
     * @return HtmlResponse
     */
    public function setting(): HtmlResponse
    {
        $setting_group = [];

        $setting = Factory::table()->setting->select();
        foreach ($setting as $item) {
            $setting_group[$item['group']][$item['name']] = $item['value'];
        }

        return View::fetch('general/config', [
            'setting' => $setting_group,
            'exec' => function_exists('exec'),
            'php_mailer' => class_exists('PHPMailer\PHPMailer\PHPMailer')
        ]);
    }

    /**
     * @rule_title 常规配置保存
     * @return JsonResponse
     */
    public function saveSetting(): JsonResponse
    {
        $group = Factory::request()->post('group');
        $setting = Factory::request()->post('setting');

        if (empty($group)) {
            return JsonResponse::error(Operate::required('group'));
        }

        if (empty($setting) || !is_array($setting)) {
            return JsonResponse::error(Operate::isArray('setting'));
        }

        Factory::extension()->general->save($group, $setting);

        return JsonResponse::saveSuccess();
    }

    /**
     * @rule_title 发送测试邮件
     * @return JsonResponse
     */
    public function emailTest(): JsonResponse
    {
        $email = Factory::request()->post('email');
        if (empty($email)) {
            return JsonResponse::error(Operate::required('email'));
        }

        if (!Validator::email($email)) {
            return JsonResponse::error(Operate::email());
        }

        $now = date('Y-m-d H:i:s');

        $html = <<<EOF
<h3 style="text-indent: 2em; text-align: start;">Seecms 测试邮件</h3>
<p> &nbsp; &nbsp; &nbsp; &nbsp;<span style="color: rgb(191, 191, 191);">$now</span></p>
<hr/>
<p>【 Seecms：简约而不简单的内容管理系统】</p>
<p style="text-indent: 2em;">使用php构建的内容管理系统。</p>
<p style="text-indent: 2em; text-align: start;">在数字化时代，内容管理系统的选择直接关系到网站或应用的灵活性与效率。Seecms，作为一款专为追求高效与易用性而设计的内容建立系统，以其简洁的界面、强大的功能以及灵活的扩展性，在众多CMS中脱颖而出，成为众多企业及个人用户的理想之选。</p>
<p style="text-align: start;"><strong>【基础功能】</strong></p>
<p style="text-indent: 2em; text-align: start;">基础菜单，直观操作：EasyCMS内置了清晰直观的基础菜单结构，无论是新手还是资深用户，都能迅速上手，轻松管理网站内容。</p>
<p style="text-indent: 2em; text-align: start;">权限管理，安全无忧：系统支持精细化的权限设置，管理员可根据不同角色分配访问权限，确保数据安全的同时，提升团队协作效率。没有权限的时候可以进行申请。</p>
<p style="text-indent: 2em; text-align: start;">登录设置，灵活便捷：提供多种登录方式及安全验证机制，包括密码登录、验证码验证、微信扫码登录、app扫码登录，有效防止非法入侵。同时，支持自定义登录页面，让每一次登录都更加个性化与便捷。</p>
<p style="text-indent: 2em; text-align: start;">站点设置，随心所欲：轻松配置网站名称、Logo、域名等基本信息，支持SEO优化设置，助力网站在搜索引擎中脱颖而出。</p>
<p style="text-indent: 2em; text-align: start;">邮件&amp;短信设置，即时通讯：集成邮件与短信发送功能，无论是用户注册验证、找回密码，还是重要通知推送，都能即时触达用户，提升用户体验。支持第三方邮件及短信服务商接入，满足多样化需求。</p>
<p style="text-indent: 2em; text-align: start;">微信公众号设置，拓宽渠道：无缝对接微信公众号，实现内容一键同步至公众号，拓宽内容传播渠道。支持自定义菜单、自动回复等功能，助力企业构建全方位营销体系。</p>
<p style="text-indent: 2em; text-align: start;">上传设置，灵活高效：支持多种文件类型上传，包括图片、视频、文档等，满足多样化内容展示需求。提供文件大小、格式等限制设置，确保上传内容的质量与安全。</p>
<p style="text-indent: 2em; text-align: start;">数据库备份，数据安全：定期自动备份数据库，确保数据不丢失。</p>
<p style="text-align: start;"><strong>【结语】</strong></p>
<p style="text-indent: 2em; text-align: start;">Seecms，以简约之名，行高效之实。它不仅是一款内容建立系统，更是您数字化转型路上的得力助手。无论您是想要快速搭建个人博客，还是为企业打造专业网站，EasyCMS都能以其灵活的扩展性满足您的需求。立即体验EasyCMS，开启您的内容管理新篇章！</p>
EOF;

        $config = Factory::extension()->general->smtp;
        $mail = new Mailer($config);
        $result = $mail->html($email, 'Seecms邮件测试', $html);

        return JsonResponse::result($result);
    }
}
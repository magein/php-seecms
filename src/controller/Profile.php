<?php

declare(strict_types=1);
/**
 * @user magein
 * @date 2024/1/23 10:44
 */

namespace magein\seecms\controller;

use magein\seecms\Factory;
use magein\seecms\library\Auth;
use magein\seecms\library\Operate;
use magein\seecms\library\View;
use magein\seecms\response\HtmlResponse;
use magein\seecms\response\JsonResponse;
use magein\seecms\SeeLang;

class Profile
{
    /**
     * @return HtmlResponse
     */
    public function index(): HtmlResponse
    {
        $userinfo = Factory::table()->user->where('id', Auth::user()->uuid())->find();

        return View::fetch('profile/index', [
            'userinfo' => $userinfo,
            'permission' => [
                'roles' => Auth::user()->role()
            ]
        ]);
    }

    /**
     * @return JsonResponse
     */
    public function login(): JsonResponse
    {
        $data = Factory::table()->user_login->where('user_id', Auth::user()->uuid())->order('id desc')->paginate();

        if ($data) {
            foreach ($data['data'] as &$item) {
                $item['status_text'] = $item['status'] ? SeeLang::login('success') : SeeLang::login('fail');
                $item['is_mobile_text'] = $item['is_mobile'] ? SeeLang::switch('yes') : SeeLang::switch('no');
            }
            unset($item);
        }

        return JsonResponse::success($data);
    }

    public function avatar(): JsonResponse
    {
        $result = Auth::user()->setAvatar();

        return JsonResponse::result($result);
    }

    /**
     * @return JsonResponse
     */
    public function setPassword(): JsonResponse
    {
        $result = Auth::user()->setPassword();

        return JsonResponse::result($result);
    }

    public function verifyPassword(): JsonResponse
    {
        $result = Auth::user()->verifyPassword();

        return JsonResponse::result($result);
    }

    /**
     * @return JsonResponse
     */
    public function updateInfo(): JsonResponse
    {
        $result = Auth::user()->updateInfo();

        return JsonResponse::result($result);
    }

    public function message()
    {

        $user_id = Auth::user()->uuid();

        if (Factory::request()->isAjax()) {

            $group = Factory::request()->get('group', '');
            $is_read = Factory::request()->get('is_read');

            $user_message = Factory::table()->user_message;
            $user_message = $user_message->where('user_id', $user_id);

            if ($is_read) {
                if ($is_read == 1) {
                    $user_message = $user_message->whereNotNull('read_at');
                } else {
                    $user_message = $user_message->whereNull('read_at');
                }
            }

            if ($group && $group != 'all') {
                $user_message = $user_message->where('group', $group);
            }

            $data = $user_message->order('id desc')->paginate();

            $groups = Factory::config()->message->group();

            foreach ($data['data'] as &$item) {
                $group = $item['group'] ?? '';
                $group_text = $groups[$group] ?? '';
                if (empty($group_text)) {
                    if ($group == 'system') {
                        $group_text = SeeLang::message('group_sys');
                    } elseif ($group == 'security') {
                        $group_text = SeeLang::message('group_security');
                    }
                }

                $item['group_text'] = $group_text;
            }
            unset($item);

            return JsonResponse::success($data);
        }

        $groups = Factory::table()->user_message->where('user_id', $user_id)
            ->whereNull('read_at')
            ->field(['count(id) as total', 'group'])
            ->group('group')
            ->select();

        $total = 0;
        $group_list = [];
        foreach ($groups as $item) {
            $total += $item['total'];
            $group_list[$item['group']] = $item['total'];
        }
        $group_list['all'] = $total;

        return View::fetch('profile/message', [
            'group' => $group_list
        ]);
    }

    public function messageGet(): JsonResponse
    {
        $id = Factory::request()->post('id');

        $record = Factory::table()->user_message->find($id);

        if ($record['user_id'] != Auth::user()->uuid()) {
            return JsonResponse::error(Operate::notFoundData());
        }

        if ($record && empty($record['read_at'])) {
            Factory::table()->user_message->where('id', $id)->data('read_at', date('Y-m-d H:i:s'))->update();
        }

        return JsonResponse::success($record);
    }

    public function messageRead(): JsonResponse
    {
        $ids = Factory::request()->post('id');

        if (empty($ids)) {
            return JsonResponse::error(Operate::required());
        }

        if ($ids == 'all') {

            Factory::table()->user_message
                ->where('user_id', Auth::user()->uuid())
                ->whereNull('read_at')
                ->data('read_at', date('Y-m-d H:i:s'))
                ->update();

        } else {

            if (is_string($ids)) {
                $ids = explode(',', $ids);
            }

            Factory::table()->user_message
                ->where('user_id', Auth::user()->uuid())
                ->whereNull('read_at')
                ->whereIn('id', $ids)
                ->data('read_at', date('Y-m-d H:i:s'))
                ->update();
        }

        return JsonResponse::success();
    }

    public function messageDelete(): JsonResponse
    {
        $ids = Factory::request()->post('id');

        if (empty($ids)) {
            return JsonResponse::error('参数错误');
        }

        if ($ids == 'all') {
            Factory::table()->user_message
                ->where('user_id', Auth::user()->uuid())
                ->data('deleted_at', date('Y-m-d H:i:s'))
                ->update();
        } else {
            if (is_string($ids)) {
                $ids = explode(',', $ids);
            }
            Factory::table()->user_message
                ->where('user_id', Auth::user()->uuid())
                ->whereIn('id', $ids)
                ->data('deleted_at', date('Y-m-d H:i:s'))
                ->update();
        }

        return JsonResponse::success();
    }

    public function wechatBind()
    {
        $qrcode = Auth::wxOfficialAccount()->qrcode;
        $flag = 'bind_' . Auth::user()->uuid() . '_' . md5(uniqid());
        $result = $qrcode->temporary($flag, 600);
        $data['ticket'] = $qrcode->url($result['ticket']);

        return JsonResponse::success($data);
    }
}
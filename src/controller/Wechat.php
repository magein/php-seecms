<?php

namespace magein\seecms\controller;

use EasyWeChat\Kernel\Exceptions\InvalidConfigException;
use GuzzleHttp\Exception\GuzzleException;
use magein\seecms\db\SeeDbException;
use magein\seecms\Factory;
use magein\seecms\library\Auth;
use magein\seecms\library\Operate;
use magein\seecms\library\Transfer;
use magein\seecms\library\Utils;
use magein\seecms\library\View;
use magein\seecms\page\Parse;
use magein\seecms\page\unit\query\SelectQuery;
use magein\seecms\response\HtmlResponse;
use magein\seecms\response\JsonResponse;
use magein\seecms\SeeException;
use magein\seecms\SeeLang;

class Wechat
{
    /**
     * @rule_title 公众号设置
     * @return HtmlResponse
     */
    public function setting(): HtmlResponse
    {
        $setting = Factory::table()->setting->where('group', 'wechat')->column('value', 'name');

        $lang = SeeLang::instance()->global('__wechat_material');

        return View::fetch('wechat/index', [
            'lang' => $lang,
            'setting' => $setting,
            'material_type' => Transfer::wechatMaterialType(),
            'queries' => (new Parse(SeeLang::instance()->global('__wechat_material')))
                ->query(
                    [
                        'filename',
                        SelectQuery::init('type')->setOptions(Transfer::wechatMaterialType()),
                        'created_at'
                    ]
                ),
        ]);
    }

    /**
     * @rule_title 素材库
     */
    public function materialList(): JsonResponse
    {
        $where = Transfer::pageQuery(['filename', 'type', 'created_at'], Factory::request()->get());
        $data = Factory::table()->wechat_material->where($where)->order('id desc')->paginate();

        return JsonResponse::success($data);
    }

    /**
     * 保存消息中包含链接或者其他字符，所以不能使用json保存，使用serialize
     * @rule_title 微信公众号设置
     * @return JsonResponse
     */
    public function saveConfig(): JsonResponse
    {
        $group = 'wechat';
        $setting = Factory::request()->post('setting');

        if (empty($setting) || !is_array($setting)) {
            return JsonResponse::error(Operate::isArray('setting'));
        }

        Factory::extension()->general->save($group, $setting);

        return JsonResponse::saveSuccess();
    }

    /**
     * 素材预览
     * @rule_title 素材预览
     */
    public function materialPreview()
    {
        $id = Factory::request()->get('id');
        $data = Factory::table()->wechat_material->find($id);
        if (empty($data)) {
            echo 'No material information available';
        } else {
            $root = Factory::config()->wechat->materialRoot();
            $filepath = $root . DIRECTORY_SEPARATOR . $data['type'] . DIRECTORY_SEPARATOR . $data['filename'];
            if (is_file($filepath)) {
                header('Content-Type: ' . $data['mime']);
                readfile($filepath);
            } else {
                echo 'The material file does not exist';
            }
        }
        exit();
    }

    /**
     * 素材删除
     * @rule_title 素材删除
     */
    public function materialRemove()
    {
        $id = Factory::request()->post('id');

        $data = Factory::table()->wechat_material->find($id);

        if (empty($data)) {
            return JsonResponse::error(Operate::notFoundData());
        }

        $expire_time = $data['expire_time'];

        if ($expire_time) {
            return JsonResponse::error('Temporary materials do not need to be deleted');
        }

        try {

            Auth::wxOfficialAccount()->material->delete($data['media_id']);

            Factory::table()->wechat_material->delete($id);

        } catch (InvalidConfigException|GuzzleException$exception) {

        }

        return JsonResponse::success(true, Operate::delSuccess() . '1');
    }

    /**
     * @rule_title 重置微信公众号菜单
     * @return JsonResponse
     */
    public function resetMenu(): JsonResponse
    {
        $wechat = Factory::extension()->general->wechat;

        try {

            $menus = $wechat['menu'];
            if (is_string($menus)) {
                $menus = json_decode($menus, true);
            }

            $result = Auth::wxOfficialAccount()->menu->create($menus);

            if ($result['errcode']) {
                return JsonResponse::error($result['errmsg']);
            }

        } catch (GuzzleException|InvalidConfigException $exception) {
            return JsonResponse::error($exception->getMessage());
        }

        return JsonResponse::success(true, 'WeChat menu reset successful');
    }

    /**
     * @rule_title 上传素材
     * @throws SeeException
     * @throws SeeDbException
     */
    public function uploadMaterial(): JsonResponse
    {
        $long = Factory::request()->post('long');
        $type = Factory::request()->post('type');
        $remark = Factory::request()->post('remark');

        $root = Factory::config()->wechat->materialRoot();
        $url = Factory::config()->wechat->materialUrl();
        if (empty($root)) {
            return JsonResponse::error('Please add in the configuration file : wechat.material.root');
        }
        if (empty($url)) {
            return JsonResponse::error('Please add in the configuration file : wechat.material.url');
        }
        $url = '/' . $url . '/' . $type;

        $save_path = Utils::ckDir($root, $type);

        $concat = function ($size, $ext) {
            return compact('size', 'ext');
        };

        $validate = [
            'image' => $concat(10485760, ['bmp', 'png', 'jpeg', 'jpg', 'gif']),
            'thumb' => $concat(65536, ['jpg']),
            'voice' => $concat(2097152, ['mp3', 'wma', 'wav', 'amr']),
            'video' => $concat(10485760, ['mp4']),
        ];

        $file = $_FILES['file'];
        $filename = $file['name'];
        $mime = $file['type'];
        $size = $file['size'];
        $ext = pathinfo($filename, PATHINFO_EXTENSION);

        $valid = $validate[$type] ?? null;
        if (empty($valid)) {
            return JsonResponse::error(SeeLang::wechat('material_type_error'));
        }

        if ($size > $valid['size']) {
            return JsonResponse::error(SeeLang::upload('error_size'));
        }

        if (!in_array($ext, $valid['ext'])) {
            return JsonResponse::error(SeeLang::upload('error_filetype'));
        }

        if (empty($file['tmp_name'])) {
            return JsonResponse::error(SeeLang::upload('error_ini'));
        }

        $save_path = $save_path . '/' . $filename;

        try {

            $app = Auth::wxOfficialAccount();

            $class = $long ? $app->material : $app->media;
            $move_result = move_uploaded_file($file['tmp_name'], $save_path);

            if ($move_result) {
                if ($type === 'image') {
                    $result = $class->uploadImage($save_path);
                } elseif ($type === 'thumb') {
                    $result = $class->uploadThumb($save_path);
                } elseif ($type === 'voice') {
                    $result = $class->uploadVoice($save_path);
                } elseif ($type === 'video') {
                    $title = Factory::request()->post('video_title');
                    $description = Factory::request()->post('video_desc');
                    $result = $class->uploadVideo($save_path, $title, $description);
                }


                if ($result['errcode'] ?? '') {
                    return JsonResponse::error($result['errmsg'] ?? 'An error occurred');
                }
            }
        } catch (\Exception|GuzzleException $exception) {
            return JsonResponse::error($exception->getMessage());
        }

        if (isset($result['media_id'])) {
            Factory::table()->wechat_material->save(
                [
                    'filename' => $filename,
                    'filepath' => $url . '/' . $filename,
                    'mime' => $mime,
                    'ext' => $ext,
                    'size' => $size,
                    'type' => $type,
                    'media_id' => $result['media_id'],
                    'url' => $result['url'] ?? '',
                    'expire_time' => $long ? null : date('Y-m-d H:i:s', strtotime('+3 day')),
                    'remark' => $remark
                ]
            );
        }

        return JsonResponse::saveSuccess();
    }
}
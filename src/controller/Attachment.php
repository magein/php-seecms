<?php
declare(strict_types=1);

namespace magein\seecms\controller;

use magein\seecms\Factory;
use magein\seecms\library\Auth;
use magein\seecms\library\Image;
use magein\seecms\library\View;
use magein\seecms\response\HtmlResponse;
use magein\seecms\response\JsonResponse;
use magein\seecms\SeeException;
use magein\seecms\SeeLang;

class Attachment
{
    /**
     * @return HtmlResponse
     */
    public function form(): HtmlResponse
    {
        return View::fetch('attachment/form',
            [
                'water' => Factory::config()->upload->water()
            ]
        );
    }

    /**
     * 附件管理
     * @return HtmlResponse|JsonResponse
     */
    public function choose()
    {
        $limit = Factory::request()->get('limit');
        $ext = Factory::request()->get('ext', 'png|jpg');
        $exts = explode('|', $ext);

        $json = Factory::request()->get('json');

        if ($json) {
            $records = Factory::table()->attachment->where('ext', 'in', $exts)->paginate();
            return JsonResponse::success($records);
        }

        $total = Factory::table()->attachment->where('ext', 'in', $exts)->count();

        return View::fetch('attachment/choose', [
            'total' => $total,
            'ext' => $ext,
            'limit' => $limit
        ]);
    }

    /**
     * @return JsonResponse
     */
    public function upload(): JsonResponse
    {
        $upload = Factory::extension()->upload;
        $record = Factory::table()->attachment->where('md5', $upload->md5())->find();
        if ($record && $record['filepath']) {
            return JsonResponse::error('请不要重复上传,图片索引【' . $record['id'] . '】');
        }

        // 移动文件
        $filepath = $upload->move();

        $config = Factory::config()->upload;
        $root = $config->root();
        $full_path = $root . $filepath;
        $filename = pathinfo($filepath, PATHINFO_BASENAME);
        // 缩略图路径
        $thumb_dir = $config->directoryThumb();
        $thumb_save_name = $thumb_dir . '/' . $filename;

        try {
            if (!is_dir($root . $thumb_dir) && !mkdir($root . $thumb_dir, 0777, true)) {
                return JsonResponse::error('创建缩略图保存目录失败');
            }

            $image = new Image($full_path);
            $image->thumb();
            $image->save($root . $thumb_save_name);
        } catch (SeeException $exception) {

        }

        // 创建水印
        $water = Factory::request()->post('water');

        if ($water) {
            try {
                // 水印路径
                $water_dir = $config->directoryWater();
                if (!is_dir($root . $water_dir) && !mkdir($root . $water_dir, 0777, true)) {
                    return JsonResponse::error('创建水印保存目录失败');
                }
                // 水印保存
                $water_save_name = $water_dir . '/' . $filename;

                // 创建图片水印
                $image_index = Factory::request()->post('image');
                // 文字水印
                $text = Factory::request()->post('text');

                if ($image_index != -1) {
                    $source = $config->waterImageList()[$image_index] ?? null;
                    if ($source) {
                        $image = new Image($full_path);
                        $image->water($config->waterImageRoot() . $source);
                        if ($image->save($root . $water_save_name)) {
                            $filepath = $water_save_name;
                        }
                    }
                } elseif ($text) {
                    $image = new Image($full_path);
                    $image->text($text);
                    if ($image->save($root . $water_save_name)) {
                        $filepath = $water_save_name;
                    }
                }
            } catch (SeeException $exception) {

            }
        }
        
        $data['user_id'] = Auth::user()->uuid();
        $data['filepath'] = $filepath;
        $data['filename'] = $upload->getOriginalName();
        $data['mime'] = $upload->getOriginalMime();
        $data['ext'] = $upload->getOriginalExtension();
        $data['size'] = $upload->getSize();
        $data['md5'] = $upload->md5();
        $data['sha1'] = $upload->sha1();
        $data['thumb'] = $thumb_save_name;
        $data['driver'] = 'local';

        Factory::table()->attachment->insert($data);

        return JsonResponse::success(['url' => $filepath], SeeLang::upload('success'));
    }
}
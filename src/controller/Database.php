<?php
declare(strict_types=1);

/**
 * @user magein
 * @date 2024/1/18 17:30
 */

namespace magein\seecms\controller;

use magein\seecms\Factory;
use magein\seecms\library\Backup;
use magein\seecms\library\Mailer;
use magein\seecms\library\Operate;
use magein\seecms\library\Transfer;
use magein\seecms\library\View;
use magein\seecms\page\Parse;
use magein\seecms\response\FileResponse;
use magein\seecms\response\HtmlResponse;
use magein\seecms\response\JsonResponse;
use magein\seecms\SeeLang;


class Database
{
    /**
     * @rule_title 数据库备份
     * @return HtmlResponse
     */
    public function setting(): HtmlResponse
    {
        $tables = Factory::config()->backup->tables();

        $setting = Factory::extension()->general->backup;

        return View::fetch('database/index', [
            'tables' => $tables,
            'setting' => $setting,
            'zip' => extension_loaded('zip'),
            'queries' => Parse::global('__database_backup')->query(['name', 'created_at']),
        ]);
    }

    /**
     * @rule_title 备份日志
     */
    public function log(): JsonResponse
    {
        $where = Transfer::pageQuery(['name', 'created_at'], Factory::request()->get());
        $data = Factory::table()->database_backup->where($where)->paginate();
        $list = $data['data'];
        foreach ($list as $key => $item) {
            $item['filename'] = pathinfo($item['filepath'], PATHINFO_BASENAME);
            $item['filesize_text'] = Transfer::filesizeFormatter($item['filesize']);
            $item['send_result_text'] = Backup::translateSendResult($item['send_result']);
            $list[$key] = $item;
        }
        $data['data'] = $list;

        return JsonResponse::success($data);
    }

    /**
     * @rule_title 备份下载
     */
    public function download()
    {
        $id = Factory::request()->get('id');

        if (empty($id)) {
            return new HtmlResponse(Operate::required());
        }

        $record = Factory::table()->database_backup->find($id);

        if (empty($record)) {
            return new HtmlResponse(Operate::notFoundData());
        }

        $root_path = Factory::config()->backup->root();
        $filepath = $root_path . '/' . $record['filepath'];

        if (is_file($filepath)) {
            return (new FileResponse($filepath))->name($filepath);
        }

        return new HtmlResponse(SeeLang::general('not_found_file'));
    }

    /**
     * @rule_title 执行备份
     * @return JsonResponse
     */
    public function backup(): JsonResponse
    {
        $tables = Factory::request()->post('tables');

        if (empty($tables)) {
            return JsonResponse::error(Operate::required('tables'));
        }

        $backup = new Backup();
        $backup->setTables($tables);
        $backup->setMailer(new Mailer());
        $result = $backup->run();

        return JsonResponse::result($result);
    }

    /**
     * @rule_title 备份发送到邮箱
     * @return JsonResponse
     */
    public function send(): JsonResponse
    {
        $id = Factory::request()->post('id');

        if (empty($id)) {
            return JsonResponse::error(Operate::required());
        }

        $record = Factory::table()->database_backup->find($id);

        if (empty($record)) {
            return JsonResponse::error(Operate::notFoundData());
        }

        $backup = Factory::extension()->general->backup;

        $receive_email = $backup['receive_email'];
        if (empty($receive_email)) {
            return JsonResponse::error(Operate::required('receive_email'));
        }
        $receive_email = explode(',', $receive_email);

        $backup = new Backup();
        $backup->setReceiveEmail($receive_email);
        $backup->setMailer(new Mailer(Factory::extension()->general->smtp));
        $result = $backup->sendMail($record);

        return JsonResponse::result($result);
    }
}
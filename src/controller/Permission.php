<?php

declare(strict_types=1);

namespace magein\seecms\controller;

use magein\seecms\db\SeeDbException;
use magein\seecms\Factory;
use magein\seecms\library\Menu;
use magein\seecms\library\Operate;
use magein\seecms\library\UserPermission;
use magein\seecms\library\View;
use magein\seecms\page\unit\control\CheckboxControl;
use magein\seecms\page\unit\control\InputControl;
use magein\seecms\response\HtmlResponse;
use magein\seecms\response\JsonResponse;
use magein\seecms\SeeLang;
use magein\utils\Tree;

class Permission
{

    /**
     * @rule_title 菜单管理
     * @return HtmlResponse
     */
    public function menu(): HtmlResponse
    {
        $records = Factory::table()->menu->order('sort', 'asc')->select();

        $tree = Tree::init($records);
        $data = $tree->floor();

        return View::fetch('permission/menu', [
            'menus' => $data
        ]);
    }

    /**
     * @rule_title 保存菜单排序
     * @return JsonResponse
     * @throws SeeDbException
     */
    public function setMenuSort(): JsonResponse
    {
        $id = Factory::request()->post('id');
        $sort = intval(Factory::request()->post('sort'));

        if (empty($id)) {
            return JsonResponse::error(Operate::required());
        }

        if ($sort <= 0) {
            $sort = 1;
        }

        if ($sort > 99999) {
            $sort = 99999;
        }

        Factory::table()->menu->save(compact('id', 'sort'));

        return JsonResponse::saveSuccess();
    }


    /**
     * @rule_title 设置菜单状态
     * @return JsonResponse
     * @throws SeeDbException
     */
    public function setMenuStatus(): JsonResponse
    {
        $id = Factory::request()->post('id');
        $status = intval(Factory::request()->post('status'));

        if (empty($id)) {
            return JsonResponse::error(Operate::required());
        }

        $status = $status === 1 ? 1 : 0;

        Factory::table()->menu->save(compact('id', 'status'));

        return JsonResponse::saveSuccess();
    }

    /**
     * @rule_title 用户授权
     * @return HtmlResponse
     */
    public function userGrant(): HtmlResponse
    {
        $user_id = Factory::request()->get('id');

        $permission = UserPermission::init($user_id);

        // 用户权限
        $user_roles = $permission->role();
        $user_menus = $permission->menu();
        $has_rules = $permission->rule();

        // 角色
        $roles = Factory::table()->user_role->select();
        // 菜单
        $menus = Factory::table()->menu->where('status', Menu::STATUS_OPEN)->order('sort', 'asc')->select();
        $menus = Tree::init($menus)
            ->setChild('children')
            ->transfer(function ($item) use ($user_menus) {
                $checked = false;
                if ($item['parent_id'] && in_array($item['id'], $user_menus)) {
                    $checked = true;
                }
                return [
                    'id' => $item['id'],
                    'parent_id' => $item['parent_id'],
                    'title' => $item['title'],
                    'spread' => true,
                    'checked' => $checked,
                ];
            });

        // 权限
        $rules = Factory::table()->rule->select();
        $group_rules = [];
        foreach ($rules as $rule) {
            $group_rules[$rule['group']][] = $rule;
        }

        return View::fetch('permission/user.grant',
            compact(
                'roles',
                'user_id',
                'menus',
                'user_roles',
                'group_rules',
                'has_rules'
            ));
    }

    /**
     * @rule_title 保存用户角色
     * @return JsonResponse
     */
    public function saveUserRoles(): JsonResponse
    {
        $user_id = Factory::request()->post('user_id');
        $role_ids = Factory::request()->post('role_ids');

        if (empty($user_id) || empty($role_ids)) {
            return JsonResponse::error(Operate::required('user_id,role_ids'));
        }

        $result = Factory::table()->user_permission->updateOrCreate(compact('user_id'), ['role' => implode(',', $role_ids)]);

        if ($result >= 0) {
            return JsonResponse::saveSuccess();
        }

        return JsonResponse::saveError();
    }


    /**
     * @rule_title 保存用户菜单
     * @return JsonResponse
     */
    public function saveUserMenus(): JsonResponse
    {
        $user_id = Factory::request()->post('user_id');
        $menu_ids = Factory::request()->post('menu_ids');

        if (empty($user_id) || empty($menu_ids)) {
            return JsonResponse::error(Operate::required('user_id,menu_ids'));
        }

        $result = Factory::table()->user_permission
            ->updateOrCreate(compact('user_id'), ['menu' => implode(',', $menu_ids)]);

        if ($result >= 0) {
            return JsonResponse::saveSuccess();
        }

        return JsonResponse::saveError();
    }


    /**
     * @rule_title 保存用户权限
     * @return JsonResponse
     */
    public function saveUserRules(): JsonResponse
    {
        $user_id = Factory::request()->post('user_id');
        $rule_ids = Factory::request()->post('rule_ids');

        if (empty($user_id) || empty($rule_ids)) {
            return JsonResponse::error(Operate::required('user_id、rule_ids'));
        }

        $result = Factory::table()->user_permission
            ->updateOrCreate(compact('user_id'), ['rule' => implode(',', $rule_ids)]);

        if ($result >= 0) {
            return JsonResponse::saveSuccess();
        }

        return JsonResponse::saveError();
    }

    /**
     * @rule_title 角色授权
     * @return HtmlResponse
     */
    public function roleGrant(): HtmlResponse
    {
        $role_id = Factory::request()->get('id');

        $role = Factory::table()->user_role->find($role_id);

        $role_menus = $role ? explode(',', $role['menu']) : [];
        $has_rules = $role ? explode(',', $role['rule']) : [];

        $menus = Factory::table()->menu->where('status', Menu::STATUS_OPEN)->order('sort', 'asc')->select();

        $data = Tree::init($menus)->setChild('children')->transfer(function ($item) use ($role_menus) {
            $checked = false;
            if ($item['parent_id'] && in_array($item['id'], $role_menus)) {
                $checked = true;
            }

            return [
                'id' => $item['id'],
                'parent_id' => $item['parent_id'],
                'title' => $item['title'],
                'spread' => true,
                'checked' => $checked,
            ];
        });

        // 获取权限
        $rules = Factory::table()->rule->select();
        $group_rules = [];
        foreach ($rules as $rule) {
            $group_rules[$rule['group']][] = $rule;
        }

        return View::fetch('permission/role.grant', [
            'id' => $role_id,
            'menus' => $data,
            'group_rules' => $group_rules,
            'has_rules' => $has_rules,
        ]);
    }

    /**
     * @rule_title 保存角色菜单
     * @return JsonResponse
     */
    public function saveRoleMenus(): JsonResponse
    {
        $id = Factory::request()->post('id');
        $menu_ids = Factory::request()->post('menu_ids');

        if (empty($menu_ids)) {
            return JsonResponse::error(SeeLang::permission('select_menus'));
        }

        if ($id == 1) {
            return JsonResponse::error(SeeLang::permission('root_not_grant_menu'));
        }

        $record = Factory::table()->user_role->find($id);
        if (empty($record)) {
            return JsonResponse::error(SeeLang::user('not_found'));
        }

        // 更新角色的菜单信息
        Factory::table()->user_role->where('id', $id)->data('menu', implode(',', $menu_ids))->update();

        return JsonResponse::saveSuccess();
    }

    /**
     * @rule_title 保存角色权限
     * @return JsonResponse
     */
    public function saveRoleRules(): JsonResponse
    {
        $id = Factory::request()->post('id');
        $rule_ids = Factory::request()->post('rule_ids');

        if (empty($rule_ids)) {
            return JsonResponse::error(SeeLang::permission('select_rules'));
        }

        if ($id == 1) {
            return JsonResponse::error(SeeLang::permission('root_not_grant_rules'));
        }

        $record = Factory::table()->user_role->find($id);
        if (empty($record)) {
            return JsonResponse::error(SeeLang::user('not_found'));
        }

        // 更新角色的权限信息
        Factory::table()->user_role->where('id', $id)->data('rule', implode(',', $rule_ids))->update();

        return JsonResponse::saveSuccess();
    }

    public function resourceRule(): HtmlResponse
    {
        $options = [
            'index' => 'index',
            'read' => 'read',
            'create' => 'create',
            'save' => 'save',
            'edit' => 'edit',
            'update' => 'update',
            'delete' => 'delete',
        ];

        $controls = [
            InputControl::init('group')->setLabel(SeeLang::rule('group'))->setDescription([SeeLang::rule('group_tip')]),
            InputControl::init('title')->setLabel(SeeLang::rule('title'))->setDescription([SeeLang::rule('title_tip')]),
            CheckboxControl::init('resource')->setLabel(SeeLang::rule('resource'))->setOptions($options)->setDescription(
                [
                    SeeLang::rule('res_index_tip'),
                    SeeLang::rule('res_read_tip'),
                    SeeLang::rule('res_create_tip'),
                    SeeLang::rule('res_save_tip'),
                    SeeLang::rule('res_edit_tip'),
                    SeeLang::rule('res_update_tip'),
                    SeeLang::rule('res_delete_tip'),
                ]
            ),
        ];

        return View::fetch('permission/resource', [
            'btn' => true,
            'action' => Factory::route('s/permission/saveResourceRule'),
            'controls' => $controls
        ]);
    }

    public function saveResourceRule(): JsonResponse
    {
        $group = Factory::request()->post('group');
        $title = Factory::request()->post('title');
        $resource = Factory::request()->post('resource');

        if (empty($group)) {
            return JsonResponse::error(SeeLang::rule('enter_group'));
        }

        if (empty($title)) {
            return JsonResponse::error(SeeLang::rule('enter_title'));
        }

        $resource = array_filter($resource);
        if (empty($resource)) {
            return JsonResponse::error(SeeLang::rule('select_resource'));
        }

        $options = [
            'page' => '%s' . SeeLang::rule('resource_page'),
            'index' => '%s' . SeeLang::rule('resource_index'),
            'read' => '%s' . SeeLang::rule('resource_read'),
            'create' => SeeLang::rule('resource_create') . '%s',
            'save' => SeeLang::rule('resource_save') . '%s',
            'edit' => SeeLang::rule('resource_edit') . '%s',
            'update' => SeeLang::rule('resource_update') . '%s',
            'delete' => SeeLang::rule('resource_delete') . '%s',
        ];

        $group = trim($group);
        $group = trim($group, '/');

        foreach ($resource as $item) {
            $url = 'p/' . $group . '/' . $item;
            $record = Factory::table()->rule->where('url', $url)->find();

            if (empty($record)) {
                Factory::table()->rule->insert([
                    'group' => $group,
                    'title' => sprintf($options[$item], $title),
                    'url' => $url,
                ]);
            }
        }

        return JsonResponse::saveSuccess();
    }
}
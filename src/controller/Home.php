<?php

declare(strict_types=1);

namespace magein\seecms\controller;

use magein\seecms\db\SeeDbException;
use magein\seecms\Factory;
use magein\seecms\library\Auth;
use magein\seecms\library\View;
use magein\seecms\response\HtmlResponse;
use magein\seecms\response\JsonResponse;
use magein\seecms\SeeLang;
use magein\utils\File;

class Home
{
    /**
     * @return HtmlResponse
     */
    public function index(): HtmlResponse
    {
        $right_toolbar = Factory::config()->right_toolbar->config;
        $userinfo = Auth::user()->info();
        $lang = Factory::config()->lang->config;
        $cookie_var = Factory::config()->lang->cookieVar();
        $default_lang = $_COOKIE[$cookie_var] ?? '';
        if ($default_lang) {
            $lang['default'] = $default_lang;
        }

        return View::fetch('home/index', compact('right_toolbar', 'userinfo', 'lang'));
    }

    /**
     * 后台首页
     * @return HtmlResponse
     */
    public function main(): HtmlResponse
    {
        $is_root = Auth::user()->isRoot();
        $suggest = false;
        if ($is_root) {
            $password = Auth::password('123456', Auth::user()->info()['encrypt']);
            if ($password == Auth::user()->info()['password']) {
                $suggest = true;
            }
        }

        $sys_info['web_server'] = $_SERVER['SERVER_SOFTWARE'];

        return View::fetch('home/main',
            [
                'phpversion' => phpversion(),
                'suggest_update_password' => $suggest,
                'sys_info' => $sys_info,
                'tips' => Factory::config()->debug(),
            ]
        );
    }

    /**
     * @return JsonResponse
     */
    public function menu(): JsonResponse
    {
        $menus = Auth::user()->menu();

        return new JsonResponse($menus);
    }

    /**
     * @return JsonResponse
     * @throws SeeDbException
     */
    public function ruleApply(): JsonResponse
    {
        $title = Factory::request()->post('title');
        $url = Factory::request()->post('url');
        $remark = Factory::request()->post('remark');

        // 验证用户是否已经拥有权限
        $rules = Auth::user()->rule();
        if (in_array($url, $rules)) {
            return JsonResponse::error(SeeLang::rule('verifying'), 1, ['status' => 1]);
        }

        $record = Factory::table()->user_rule_apply->where('url', $url)
            ->where('user_id', Auth::user()->uuid())
            ->where('status', '<>', 1)
            ->order('id desc')
            ->find();
        if ($record) {
            return JsonResponse::error(SeeLang::rule('verifying'), 1, $record);
        }

        $data = [
            'user_id' => Auth::user()->uuid(),
            'title' => $title,
            'url' => $url,
            'remark' => $remark,
        ];

        Factory::table()->user_rule_apply->insert($data);

        return JsonResponse::saveSuccess();
    }

    /**
     * 清除缓存
     * @return JsonResponse
     */
    public function clearCache(): JsonResponse
    {
        $type = Factory::request()->param('type');
        // 清除菜单、权限缓存
        Auth::user()->clearCachePermission($type);

        $template = function () {
            $cache = Factory::config()->twig->cache();
            if (is_dir($cache)) {
                $files = File::ins()->getTreeList($cache)->getData();
                if ($files) {
                    foreach ($files as $file) {
                        @unlink($file);
                    }
                }
            }
        };

        if ($type === 'all') {
            $template();
        } elseif ($type === 'template') {
            $template();
        }

        return JsonResponse::saveSuccess();
    }

    public function switchLangSet()
    {
        $lang = Factory::request()->post('lang');

        $cookie_var = Factory::config()->lang->cookieVar();
        setcookie($cookie_var, $lang, 0, '/');
        // 清除菜单
        Auth::user()->clearCachePermission('menu');

        return JsonResponse::saveSuccess();
    }

    public function upload(): JsonResponse
    {
        try {
            $upload = Factory::extension()->upload;
            $filepath = $upload->move();
        } catch (\RuntimeException $exception) {
            return JsonResponse::error($exception->getMessage());
        }

        return JsonResponse::success(['url' => $filepath], SeeLang::upload('success'));
    }

    /**
     * @rule_title 查看商店
     * @return HtmlResponse
     */
    public function store(): HtmlResponse
    {
        return View::fetch('general/store');
    }
}
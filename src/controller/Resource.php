<?php

declare(strict_types=1);

namespace magein\seecms\controller;

use magein\seecms\db\SeeDbException;
use magein\seecms\db\SeeDbQuery;
use magein\seecms\Factory;
use magein\seecms\library\Operate;
use magein\seecms\library\Transfer;
use magein\seecms\library\View;
use magein\seecms\page\Builder;
use magein\seecms\page\Parse;
use magein\seecms\response\HtmlResponse;
use magein\seecms\response\JsonResponse;
use magein\seecms\SeeLang;
use magein\utils\Result;

/**
 *资源控制器
 */
class Resource
{
    /**
     * @var Builder
     */
    protected $builder = null;

    /**
     * 表名称
     * @var null|SeeDbQuery
     */
    protected $db = null;

    public function __construct(Builder $builder)
    {
        $this->builder = $builder;

        $this->db();
    }

    /**
     * @return SeeDbQuery|null
     */
    protected function db(): ?SeeDbQuery
    {
        $name = $this->builder->getTableName() ?: $this->builder->getPageName();

        $this->db = Factory::db()->table($name);

        return $this->db;
    }

    /**
     * @return JsonResponse
     * @throws SeeDbException
     */
    public function index(): JsonResponse
    {
        $parse = new Parse();
        $query = $parse->query($this->builder->getQueries());

        $params = Factory::request()->get() ?: [];

        $event = $this->builder->event();
        $callable = $event->index->before;
        $data = null;
        if (is_callable($callable)) {
            /**
             * @var Result $result
             */
            $result = $callable();
            if ($result instanceof Result) {
                if ($result->getCode()) {
                    return JsonResponse::error($result->getMsg());
                }
                $data = $result->getData() ?: null;
            } elseif (is_array($result)) {
                $data = $result;
            }
        }

        // 查询之前返回data，则不在进行查询
        if (empty($data)) {
            $where = Transfer::pageQuery($query, $params);
            $data = $this->db->order('id desc')->where($where)->paginate();
        }

        $callable = $event->index->after;
        if (is_callable($callable)) {
            /**
             * @var Result $result
             */
            $result = $callable($data);
            if ($result instanceof Result) {
                if ($result->getCode()) {
                    return JsonResponse::error($result->getMsg());
                }
                $data = $result->getData() ?: $data;
            } elseif (is_array($result)) {
                $data = $result;
            }
        }

        return JsonResponse::success($data);
    }

    public function read()
    {
        $id = Factory::request()->get('id');

        $parse = Parse::global($this->builder->getDictionary());
        $controls = $parse->controls($this->builder->getShows());

        $event = $this->builder->event();
        $callable = $event->read->before;
        if (is_callable($callable)) {
            /**
             * @var Result $result
             */
            $result = $callable();
            if ($result instanceof Result) {
                if ($result->getCode()) {
                    return JsonResponse::error($result->getMsg());
                }
                $data = $result->getData() ?: null;
            } else {
                $data = $result;
            }
        }

        if (empty($data)) {
            $data = $this->db->find($id);
        }

        if ($data) {
            foreach ($controls as $key => $control) {
                $control->setReadonly();
                $control->setDisabled();
                $control->setValue(null);
                $controls[$key] = $control;

                $name = $control->getName() ?? null;
                if (empty($name) || !isset($data[$name])) {
                    continue;
                }
                $val = $data[$name];
                $control->setValue($val);
                $controls[$key] = $control;
            }
        }

        $callable = $event->read->after;
        if (is_callable($callable)) {
            /**
             * @var Result $result
             */
            $result = $callable($data);
            if ($result->getCode()) {
                return JsonResponse::error($result->getMsg());
            }
        }

        return View::fetch('page/control', [
            'id' => $id,
            'btn' => false,
            'controls' => $controls,
            'dialog' => $this->builder->getDialog()->toArray(),
        ]);
    }

    /**
     * @return HtmlResponse
     */
    public function edit(): HtmlResponse
    {
        $id = Factory::request()->get('id');

        $parse = new Parse(SeeLang::instance()->global($this->builder->getDictionary()));
        $controls = $parse->controls($this->builder->getControls());

        $data = $this->db->find($id);

        if ($data) {
            foreach ($controls as $key => $control) {
                $name = $control->getName();
                if (empty($name)) {
                    continue;
                }
                $val = $data[$name] ?? $control->getValue();
                $control->setValue($val);
                $controls[$key] = $control;
            }
        }

        return View::fetch('page/control', [
            'id' => $id,
            'btn' => true,
            'controls' => $controls,
            'action' => sprintf(Factory::route('p/%s/update'), $this->builder->getPageName()),
        ]);
    }

    /**
     * @return JsonResponse
     * @throws SeeDbException
     */
    public function update(): JsonResponse
    {
        $data = Factory::request()->post();

        $event = $this->builder->event();
        $callable = $event->update->before;
        if (is_callable($callable)) {
            /**
             * @var Result $result
             */
            $result = $callable($data);
            if ($result instanceof Result) {
                if ($result->getCode()) {
                    return JsonResponse::error($result->getMsg());
                }
                $data = $result->getData() ?: null;
            } else {
                $data = $result;
            }
        }

        if (empty($data)) {
            return JsonResponse::error(Operate::saveParameterNull());
        }

        if (empty($data['id'] ?? '')) {
            return JsonResponse::error(Operate::required());
        }

        // 返回的是更新条数
        $result = $this->db()->where('id', $data['id'])->update($data);

        $callable = $event->update->after;
        if (is_callable($callable)) {
            /**
             * @var mixed $result 保存结果
             */
            $result = $callable($data, $result);
            if ($result instanceof Result) {
                if ($result->getCode()) {
                    return JsonResponse::error($result->getMsg());
                }
            }
        }

        return JsonResponse::result($result);
    }

    /**
     * @return HtmlResponse
     */
    public function create(): HtmlResponse
    {
        $parse = new Parse(SeeLang::instance()->global($this->builder->getDictionary()));
        $controls = $parse->controls($this->builder->getCreates());

        return View::fetch('page/control', [
            'controls' => $controls,
            'btn' => true,
            'action' => sprintf(Factory::route('p/%s/save'), $this->builder->getPageName()),
            'dialog' => $this->builder->getDialog()->toArray(),
        ]);
    }

    /**
     * @return JsonResponse
     * @throws SeeDbException
     */
    public function save(): JsonResponse
    {
        $data = Factory::request()->post();
        unset($data['id']);

        $event = $this->builder->event();
        $callable = $event->save->before;
        if (is_callable($callable)) {
            /**
             * @var Result $result
             */
            $result = $callable($data);
            if ($result instanceof Result) {
                if ($result->getCode()) {
                    return JsonResponse::error($result->getMsg());
                }
                $data = $result->getData() ?: null;
            } else {
                $data = $result;
            }
        }

        if (empty($data)) {
            return JsonResponse::error(Operate::saveParameterNull());
        }

        /**
         * 返回自增主键
         */
        $result = $this->db()->save($data);

        $callable = $event->save->after;
        if (is_callable($callable)) {
            $result = $callable($data, $result);
            if ($result instanceof Result) {
                if ($result->getCode()) {
                    return JsonResponse::error($result->getMsg());
                }
            }
        }

        return JsonResponse::result($result);
    }

    /**
     * @return JsonResponse
     * @throws SeeDbException
     */
    public function delete(): JsonResponse
    {
        $id = Factory::request()->delete('id');

        $event = $this->builder->event();
        $callable = $event->delete->before;
        if (is_callable($callable)) {
            /**
             * @var Result $result
             */
            $result = $callable();
            if ($result->getCode()) {
                return JsonResponse::error($result->getMsg());
            }
        }

        if (empty($id)) {
            return JsonResponse::error(Operate::required());
        }

        if (is_numeric($id)) {
            $data = $this->db->find(intval($id));
            if (empty($data)) {
                return JsonResponse::error(Operate::notFoundData());
            }
        }

        if (is_string($id)) {
            $id = explode(',', $id);
        }

        $res = $this->db()->delete($id);

        $callable = $event->delete->after;
        if (is_callable($callable)) {
            $result = $callable($id, $res);
            if ($result instanceof Result) {
                if ($result->getCode()) {
                    return JsonResponse::error($result->getMsg());
                }
            }
        }

        if ($res) {
            return JsonResponse::success($res, sprintf(SeeLang::operate('del_success') . ':%s', $res));
        }

        return JsonResponse::error(SeeLang::operate('del_fail'));
    }

    /**
     * @return JsonResponse
     * @throws SeeDbException
     */
    public function patch(): JsonResponse
    {
        $id = Factory::request()->post('id');
        $field = Factory::request()->post('field');
        $value = Factory::request()->post('value');

        if (empty($id)) {
            return JsonResponse::error(SeeLang::operate('parameter_error'));
        }

        if (empty($field)) {
            return JsonResponse::error(SeeLang::operate('parameter_error'));
        }

        if ($value === '' || $value === 'null' || $value === null) {
            return JsonResponse::error(SeeLang::operate('parameter_error'));
        }

        $event = $this->builder->event();
        $callable = $event->patch->before;
        if (is_callable($callable)) {
            /**
             * @var Result $result
             */
            $result = $callable(compact('field', 'value'));
            if ($result instanceof Result) {
                if ($result->getCode()) {
                    return JsonResponse::error($result->getMsg());
                }
                $data = $result->getData() ?: null;
            } else {
                $data = $result;
            }
        } else {
            $data['id'] = $id;
            $data[$field] = $value;
        }

        $result = $this->db()->save($data);

        $callable = $event->patch->after;
        if (is_callable($callable)) {
            $result = $callable($data, $result);
            if ($result instanceof Result) {
                if ($result->getCode()) {
                    return JsonResponse::error($result->getMsg());
                }
            }
        }

        return $result ? JsonResponse::saveSuccess() : JsonResponse::saveError();
    }

    /**
     * @return HtmlResponse
     */
    public function page(): HtmlResponse
    {

        $parse = new Parse(SeeLang::instance()->global($this->builder->getDictionary()));

        $urls = [];

        $actions = [
            'index',
            'read',
            'create',
            'edit',
            'update',
            'delete',
            'patch',
        ];

        foreach ($actions as $action) {
            $urls[$action] = sprintf(Factory::route('/p/%s/%s'), $this->builder->getPageName(), $action);
        }

        $page_title = SeeLang::instance()->getGroup('page_title');
        $title = ($page_title[$this->builder->getPageName()] ?? '') ?: $this->builder->getTitle();

        return View::fetch('page/index',
            [
                'urls' => $urls,
                'title' => $title,
                'queries' => $parse->query($this->builder->getQueries()),
                'alerts' => $parse->alerts($this->builder->getAlerts()),
                'left_toolbars' => $parse->toolbars($this->builder->getLeftToolbars()),
                'right_toolbars' => $this->builder->getRightToolbars(),
                'columns' => $parse->columnsTransfer($this->builder->getActionColumns()),
                'dialog' => $this->builder->getDialog()->toArray(),
            ]
        );
    }
}
<?php
declare(strict_types=1);
/**
 * @user magein
 * @date 2024/1/18 17:30
 */

namespace magein\seecms\controller;

use magein\seecms\Factory;
use magein\seecms\library\View;
use magein\seecms\page\Builder;
use magein\seecms\page\Parse;
use magein\seecms\response\HtmlResponse;
use magein\seecms\response\JsonResponse;
use magein\seecms\SeeLang;
use magein\utils\Apipost;
use magein\utils\Faker;
use magein\utils\Variable;
use ReflectionClass;

class Develop
{
    /**
     * @var \magein\seecms\library\Develop|null
     */
    protected $parse = null;

    public function __construct()
    {
        $this->parse = new \magein\seecms\library\Develop();
    }

    public function model(): HtmlResponse
    {

        if (Factory::request()->isAjax()) {

            $table_name = Factory::request()->post('table');
            $action = Factory::request()->post('action');

            $data = $this->parse->parseTable($table_name);

            // 字段列表
            if ($action === 'columns') {
                $string = "//字段列表\n [ \n" . $this->parse->columnFormatter($data['columns'], '    ') . ']';
                return new HtmlResponse($string);
            }

            // 字段描述
            if ($action === 'comment') {
                $data = $this->parse->columnComment($data['dictionary'], $table_name);
                $string = $data['zh_cn'] . "\n" . $data['en_us'];
                return new HtmlResponse($string);
            }

            // 模型属性
            if ($action === 'attrs') {
                $attrs = Factory::db()->query("show full columns from $table_name");
                $data = $this->parse->columnToAttrs($attrs);
                return new HtmlResponse($data);
            }

            // 构建页面
            if ($action === 'page') {
                $data = $this->parse->pageBuilder($data['columns'], $table_name);
                return new HtmlResponse($data);
            }
        }

        $tables = Factory::db()->getTables();

        $action = [
            'columns' => '字段',
            'comment' => '字典',
            'attrs' => '属性',
            'page' => '构建管理页面',
        ];

        return View::fetch('develop/model', compact('tables', 'action'));
    }

    public function mock()
    {
        if (Factory::request()->isAjax()) {

            $table_name = Factory::request()->post('table');
            $limit = (int)Factory::request()->post('limit');
            if ($limit <= 0) {
                $limit = 10;
            }

            $attrs = Factory::db()->query("show full columns from $table_name");

            for ($i = 0; $i < $limit; $i++) {
                $data = Faker::mock($attrs);
                Factory::db()->table($table_name)->insert($data);
            }

            return JsonResponse::saveSuccess();
        }

        $tables = Factory::db()->getTables();

        return View::fetch('develop/mock', compact('tables'));
    }

    public function apiPost(): HtmlResponse
    {
        if (Factory::request()->isAjax()) {

            $table_name = Factory::request()->post('table');
            $format = Factory::request()->post('format');

            $attrs = Factory::db()->query("show full columns from $table_name");

            $apipost = new Apipost($attrs);
            if ($format == 'page') {
                $data = $apipost->page();
            } elseif ($format == 'list') {
                $data = $apipost->lists();
            } else {
                $data = $apipost->object();
            }
            return new HtmlResponse($data);
        }

        $tables = Factory::db()->getTables();

        $format = [
            'object' => '对象',
            'list' => '列表',
            'page' => '分页'
        ];

        return View::fetch('develop/apipost', compact('tables', 'format'));
    }

    public function icon(): HtmlResponse
    {

        $vendor = Factory::config()->vendor;

        $root = $vendor->root();
        $cms = $vendor->assetCms();
        $lib = $vendor->assetLib();

        $files = [
            'icon' => $root . $cms . '/font/iconfont.css',
            'layui-icon' => $root . $lib . '/layui/css/layui.css',
        ];

        $icons = [];
        foreach ($files as $key => $file) {
            $file = trim($file, '\\');
            if (is_file($file)) {
                $content = file_get_contents($file);
                preg_match_all('/\.(' . $key . '-\w+):before/', $content, $matches);
                $icon_fonts = $matches[1] ?? [];
                if ($icon_fonts) {
                    if ($key == 'layui-icon') {
                        array_pop($icon_fonts);
                    }
                    $icons[$key] = array_chunk($icon_fonts, 10);
                }
            }
        }

        return View::fetch('develop/icon', [
            'icons' => $icons
        ]);
    }

    public function rules(): HtmlResponse
    {
        // 已经添加的权限规则
        $rules = Factory::table()->rule->column('url');

        // 权限规则池
        $rule_pool = [];
        /**
         * @param string $group 权限组
         * @param string $group_title 权限组名称
         * @param string $rule_name 权限规则名称
         * @param string $path 权限规则路径
         * @return void
         */
        $concat = function (string $group, string $group_title, string $rule_name, string $path) use (&$rule_pool, $rules) {

            $group = trim($group);
            $name = trim($rule_name);

            $path = trim($path);
            $path = trim($path, '/');
            $path = trim($path, 'admin');
            $path = trim($path, '/');
            $path = Variable::ins()->camelCase($path);

            $already = false;
            if (in_array($path, $rules)) {
                $already = true;
            }

            $data = compact('name', 'path', 'already');

            if (isset($rule_pool[$group])) {
                $rule_pool[$group]['paths'][$path] = $data;
            } else {
                $rule_pool[$group] = [
                    'title' => $group_title,
                    'paths' => [$path => $data],
                ];
            }
        };

        $pages = Factory::extension()->page_provider->getPages();
        if ($pages) {

            $options = [
                'index' => '%s列表',
                'read' => '%s查看',
                'create' => '新增%s表单',
                'save' => '保存%s',
                'edit' => '编辑%s表单',
                'update' => '更新%s',
                'delete' => '删除%s',
            ];

            $parse = new Parse();
            foreach ($pages as $name => $item) {

                try {
                    /**
                     * @var Builder $page
                     */
                    $page = new $item();
                } catch (\Exception $exception) {

                }

                if (!$page instanceof Builder) {
                    continue;
                }

                $group = $page->getPageName();

                $group_title = SeeLang::instance()->get('permission.' . $group) ?: $group;

                $concat($group, $group_title, sprintf($options['index'], $group_title), sprintf('p/%s/index', $name));

                $left_toolbars = $page->getLeftToolbars();
                if ($left_toolbars) {
                    $left_toolbars = $parse->toolbars($left_toolbars);
                    foreach ($left_toolbars as $toolbar) {
                        $event = $toolbar->getEvent();
                        $rule_name = $toolbar->getText();
                        if (in_array($event, ['__create__', '__del__'])) {
                            $flag = trim($event, '_');
                            $concat($group, $group_title, $group_title . $rule_name, sprintf('p/%s/%s', $name, $flag));
                            // create跟save是两个行为， create为展示页面 save是提交保存
                            if ($event == '__create__') {
                                $concat($group, $group_title, '保存' . $group_title, sprintf('p/%s/save', $name));
                            }
                        } elseif ($event === '__page__') {
                            $concat($group, $group_title, $rule_name, sprintf('p/%s/save', $name));
                        }
                    }
                }

                $actions = $page->getActions();
                if ($actions) {
                    $actions = $parse->buttons($page->getActions());
                    foreach ($actions as $action) {
                        $event = $action->getEvent();
                        $rule_name = $action->getText();
                        if ($event === '__read__') {
                            $concat($group, $group_title, $group_title . '查看', sprintf('p/%s/read', $name));
                        } elseif (in_array($event, ['__edit__', '__del__'])) {
                            $flag = trim($event, '_');
                            $concat($group, $group_title, $group_title . $rule_name, sprintf('p/%s/%s', $name, $flag));
                            if ($event == '__edit__') {
                                $concat($group, $group_title, '更新' . $group_title, sprintf('p/%s/update', $name));
                            }
                        } elseif ($event === '__page__') {
                            $concat($group, $group_title, $rule_name, $action->getUrl());
                        }
                    }
                }
            }
        }

        // 配置文件中设置的自动识别的控制器
        $controllers = Factory::config()->permission->auto();

        // 内置的控制器
        array_unshift($controllers, [
            'dir' => __DIR__,
            'namespace' => 'magein\seecms\controller',
            'excepts' => ['User', 'Home', 'Profile', 'Develop', 'Resource', 'Attachment'],
            'prefix' => 's',
        ]);

        foreach ($controllers as $controller) {
            $dir = $controller['dir'] ?? '';
            $excepts = $controller['excepts'] ?? [];

            $dir = trim($dir, DIRECTORY_SEPARATOR);
            if (is_dir($dir)) {

                $files = glob($dir . DIRECTORY_SEPARATOR . '*.php');
                foreach ($files as $file) {
                    $namespace = $controller['namespace'] ?? '';
                    $prefix = $controller['prefix'] ?? '';
                    $filename = pathinfo($file, PATHINFO_FILENAME);
                    $underline = Variable::ins()->underline($filename);
                    if (in_array($filename, $excepts) || in_array($underline, $excepts)) {
                        continue;
                    }

                    $namespace .= '\\' . $filename;

                    if (class_exists($namespace)) {
                        $class = new $namespace;
                        $ref = new ReflectionClass($class);
                        $methods = $ref->getMethods();
                        $group = $underline;
                        $group_title = SeeLang::instance()->get('permission.' . $group) ?: $group;
                        foreach ($methods as $method) {
                            $name = $method->getName();
                            $document = $method->getDocComment();
                            if ($document) {
                                preg_match('/@rule_title(.*)/', $document, $matches);
                                $rule_title = trim($matches[1] ?? '');
                                $rule_path = sprintf('%s%s/%s', $prefix ? $prefix . '/' : '', Variable::ins()->camelCase($filename), $name);
                                $concat($group, $group_title, $rule_title, $rule_path);
                            }
                        }
                    }
                }
            }
        }

        return View::fetch('develop/rules', [
            'rules' => $rule_pool
        ]);
    }

    public function saveRule(): JsonResponse
    {
        $rules = Factory::request()->post('rules');

        if (empty($rules)) {
            return JsonResponse::error(SeeLang::permission('请选择权限'));
        }

        $success = 0;
        foreach ($rules as $rule) {
            [$group, $title, $url] = explode(',', $rule);
            $group = trim($group);
            $title = trim($title);
            $url = trim($url);

            $record = Factory::table()->rule->where('url', $url)->find();
            if (empty($record)) {
                $res = Factory::table()->rule->insert(compact('group', 'title', 'url'));
                if ($res) {
                    $success++;
                }
            }
        }

        return JsonResponse::saveSuccess($success);
    }

    public function transMenus()
    {
        $menus = Factory::table()->menu->select();

        foreach ($menus as $item) {
            $parent_id = $item['parent_id'];
            $title = $item['title'];
            $icon = $item['icon'];
            $url = $item['url'];
            $sort = $item['sort'];
            $open_type = $item['open_type'];
            echo sprintf("\$concat('%s','%s','%s','%s','%s','%s'),",
                $parent_id,
                $title,
                $icon,
                $url,
                $sort,
                $open_type
            );
            echo "<br/>";
        }
        die();
    }

    public function transRule()
    {
        $menus = Factory::table()->rule->select();

        foreach ($menus as $item) {
            $group = $item['group'];
            $title = $item['title'];
            $url = $item['url'];

            echo sprintf("\$concat('%s','%s','%s'),",
                $group,
                $title,
                $url
            );
            echo "<br/>";
        }
        die();
    }
}
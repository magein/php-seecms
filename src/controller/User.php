<?php
declare(strict_types=1);

namespace magein\seecms\controller;

use Endroid\QrCode\Color\Color;
use Endroid\QrCode\Encoding\Encoding;
use Endroid\QrCode\ErrorCorrectionLevel\ErrorCorrectionLevelLow;
use Endroid\QrCode\QrCode;
use Endroid\QrCode\RoundBlockSizeMode\RoundBlockSizeModeEnlarge;
use Endroid\QrCode\Writer\PngWriter;
use magein\seecms\extension\UserLogin;
use magein\seecms\Factory;
use magein\seecms\library\Auth;
use magein\seecms\library\Captcha;
use magein\seecms\library\View;
use magein\seecms\response\HtmlResponse;
use magein\seecms\response\JsonResponse;
use magein\seecms\response\RedirectResponse;
use magein\seecms\SeeLang;

class User
{

    /**
     * @return string
     */
    public function index(): string
    {
        $type = Factory::request()->get('type') ?: 1;

        $setting = Factory::extension()->general->login;

        $data['support'] = json_decode($setting['support'] ?? '') ?: [];
        $data['sms_code_len'] = $setting['sms_code_len'] ?? 4;
        $keep_login_data = Factory::extension()->user_login->keepLoginData();
        $data['keep'] = $keep_login_data;
        $data['type'] = $type;

        if ($type == UserLogin::TYPE_SCAN_WX) {
            $qrcode = Auth::wxOfficialAccount()->qrcode;
            // 设置登录标识
            $flag = 'login_' . md5(uniqid());
            $result = $qrcode->temporary($flag, 600);
            $data['wx_qrcode'] = $qrcode->url($result['ticket']);
            $data['wx_flag'] = $flag;
        }

        return View::fetch('user/index', $data)->data;
    }

    /**
     * @return HtmlResponse
     */
    public function captcha(): HtmlResponse
    {
        $captcha = new Captcha();
        return $captcha->output();
    }

    /**
     * app 扫码登录
     * @return HtmlResponse
     * @throws \Exception
     */
    public function appScan(): HtmlResponse
    {
        $token = Factory::extension()->user_login->appScan();

        $writer = new PngWriter();
        $qrCode = QrCode::create($token)
            ->setEncoding(new Encoding('UTF-8'))
            ->setErrorCorrectionLevel(new ErrorCorrectionLevelLow())
            ->setSize(230)
            ->setRoundBlockSizeMode(new RoundBlockSizeModeEnlarge())
            ->setForegroundColor(new Color(0, 0, 0))
            ->setBackgroundColor(new Color(255, 255, 255));
        $result = $writer->write($qrCode);
        header('Content-Type: ' . $result->getMimeType());
        return new HtmlResponse($result->getString());
    }

    /**
     * 微信扫码登录
     * @return JsonResponse
     */
    public function wxScan()
    {
        $flag = Factory::request()->post('flag') ?: '';

        // 获取缓存的值
        $data = Factory::cache()->get($flag);
        if ($data) {
            $user_login = Factory::extension()->user_login;
            $data['type'] = UserLogin::TYPE_SCAN_WX;
            $result = $user_login->doLogin($data);
            return JsonResponse::result($result);
        }

        return JsonResponse::success(2);
    }

    /**
     * 用户登录
     * @return JsonResponse
     */
    public function login(): JsonResponse
    {
        $user_login = Factory::extension()->user_login;
        $data = Factory::request()->post();

        if (empty($data['type'] ?? '')) {
            $data['type'] = UserLogin::TYPE_ACCOUNT;
        }

        if (in_array($data['type'], [1, 2])) {
            $user_login->keepLogin($data);
        }

        if ($data['type'] == UserLogin::TYPE_ACCOUNT && !Factory::config()->debug()) {
            $captcha = new Captcha();
            $verify = $captcha->check($data['verify'] ?? '');
            if ($verify === false) {
                return JsonResponse::error(SeeLang::login('verify_code_error'));
            }
        }

        $result = $user_login->doLogin($data);

        return JsonResponse::result($result);
    }


    /**
     * 登出页面
     * @return RedirectResponse
     */
    public function logout(): RedirectResponse
    {
        Auth::user()->logout();

        return new RedirectResponse(Factory::route('login'));
    }

    /**
     * 没有访问权限页面
     * @return HtmlResponse
     */
    public function noPermission(): HtmlResponse
    {
        $url = Factory::request()->param('url');

        $record = Factory::table()->rule->where('url', $url)->find();

        return View::fetch('user/no.permission', [
            'title' => $record ? ($record['title'] ?? '') : '',
            'url' => $url,
            'is_dialog' => true
        ]);
    }
}
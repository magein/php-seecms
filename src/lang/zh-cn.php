<?php

return [

    'title' => 'Seecms',
    'name' => 'See Cms',

    // 选项卡
    'tab' => [
        'home' => '首页',
    ],

    'menu' => [
        'search' => '搜索菜单',
        'redirect_url_tip' => '点击菜单跳转连接',
        'open_type_tip' => '当类型为菜单时生效'
    ],

    // 页面标题
    'page_title' => [
        'attachment' => '附件管理',
        'user' => '用户列表',
        'user_role' => '角色列表',
        'menu' => '菜单列表',
        'rule' => '权限规则列表',
        'user_rule_apply' => '权限申请列表',
        'user_login' => '登录日志',
    ],

    'page' => [
        'count_text' => '共 ',
        'count_text_unit' => ' 条',
        'skip_go' => ' 到第',
        'skip_confirm' => ' 确定',
        'prev' => '上一页',
        'next' => '下一页'
    ],

    // 对话框
    'dialog' => [
        'user_create' => '新增用户',
        'user_grant' => '用户授权',
        'role_create' => '新增角色',
        'role_grant' => '角色授权',
        'resource_rule' => '资源权限',
        'rule_create' => '添加权限规则',

        'modify_password' => '修改密码',
        'enter_password' => '请输入密码',

        'cp_success' => '复制成功',
        'cp_fail' => '复制失败！请手动复制',

        'confirm_title' => '防误提醒',
    ],

    // 操作
    'operate' => [
        'del_success' => '删除成功，删除数量',
        'del_fail' => '删除失败，请刷新后重试',

        'save_success' => '保存成功',
        'save_fail' => '保存失败',

        'update_success' => '更新成功',
        'update_fail' => '更新失败',

        'parameter_error' => '参数错误',
        'parameter_format_error' => '参数格式错误',

        'email_success' => '邮件发送成功',
        'not_found_data' => '没有找到数据',

        'save_parameter_null' => '更新参数为不能为空',
    ],

    // 用户
    'user' => [
        'logout_confirm' => '请再起确认是否注销登录?',

        'wechat_bind' => '微信绑定',
        'wechat_scan_bind' => '请扫码绑定',
        'mac_address' => 'mac地址',
        'edit_tips' => '编辑个人信息需要验证密码',
        'click_to_verify' => '点击验证',

        'username_tip' => '请输入数字字母下划线',
        'password_tip' => '请输入6-18位密码',
        'confirm_password_tip' => '请输入确认密码，需要跟密码保存一致',
        'nickname_tip' => '请输入您的昵称',

        'new_password_tip' => '不填写则不进行更新',

        'grant_role' => '分配角色',
        'grant_menu' => '授权菜单',
        'grant_rule' => '访问权限',

        'role_grant' => '授权',
        'role_grant_menu' => '授权菜单',
        'role_grant_rule' => '访问权限',

        'delete_root' => '不能删除超级管理员',
        'delete_role' => '不能删除系统内置的角色',

        'not_found' => '用户不存在',
        'select_user' => '请选择用户',
        'role_name' => '角色名称',

        'message' => '消息中心',

        'enter_passwd' => '请输入密码',
        'passwd_error' => '密码错误',
        'enter_old_passwd' => '请输入旧密码',
        'old_passwd_error' => '旧密码不正确',
        'enter_new_passwd' => '请输入新密码',
        'enter_new_passwd_format' => '新密码长度需要6~18位',
        'enter_confirm_passwd' => '请输入确认密码',
        'new_not_equal_confirm' => '新密码与确认密码不一致',
        'reset_passwd_success' => '密码更新成功',
        'verify_passwd' => '请先验证密码',
        'verify_passwd_expired' => '验证信息已经失效，请重新验证',
    ],


    // 登录设置
    'login' => [
        'title' => '登录设置',
        'support' => '登录方式',
        'support_tip' => '登录支持的方式',
        'together' => '同时在线',
        'together_tip' => '同一个账号是否允许同时登录',
        'effective_duration' => '登录有效时长',
        'effective_duration_tip' => '单次登录有效期，超过后需要重新登录，以小时为单位，最大值24小时',
        'ip_whitelist' => 'ip白名单',
        'ip_whitelist_tip' => '使用专线网络的时候使用此功能。多个请用,隔开',
        'mac_whitelist' => 'mac白名单',
        'mac_whitelist_tip' => '开启后将限制设备登录，请慎用。多个请用,隔开',

        'username_password_null' => '账号或密码不能为空',
        'username_error' => '账号错误',
        'username_password_error' => '账号密码错误',
        'username_forbid' => '账号已被禁用',

        'verify_code_error' => '图形验证码错误',

        'method_username_passwd' => '账号+密码+图形验证码',
        'method_phone_code' => '手机号码+动态验证码',
        'method_wx_oauth' => '微信授权登录',
        'method_app_scan' => '扫码登录',

        'account_passwd' => '账号登录',
        'phone_code' => '验证码登录',

        'other_login' => '其他登录方式',
        'keep_login' => '保持登录',

        'success' => '成功',
        'fail' => '失败',
        'btn' => '登录',

        'enter_phone' => '请输入手机号码',
        'enter_code' => '请输入动态验证码',
        'enter_code_error' => '动态验证码错误',

        'qrcode_expire' => '二维码已经过期',
        'qrcode_refresh' => '请刷新扫码登录',

        'mac_forbid' => 'mac地址不允许登录',
        'ip_forbid' => 'ip地址不允许登录',
    ],

    // 站点配置
    'site' => [
        'title' => '站点配置',
        'name' => '站点名称',
        'name_tip' => '浏览器标签页显示的名称',
        'domain' => '站点地址',
        'domain_tip' => '请输入http、https的地址',
        'icon' => 'icon',
        'logo' => 'logo',
        'meta_keyword' => 'META关键词',
        'meta_keyword_tip' => '适用于seo 设置页面meta的keywords信息 多个用英文逗号隔开',
        'meta_tip' => 'META描述',
        'meta_desc_tip' => '适用于seo 设置页面meta的description信息',
        'copyright' => '备案信息',
        'copyright_tip' => '网站底部的备案信息',
    ],

    // 邮件服务
    'smtp' => [
        'title' => '邮件服务',
        'server' => 'smtp服务器',
        'server_tip' => 'smtp服务器地址，如smtp.qq.com',
        'port' => 'smtp端口号',
        'port_tip' => '一般为25或者465 具体以服务提供商为准',
        'email' => '发件人邮箱',
        'email_tip' => '作为邮件的发送人',
        'nickname' => '发件人昵称',
        'nickname_tip' => '作为邮件的发送人显示的昵称',
        'password' => '发件人密码',
        'password_tip' => '一般是授权码而非真实的邮箱密码',
        'receive' => '收件人邮箱',
        'receive_tip' => '测试邮件发送是否正常',
    ],

    // 上传设置
    'upload' => [
        'title' => '上传设置',
        'base' => '基础设置',
        'mime' => '上传类型',
        'mime_tip' => '允许上传的文件类型',
        'ext' => '上传格式',
        'ext_tip' => '允许上传的文件格式 多个逗号隔开',
        'size' => '上传大小',
        'size_tip' => '上传文件最大值，单位默认为byte',
        'driver' => '上传驱动',
        'driver_tip' => '阿里云、腾讯云、七牛云需要添加配置文件',
        'driver_local' => '本地磁盘',
        'driver_ali' => '阿里云',
        'driver_tencent' => '腾讯云',
        'driver_qiniu' => '七牛云',

        'limit_num_tip' => '限定上传数量',
        'limit_size_tip' => '限定大小',
        'limit_ext_tip' => '限定格式',

        'mime_image' => '图片',
        'mime_video' => '视频',
        'mime_audio' => '音频',
        'mime_all' => '音频',

        'image_ext' => '图片格式：jpg,jpeg,png,gif',
        'video_ext' => '视频格式：avi,mov,mpeg,mp4,3gp,wmv,mkv',
        'audio_ext' => '音频格式：mp3',
        'other_ext' => '其他常用：pdf,doc,docx,xls,xlsx,ppt,pptx,txt',

        'drag_tip' => '点击上传或将文件拖拽到此处',

        'image' => '上传图片',
        'file' => '上传文件',
        'choose' => '选择',

        'error_size' => '上传超出限制的大小',
        'error_filetype' => '不存允许的文件类型',
        'error_ini' => '上传发生错误，请检查php配置文件中上传大小限制',

        'success' => '上传成功'
    ],

    // 短信服务
    'sms' => [
        'title' => '短信服务',
        'base' => '基础设置',
        'base_tip' => '短息基础设置',
        'sign' => '签名',
        'sign_tip' => '短信内容前的签名',
        'sign_code' => '动态验证码',
        'sign_code_tip' => '动态验证码长度',
    ],

    // 数据库备份
    'backup' => [
        'title' => '数据库备份',
        'log' => '备份记录',
        'setting' => '备份设置',
        'manual' => '手动备份',
        'base' => '备份',
        'intro' => '说明',
        'btn' => '立即备份',

        'storage_duration' => '储存时长',
        'storage_duration_tip' => '备份文件储存时长，单位天',
        'max_memory' => '最大内存分配',
        'max_memory_tip' => '执行备份的时候，防止内存溢出报错，临时提高的内存空间',
        'chunk' => '分块长度',
        'chunk_tip' => '单次查询数据的数量',
        'zip' => '是否压缩',
        'zip_tip' => '开启压缩后将备份文件压缩成一份发送到指定邮箱',
        'zip_install' => '已安装zip扩展',
        'zip_un_install' => '未安装zip扩展',
        'receive' => '接收邮箱',
        'receive_tip' => '需要在系统配置中设置邮件smtp服务信息',
        'tables' => '已设置表',
        'tables_tip' => '配置文件中设置允许备份的表',

        'explain_support_db' => '仅仅支持mysql(mariadb)数据库备份',
        'explain_tip' => '采用增量备份的方式进行备份',
        'explain_suggest1' => '不建议依赖于此备份功能进行数据库容灾的可选项',
        'explain_suggest2' => '条件允许的情况下建议使用云服务',
        'explain_suggest3' => '如果您的数据很重要，请使用云服务器',
        'explain_command' => '备份命令',
        'explain_cron_task' => '定时任务',
        'explain_cron_tip' => '凌晨两点执行备份命令',
    ],

    'wechat' => [
        'title' => '微信设置',

        'setting' => '公众号配置',
        'event' => '公众号事件',
        'menu' => '公众号菜单',
        'material' => '素材管理',

        'app_config' => '配置',
        'app_qrcode' => '公众号二维码',
        'app_name' => '公众号名称',
        'app_name_tip' => '微信公众号名称',
        'app_id' => 'app_id',
        'app_id_tip' => '微信公众号app_id',
        'app_secret' => 'app_secret',
        'app_secret_tip' => '微信公众号app secret',
        'token' => 'token',
        'token_tip' => '微信公众号设置的token',
        'remake' => '备注',
        'remake_tip' => '微信公众号备注',

        'menu_name' => '菜单名称',
        'menu_name_tip' => '仅支持中英文和数字，字数不超过4个汉字或8个字母',
        'menu_type' => '事件类型',
        'menu_type_tip' => '详情请查看官网提供微信公总号开发文档',
        'menu_key' => 'key',
        'menu_key_tip' => '菜单KEY值，用于消息接口推送，不超过128字节。click等点击类型必须',
        'menu_url' => 'url',
        'menu_url_tip' => '网页 链接，用户点击菜单可打开链接，不超过1024字节。 type为miniprogram时，不支持小程序的老版本客户端将打开本url。',
        'menu_media_id' => 'Media id',
        'menu_media_id_tip' => 'media_id类型和view_limited类型必须，调用新增永久素材接口返回的合法media_id',
        'menu_appid' => 'Appid',
        'menu_appid_tip' => 'miniprogram类型必须，小程序的appid（仅认证公众号可配置）',
        'menu_pagepath' => 'Pagepath',
        'menu_pagepath_tip' => 'miniprogram类型必须，小程序的页面路径',
        'menu_article_id' => 'Article id',
        'menu_article_id_tip' => 'article_id类型和article_view_limited类型必须，发布后获得的合法 article_id',

        'add_menu' => '添加主菜单',
        'add_menu_max_tip' => ' 微信公众号限制主菜单最多只能添加三个',
        'add_sub_menu_max_tip' => ' 微信公众号限制子菜单最多只能添加五个',
        'update_menu' => '点击提交则更新公众号的菜单信息',
        'menu_event_click' => '单击',
        'menu_event_view' => '跳转',
        'menu_event_mini' => '小程序',
        'menu_event_scan' => '扫一扫',
        'menu_event_photo' => '相机',
        'menu_event_photo_or_album' => '拍照或者相册',
        'menu_event_album' => '微信相册',
        'menu_event_location_select' => '地理位置选择器',

        'menu_event_subscribe' => '关注公众号',
        'menu_event_keyword' => '关键字回复',
        'replay_notice' => '被动回复与主动回复说明',
        'replay_notice_item1' => '被动回复是微信服务器将交互事件推送给开发者目标服务器且目标服务器需要再五秒内作出应答。',
        'replay_notice_item2' => '被动回复消息条数只能回复一条。主动消息则可以多条。',
        'replay_notice_item3' => '被动回复消息不占用微信开放平台接口的每日调用次数上限。主动消息则有此限制。',
        'replay_notice_item4' => '主动回复触发场景、下发消息额度、有效期限制如下：',

        'push_trigger_condition' => '触发条件',
        'push_message_limit' => '下发额度',
        'push_expire' => '有效期',

        'user_message_event' => '用户发送消息',
        'user_message_limit' => '5条',
        'user_message_expire' => '48小时',

        'click_menu_event' => '点击自定义菜单',
        'click_menu_message_limit' => '3条',
        'click_menu_message_expire' => '1分钟',

        'subscribe_event' => '关注公众号',
        'subscribe_message_limit' => '3条',
        'subscribe_message_expire' => '1分钟',

        'scan_event' => '扫描二维码',
        'scan_message_limit' => '3条',
        'scan_message_expire' => '1分钟',

        'event_replay_method' => '回复方式',
        'event_replay_method_response' => '被动回复',
        'event_replay_method_push' => '主动回复',

        'event_replay_message_type' => '回复类型',
        'event_replay_message_text' => '文本消息',
        'event_replay_message_image' => '图片消息',
        'event_replay_message_voice' => '语音消息',
        'event_replay_message_video' => '视频消息',
        'event_replay_message_tip' => '图片、语音需要上传到素材库',

        'replay_keyword' => '关键字',
        'replay_keyword_add' => '添加关键字',

        'material_list' => '素材列表',
        'material_upload' => '上传素材',
        'material_upload_form' => '上传表单',
        'material_permanent' => '永久素材',
        'material_type' => '素材类型',
        'material_remark' => '素材备注',
        'material_video_title' => '视频标题',
        'material_video_title_tip' => '上传视频需要填写视频标题',
        'material_video_desc' => '视频描述',
        'material_video_desc_tip' => '上传视频需要填写视频描述',
        'material_upload_notice' => '上传素材须知',
        'material_upload_notice1' => '在微信里的图片，音乐，视频等等都需要先上传到微信服务器作为素材才可以在消息中使用。',
        'material_upload_notice2' => '微信图片上传服务有敏感检测系统，图片内容如果含有敏感内容，如色情，商品推广，虚假信息等，上传可能失败。',
        'material_upload_notice3' => '上传缩略图用于视频封面或者音乐封面。支持jpg。',
        'material_upload_notice4' => '上传图片 大小不超过 10M，支持bmp/png/jpeg/jpg/gif格式。',
        'material_upload_notice5' => '上传语音 大小2M，播放长度不超过60s，支持mp3/wma/wav/amr格式。',
        'material_upload_notice6' => '上传视频 大小210MB，支持MP4格式。',
        'material_upload_notice7' => '临时素材媒体文件在微信后台保存时间为3天，即3天后media_id失效。',
        'material_type_error' => '素材类型错误',

        'image' => '图片',
        'thumb' => '缩略图',
        'voice' => '语音',
        'video' => '视频',
    ],

    'cache' => [
        'title' => '清理缓存',
        'clear_all' => '一键清理缓存',
        'clear_menu' => '清理菜单缓存',
        'clear_rule' => '清理权限缓存',
        'clear_data' => '清理数据缓存',
        'clear_template' => '清理模版缓存',
        'clear_plugin' => '清理插件缓存',

        'clear_fail' => '清除缓存失败',
        'clearing' => '清除缓存中，请稍候!',
        'clear_success' => '缓存清除成功',
    ],

    'general' => [
        'header' => '通用配置',
        'submit_after_stay' => '完成后停留此页面',
        'not_found_file' => '文件不存在',
        'not_found_mailer' => '尚未配置smtp服务',
        'switch_Lang_set_tip' => '切换语言已经打开的标签需要重新打开后才能更新',
        'switch_Lang_set_confirm' => '请确认是否切换语言？',
    ],

    'btn' => [
        'title' => '按钮',
        'create' => '新增',
        'save' => '保存',
        'edit' => '编辑',
        'update' => '更新',
        'read' => '查看',
        'delete' => '删除',
        'send' => '发送',
        'submit' => '提交',
        'reset' => '重置',
        'reset_wechat_menu' => '重置微信公众号菜单',
        'search' => '搜索',
        'modify' => '修改',
        'export' => '导出',
        'print' => '打印',
        'redirect' => '跳转',
        'grant' => '授权',
        'role_grant' => '授权',
        'resource_rule' => '资源规则',
        'verify' => '审核',
        'operation' => '操作',
        'ok' => '确定',
        'cancel' => '取消',
        'close' => '关闭',
        'phone_code' => '获取验证码',
        'copy' => '复制',
        'preview' => '预览',
        'download' => '下载',
        'upload' => '上传',
    ],

    'switch' => [
        'forbid' => '禁用',
        'open' => '启用',
        'yes' => '是',
        'no' => '否',
        'up' => '上架',
        'down' => '下架',
    ],

    'permission' => [
        'attachment' => '附件',
        'user' => '用户',
        'user_role' => '用户角色',
        'menu' => '菜单',
        'rule' => '权限规则',
        'user_rule_apply' => '权限申请',
        'user_login' => '登录日志',
        'database' => '数据库备份',
        'general' => '通用设置',
        'permission' => '权限',
        'wechat' => '微信公众号',

        // 没有权限访问
        'no_permission' => '您尚未获得访问权限',
        'apply_rule_title' => '权限名称',
        'apply_rule_url' => '权限路径',
        'is_apply' => '您是否需要取得访问权限?',
        'apply_text' => '【点击申请访问】',
        'apply_intro_not_null' => '申请说明不能为空',
        'apply_submit' => '您的权限申请已经提交，请等待管理员审核',
        'please_input_intro' => '请输入申请说明',

        // 授权
        'select_menus' => '请选择菜单',
        'root_not_grant_menus' => '不能给超级管理员分配菜单',
        'select_rules' => '请选择权限',
        'root_not_grant_rules' => '不能给超级管理员分配权限',
    ],

    'rule' => [
        'group' => '权限组',
        'title' => '权限名称',
        'path' => '权限路径',
        'group_tip' => '允许字母和下划线',
        'title_tip' => '权限名称用于分配权限的时候可以快速匹配',
        'url_tip' => '路由地址,如: order/copy',
        'resource' => '资源路由',
        'res_group_tip' => '如：feedback、member',
        'res_title_tip' => '如：会员',
        'res_index_tip' => '勾选 index 则生成权限规则的名称为：会员列表',
        'res_read_tip' => '勾选 read 则生成权限规则的名称为：查看会员',
        'res_create_tip' => '勾选 create 则生成权限规则的名称为：新增会员表单',
        'res_save_tip' => '勾选 save 则生成权限规则的名称为：保存会员',
        'res_edit_tip' => '勾选 edit 则生成权限规则的名称为：编辑会员表单',
        'res_update_tip' => '勾选 update 则生成权限规则的名称为：更新会员',
        'res_delete_tip' => '勾选 delete 则生成权限规则的名称为：删除会员',

        'select_verify_status' => '请选择审核状态',
        'verify_padding' => '待审核',
        'verify_pass' => '通过',
        'verify_refuse' => '拒绝',
        'verify_passed' => '已经通过审核',
        'verify_invalid' => '无效的操作',
        'apply_not_found' => '申请不存在',
        'not_found' => '权限不存在',

        'verifying' => '您申请访问权限正在审核中',
        'apply_passed_tip' => '您已经通过申请，请跟新缓存或者重新登录',
        'apply_refused_tip' => '您的申请被拒绝，原因',

        'enter_group' => '请输入权限规则所属分组',
        'enter_title' => '请输入权限规则名称',
        'select_resource' => '请选择resource资源路由',
        'resource_page' => '管理页面',
        'resource_index' => '列表',
        'resource_read' => '查看',
        'resource_create' => '表单',
        'resource_save' => '保存',
        'resource_edit' => '编辑',
        'resource_update' => '更新',
        'resource_delete' => '删除',
    ],

    'message' => [
        'title' => '消息中心',

        'group_all' => '全部消息',
        'group_sys' => '系统消息',
        'group_security' => '安全消息',
        'group_text' => '消息分组',

        'group_detail' => '消息详情',

        'all' => '全部',
        'read' => '已读',
        'unread' => '未读',
        'do_read' => '标记已读',
        'do_delete' => '删除',
        'do_all_read' => '全部已读',
        'do_all_delete' => '全部删除',
    ],

    'attachment' => [
        'upload' => '附件上传',
        'water_image' => '水印图片',
        'water_text' => '水印文字',
        'add_water' => '添加水印',
        'image' => '附件图片',
        'choose_limit' => '限定选择数量',
        'list' => '附件列表',
        'choose_list' => '已选列表',
    ],

    'database' => [
        'confirm_download' => '请再次确认是否下载?',
        'confirm_email' => '请再次确认是否发送到邮箱?',

        'bk_confirm_title' => '数据库备份',
        'bk_confirm_content' => '请再次确认是否执行备份？',
        'bk_command' => '建议使用命令行执行备份',
    ],

    '__user' => [
        'username' => '登录账号',
        'nickname' => '昵称',
        'password' => '登录密码',
        'confirm_password' => '确认密码',
        'tries' => '尝试登录次数',
        'last_login_time' => '最后登录时间',
        'last_login_ip' => '最后登录IP',
        'encrypt' => '加密因子',
    ],

    '__user_role' => [
        'name' => '角色名称',
        'rule' => '角色权限',
        'menu' => '角色菜单',
        'description' => '角色描述',
    ],

    '__attachment' => [
        'filename' => '文件名称',
        'filepath' => '保存路径',
        'thumb' => '缩略图',
        'mime' => 'mime类型',
        'ext' => '扩展类型',
        'size' => '文件大小',
        'driver' => '储存引擎',
    ],

    '__menu' => [
        'parent_id' => '父级分类',
        'title' => '菜单名称',
        'icon' => '图标',
        'url' => '访问路径',
        'condition' => '规则条件',
        'remark' => '备注',
        'type' => '菜单类型',
        'open_type' => '打开类型',
        'extent' => '扩展属性',
        'is_menu' => '是菜单',
    ],

    '__rule' => [
        'group' => '权限组',
        'title' => '权限名称',
        'url' => '权限路径',
        'is_resource_permission' => '页面资源权限'
    ],

    '__user_rule_apply' => [
        'title' => '权限名称',
        'url' => '权限路径',
        'reason' => '原因',
        'audited_at' => '审核时间',
        'audit_user' => '审核员',
    ],

    '__user_login' => [
        'user_id' => '用户id',
        'ip' => '登录ip',
        'browser' => '浏览器',
        'browser_version' => '浏览器版本',
        'device' => '设备',
        'platform' => '平台',
        'platform_version' => '平台版本',
        'is_mobile' => '移动端',
        'status' => '登录结果',
        'reason' => '失败原因',
        'user_agent' => '登录标识',
    ],

    '__database_backup' => [
        'name' => '表名称',
        'begin_id' => '起始值',
        'end_id' => '截止值',
        'increase' => '增量',
        'filename' => '备份名称',
        'filepath' => '备份地址',
        'filesize' => '文件大小',
        'send_at' => '邮件发送时间',
        'send_result' => '邮件发送结果',
        'send_fail_reason' => '邮件失败原因',
    ],

    '__wechat_material' => [
        'type' => '素材类型',
        'media_id' => '素材id',
        'filename' => '文件名称',
        'filepath' => '保存路径',
        'mime' => 'mime类型',
        'ext' => '扩展类型',
        'size' => '文件大小',
        'url' => '图片地址',
        'expire_time' => '过期时间',
    ],

    '__messages' => [
        'user_id' => '用户id',
        'group' => '消息分组',
        'title' => '消息标题',
        'content' => '消息内容',
        'read_at' => '阅读时间',
        'read_text' => '是否已读',
        'created_at' => '创建时间',
    ],

    '__global__' => [
        'id' => '索引',
        'sort' => '排序',
        'created_at' => '创建时间',
        'updated_at' => '更新时间',
        'nickname' => '昵称',
        'profile' => '个人资料',
        'logout' => '注销登录',
        'login_at' => '登录时间',
        'login_ip' => '登录ip',
        'phone' => '手机号码',
        'mobile' => '手机号码',
        'tel' => '联系方式',
        'email' => '邮箱地址',
        'password' => '密码',
        'phone_code' => '动态验证码',
        'old_password' => '旧密码',
        'new_password' => '新密码',
        'confirm_password' => '确认密码',
        'remark' => '备注',
        'status' => '状态',
        'status_text' => '状态',
        'user_id' => '用户id',
        'content' => '内容',
        'des' => '描述',
        'description' => '描述',
        'captcha' => '图形验证码',
        'icon' => '图标',
        'file' => '文件',
        'files' => '文件',
        'image' => '图片',
        'images' => '图片',
        'img' => '图片',
        'photo' => '图片',
        'photos' => '图片',
        'picture' => '图片',
        'pictures' => '图片',
        'pic' => '图片',
        'pics' => '图片',
        'avatar' => '头像',
        'header' => '头像',
        'enter' => '请输入',
        'select' => '请选择',
    ],
];

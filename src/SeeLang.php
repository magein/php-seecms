<?php

namespace magein\seecms;

/**
 * @method static user($name)
 * @method static menu($name)
 * @method static rule($name)
 * @method static message($name)
 * @method static attachment($name)
 * @method static switch ($name)
 * @method static pageTitle($name)
 * @method static login($name)
 * @method static site($name)
 * @method static smtp($name)
 * @method static upload($name)
 * @method static sms($name)
 * @method static backup($name)
 * @method static cache($name)
 * @method static btn($name)
 * @method static permission($name)
 * @method static dialog($name)
 * @method static operate($name)
 * @method static general($name)
 * @method static wechat($name)
 */
class SeeLang
{
    protected $lang = [];

    /**
     * @var SeeLang|null
     */
    protected static $instance = null;

    /**
     * @var string
     */
    protected $range = 'zh-cn';

    protected $allow_group = true;

    protected $pool = [];

    protected function __construct()
    {

    }

    protected function __clone()
    {

    }

    public static function __callStatic($name, $params)
    {
        $field = current($params);
        $field = trim($field);
        if (empty($field)) {
            return '';
        }

        $group = self::$instance->pool[$name] ?? '';
        if (empty($group)) {
            $group = self::$instance->getGroup($name);
            self::$instance->pool[$name] = $group;
        }

        $value = $group[$field] ?? null;

        if ($value) {
            return $value;
        }

        $global = self::$instance->pool['global'] ?? '';
        if (empty($global)) {
            $global = self::$instance->pool['global'] = self::$instance->getGroup('__global__');
        }

        $value = $global[$field] ?? null;
        if ($value) {
            return $value;
        }

        return $field;
    }

    /**
     * @return SeeLang
     */
    public static function instance(): SeeLang
    {
        if (is_null(self::$instance)) {
            self::$instance = new self();
            self::$instance->switchLangSet($_COOKIE['seecms_lang'] ?? self::$instance->range);
        }

        return self::$instance;
    }

    /**
     * @return void
     */
    public function forbidGroup()
    {
        $this->allow_group = false;
    }

    /**
     * @param string $lang
     * @return void
     */
    public function switchLangSet(string $lang)
    {
        if ($lang) {
            $this->range = $lang;
            // 加载默认的语言包
            $this->load(__DIR__ . '/lang/' . $lang . '.php');
        }
    }

    public function getLangSet(): string
    {
        return $this->range;
    }

    /**
     * 加载语言包
     * @param string $file
     * @param string $range
     * @return array
     */
    public function load(string $file, string $range = ''): array
    {

        $range = $range ?: $this->range;
        if (!isset($this->lang[$range])) {
            $this->lang[$range] = [];
        }

        $lang = [];

        if (is_file($file)) {
            $result = include $file;
            $lang = array_change_key_case($result) + $lang;
        }

        if (!empty($lang)) {
            $this->lang[$range] = $lang + $this->lang[$range];
        }

        return $this->lang[$range];
    }

    /**
     * @param array $lang
     * @return void
     */
    public function refresh(array $lang)
    {
        $this->lang[$this->range] = $lang;
    }

    /**
     * @param array $lang
     * @param string $range
     * @return void
     */
    public function append(array $lang, string $range = '')
    {
        $range = $range ?: $this->range;

        if (!isset($this->lang[$range])) {
            $this->lang[$range] = [];
        }

        if ($this->lang[$range]) {
            $lang = array_merge($this->lang[$range], $lang);
        }

        $this->lang[$range] = $lang;
    }

    /**
     * 获取组
     * @param $group_name
     * @param string $range
     * @return mixed|string
     */
    public function getGroup($group_name, string $range = '')
    {
        if (is_null($group_name)) {
            return '';
        }

        $range = $range ?: $this->range;

        return $this->lang[$range][$group_name] ?? [];
    }

    /**
     * @param string|null $name
     * @param array $vars
     * @param string $range
     * @return array|mixed|string|string[]
     */
    public function get(string $name = null, array $vars = [], string $range = '')
    {
        if (is_null($name)) {
            return '';
        }

        $range = $range ?: $this->range;


        if ($this->allow_group && strpos($name, '.')) {
            [$name1, $name2] = explode('.', $name, 2);
            $value = $this->lang[$range][strtolower($name1)][$name2] ?? null;
        } else {
            $value = $this->lang[$range][strtolower($name)] ?? null;
        }

        // 从global中取值
        if (empty($value)) {
            $value = $this->lang[$range]['__global__'][strtolower($name)] ?? $name;
        }

        // 变量解析
        if (!empty($vars) && is_array($vars)) {
            if (key($vars) === 0) {
                // 数字索引解析
                array_unshift($vars, $value);
                $value = call_user_func_array('sprintf', $vars);
            } else {
                // 关联索引解析
                $replace = array_keys($vars);
                foreach ($replace as &$v) {
                    $v = "{:{$v}}";
                }
                $value = str_replace($replace, $vars, $value);
            }
        }

        return $value;
    }

    /**
     * 将语言包中的组名字取得的数据跟__global__组进行合并
     * @param string|null $name 语言包中的组名称
     * @param string $range
     * @return array
     */
    public function global(string $name = null, string $range = ''): array
    {
        if (is_null($name)) {
            return [];
        }

        $range = $range ?: $this->range;

        $global = $this->lang[$range]['__global__'] ?? [];
        $data = $this->lang[$range][strtolower($name)] ?? [];

        if (is_array($global) || is_array($data)) {
            return array_merge($global, $data);
        }

        return [];
    }
}
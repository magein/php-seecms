<?php

namespace magein\seecms;

class SeeResponse
{
    /**
     * 相应数据
     * @var null
     */
    public $data = null;

    /**
     * 相应内容
     * @var string
     */
    public $type = '';

    /**
     * 相应状态码
     * @var int
     */
    public $status_code = 200;

    /**
     * @var array
     */
    public $header = [];

    /**
     * @param $data
     * @param $type
     * @param int $status_code
     */
    public function __construct($data, $type, int $status_code = 200)
    {
        $this->data = $data;
        $this->type = $type;
        $this->status_code = $status_code;
    }
}
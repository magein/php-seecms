<?php

namespace magein\seecms\response;

use magein\seecms\SeeResponse;

class FileResponse extends SeeResponse
{

    /**
     * @var int
     */
    public $expire = 360;

    /**
     * @var
     */
    public $name;

    /**
     * @var
     */
    public $mime_type;

    /**
     * @var bool
     */
    public $is_content = false;

    /**
     * @var bool
     */
    protected $force = true;

    public function __construct($data, int $status_code = 200)
    {
        parent::__construct($data, 'file', $status_code);
    }

    /**
     * 设置是否为内容 必须配合mimeType方法使用
     * @param bool $content
     * @return $this
     */
    public function isContent(bool $content = true): FileResponse
    {
        $this->is_content = $content;

        return $this;
    }

    /**
     * 设置文件类型
     * @param string $mimeType
     * @return $this
     */
    public function mimeType(string $mimeType): FileResponse
    {
        $this->mime_type = $mimeType;

        return $this;
    }

    /**
     * 设置文件强制下载
     * @param bool $force
     * @return $this
     */
    public function force(bool $force): FileResponse
    {
        $this->force = $force;

        return $this;
    }


    /**
     * 设置有效期
     * @access public
     * @param integer $expire 有效期
     * @return $this
     */
    public function expire(int $expire): FileResponse
    {
        $this->expire = $expire;

        return $this;
    }

    /**
     * 设置下载文件的显示名称
     * @param string $filename 文件名
     * @param bool $extension 后缀自动识别
     * @return $this
     */
    public function name(string $filename, bool $extension = true): FileResponse
    {
        $this->name = $filename;

        if ($extension && false === strpos($filename, '.')) {
            $this->name .= '.' . pathinfo($this->data, PATHINFO_EXTENSION);
        }

        return $this;
    }
}
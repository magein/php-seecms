<?php

namespace magein\seecms\response;

use magein\seecms\SeeResponse;

class RedirectResponse extends SeeResponse
{
    public function __construct($data)
    {
        parent::__construct($data, 'redirect', 302);
    }
}
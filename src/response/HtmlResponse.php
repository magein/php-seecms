<?php

namespace magein\seecms\response;

use magein\seecms\SeeResponse;

class HtmlResponse extends SeeResponse
{
    public function __construct($data, int $status_code = 200)
    {
        parent::__construct($data, 'html', $status_code);
    }
}
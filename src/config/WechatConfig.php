<?php

namespace magein\seecms\config;

class WechatConfig
{
    public $config = [];

    public function __construct($config)
    {
        $this->config = $config;
    }

    /**
     * 微信公众号素材设置
     * @return array|mixed
     */
    public function material()
    {
        return $this->config['material'] ?? [];
    }

    /**
     * 微信公众号素材保存路径
     * @return string
     */
    public function materialRoot(): string
    {
        $root = $this->material()['root'] ?? '';

        return trim(trim($root, '/'), '\\');
    }

    /**
     * 保存路径对应的外部URL路径
     * @return string
     */
    public function materialUrl(): string
    {
        $url = $this->material()['url'] ?? '';

        return trim(trim($url, '/'), '\\');
    }
}
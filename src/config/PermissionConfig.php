<?php

namespace magein\seecms\config;

class PermissionConfig
{
    public $config = [];

    public function __construct($config)
    {
        $this->config = $config;
    }

    /**
     * 自动提取权限规则的字段
     * @return array
     */
    public function auto(): array
    {
        return $this->config['auto'] ?? [];
    }

    /**
     * 跳过验证的路由地址
     * @return array
     */
    public function expects(): array
    {
        return $this->config['expects'] ?? [];
    }
}
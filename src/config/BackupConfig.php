<?php

namespace magein\seecms\config;

class BackupConfig
{
    public $config = [];

    public function __construct($config)
    {
        $this->config = $config;
    }

    /**
     * 备份文件保存基础路径
     * @return string
     */
    public function root(): string
    {
        return rtrim($this->config['root'] ?? '', '/\\');
    }

    /**
     * 备份文件保存路径，拼接root路径
     * @return string
     */
    public function backupPath(): string
    {
        return $this->config['backup_path'] ?? '';
    }

    /**
     * 压缩文件保存路径
     * @return string
     */
    public function zipPath(): string
    {
        return $this->config['zip_path'] ?? '';
    }

    /**
     * 需要备份的表
     * @return array
     */
    public function tables(): array
    {
        return $this->config['tables'] ?? [];
    }

    /**
     * 执行备份的命令提示
     * @return mixed|string
     */
    public function command()
    {
        return $this->config['command'] ?? '';
    }

}
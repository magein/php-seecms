<?php

namespace magein\seecms\config;

class DatabaseConfig
{
    public $config = [];

    public function __construct($config)
    {
        $this->config = $config;
    }

    /**
     * @return string
     */
    public function type(): string
    {
        return $this->config['type'] ?? '';
    }

    /**
     * @return string
     */
    public function host(): string
    {
        return $this->config['host'] ?? '';
    }

    /**
     * @return string
     */
    public function database(): string
    {
        return $this->config['database'] ?? '';
    }

    /**
     * @return string
     */
    public function username(): string
    {
        return $this->config['username'] ?? '';
    }

    /**
     * @return string
     */
    public function password(): string
    {
        return $this->config['password'] ?? '';
    }

    /**
     * @return string
     */
    public function port(): string
    {
        return $this->config['port'] ?? 3306;
    }

    /**
     * @return string
     */
    public function prefix(): string
    {
        return $this->config['prefix'] ?? '';
    }

    /**
     * @return string
     */
    public function charset(): string
    {
        return $this->config['charset'] ?? 3306;
    }
}
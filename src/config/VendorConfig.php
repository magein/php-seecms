<?php

namespace magein\seecms\config;

class VendorConfig
{
    public $config = [];

    public function __construct($config)
    {
        $this->config = $config;
    }

    /**
     * 静态资源版本
     * @return string
     */
    public function version(): string
    {
        return $this->config['version'] ?? '';
    }

    /**
     * 基础路径
     * @return string
     */
    public function root(): string
    {
        return $this->config['root'] ?? '';
    }

    /**
     * 静态资源路径
     * @return array
     */
    public function asset(): array
    {
        return $this->config['asset'] ?? [];
    }

    /**
     * easycms静态资源路径
     * @return string
     */
    public function assetCms(): string
    {
        return rtrim($this->config['asset']['cms'] ?? '', '/');
    }

    /**
     * 插件库
     * @return string
     */
    public function assetLib(): string
    {
        return rtrim($this->config['asset']['lib'] ?? '', '/');
    }
}
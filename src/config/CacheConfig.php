<?php

namespace magein\seecms\config;

class CacheConfig
{
    public $config = [];

    public function __construct($config)
    {
        $this->config = $config;
    }

    public function type()
    {
        return $this->config['type'] ?? 'file';
    }

    public function name()
    {
        return $this->config['name'] ?? 'seecms';
    }


    public function path()
    {
        return $this->config['path'] ?? '';
    }

    public function prefix()
    {
        return $this->config['prefix'] ?? '';
    }

    public function expire()
    {
        return $this->config['expire'] ?? 0;
    }
}
<?php

namespace magein\seecms\config;

class TwigConfig
{
    public $config = [];

    public function __construct($config)
    {
        $this->config = $config;
    }

    public function debug()
    {
        return $this->config['debug'] ?? false;
    }

    /**
     * 模版地址
     * @return array|mixed
     */
    public function templates()
    {
        return $this->config['templates'] ?? [];
    }

    /**
     * 页面字符编码
     * @return string
     */
    public function charset(): string
    {
        return $this->config['charset'] ?? 'utf-8';
    }

    /**
     * 缓存路径
     * @return string
     */
    public function cache(): string
    {
        return $this->config['cache'] ?? false;
    }
}
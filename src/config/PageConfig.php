<?php

namespace magein\seecms\config;

class PageConfig
{
    public $config = [];

    protected $image_fields = ['head', 'avatar', 'icon', 'img', 'image', 'pic', 'picture', 'photo', 'thumb'];

    public function __construct($config)
    {
        $this->config = $config;
    }

    /**
     * 页面的表格设置
     * @return array|mixed
     */
    public function column()
    {
        return $this->config['column'] ?? [];
    }

    /**
     * 自动转化为图片的字段
     * @return mixed|string[]
     */
    public function columnImg()
    {
        return $this->config['column']['img'] ?? $this->image_fields;
    }

    /**
     * 自动转化为开关的字段
     * @return mixed|string[]
     */
    public function columnSwitch()
    {
        return $this->config['column']['switch'] ?? ['status', 'is_hot', 'is_recommend', 'is_publish'];
    }

    /**
     * 页面表单设置
     * @return array
     */
    public function control(): array
    {
        return $this->config['control'] ?? [];
    }

    /**
     * 自动转化为图片的字段
     * @return mixed|string[]
     */
    public function controlImg()
    {
        return $this->config['control']['img'] ?? $this->image_fields;
    }


    /**
     * 自动转化为密码框的字段
     * @return mixed|string[]
     */
    public function controlPassword()
    {
        return $this->config['control']['password'] ?? ['password', 'confirm_password'];
    }

    /**
     * 自动转化为文件的字段
     * @return mixed|string[]
     */
    public function controlFile()
    {
        return $this->config['control']['file'] ?? ['file', 'attachment'];
    }

    /**
     * 自动转化为隐藏域的字段
     * @return mixed|string[]
     */
    public function controlHidden()
    {
        return $this->config['control']['hidden'] ?? ['id', 'user_id'];
    }
}
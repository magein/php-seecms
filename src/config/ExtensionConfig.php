<?php

namespace magein\seecms\config;

class ExtensionConfig
{
    public $config = [];

    public function __construct($config)
    {
        $this->config = $config;
    }
}
<?php

namespace magein\seecms\config;

class DevelopConfig
{
    public $config = [];

    public function __construct($config)
    {
        $this->config = $config;
    }

    /**
     * 开启开发工具菜单
     * @return bool
     */
    public function switch(): bool
    {
        return $this->config['switch'] ?? false;
    }
}
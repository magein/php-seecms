<?php

namespace magein\seecms\config;

class LangConfig
{
    public $config = [];

    /**
     * @param $config
     */
    public function __construct($config)
    {
        $this->config = $config;
    }

    /**
     * @return string
     */
    public function default(): string
    {
        return $this->config['default'] ?? 'zh-cn';
    }

    /**
     * @return string[]
     */
    public function support(): array
    {
        return $this->config['support'] ?? [
            'zh-cn' => '简体中文',
            'en-us' => 'English'
        ];
    }

    /**
     * @return mixed|string
     */
    public function cookieVar(): string
    {
        return $this->config['cookie_var'] ?? '';
    }

    /**
     * @return string
     */
    public function headerVar(): string
    {
        return $this->config['header_var'] ?? '';
    }
}
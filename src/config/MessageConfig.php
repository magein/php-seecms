<?php

namespace magein\seecms\config;

class MessageConfig
{
    public $config = [];

    public function __construct($config)
    {
        $this->config = $config;
    }

    /**
     * 自定义的消息分组
     * @return array|mixed
     */
    public function group()
    {
        return $this->config['group'] ?? [];
    }
}
<?php

namespace magein\seecms\config;

class UploadConfig
{
    public $config = [];

    public function __construct($config)
    {
        $this->config = $config;
    }

    public function root(): string
    {
        return rtrim($this->config['root'] ?? '', '/\\');
    }

    public function url(): string
    {
        return $this->config['url'] ?? '/storage/upload';
    }

    public function directory(): array
    {
        return $this->config['directory'] ?? [];
    }

    public function directoryImage(): string
    {
        return $this->directory()['image'] ?? '/storage/upload/origin/image';
    }

    public function directoryFile(): string
    {
        return $this->directory()['file'] ?? '/storage/upload/origin/file';
    }

    public function directoryThumb(): string
    {
        return $this->directory()['thumb'] ?? '/storage/upload/thumb';
    }

    public function directoryWater(): string
    {
        return $this->directory()['water'] ?? '/storage/upload/water';
    }

    public function validate(): string
    {
        return $this->config['validate'] ?? '';
    }

    public function thumb(): array
    {
        return $this->config['thumb'] ?? [];
    }


    public function thumbSize(): string
    {
        return $this->config['thumb']['size'] ?? '';
    }

    public function thumbType(): int
    {
        return $this->config['thumb']['type'] ?? 1;
    }

    public function water(): array
    {
        return $this->config['water'] ?? [];
    }

    public function waterImage(): array
    {
        return $this->config['water']['image'] ?? [];
    }

    public function waterImageRoot(): string
    {
        return $this->waterImage()['root'] ?? '';
    }

    public function waterImageList()
    {
        return $this->waterImage()['list'] ?? '';
    }

    public function waterText(): array
    {
        return $this->config['water']['text'] ?? [];
    }

    public function waterTextDefault(): string
    {
        return $this->waterText()['default'] ?? '';
    }

    public function waterTextOpen(): bool
    {
        return boolval($this->waterText()['open'] ?? false);
    }

    public function waterTextFont(): string
    {
        return $this->waterText()['font'] ?? '';
    }
}
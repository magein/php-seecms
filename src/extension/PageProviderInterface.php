<?php

declare(strict_types=1);

namespace magein\seecms\extension;

interface PageProviderInterface
{
    public function getPages(): array;

    /**
     * @param $page_name
     * @return string
     */
    public function getPageBuild($page_name): string;
}
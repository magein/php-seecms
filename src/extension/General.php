<?php

declare(strict_types=1);

namespace magein\seecms\extension;

use magein\seecms\Factory;
use Psr\SimpleCache\InvalidArgumentException;

/**
 * @property array|null login
 * @property array|null smtp
 * @property array|null backup
 * @property array|null upload
 * @property array|null website
 * @property array|null wechat
 */
class General
{
    /**
     * 保存
     * @param string $group
     * @param array $settings
     * @return void
     */
    public function save(string $group, array $settings)
    {
        foreach ($settings as $name => $value) {

            if (is_array($value)) {
                $value = json_encode($value, JSON_UNESCAPED_UNICODE);
            }

            Factory::table()->setting->updateOrCreate(compact('group', 'name'), compact('value'));
        }

        $this->clear($group);
    }

    /**
     * @param $name
     * @return array
     * @throws \Psr\Cache\InvalidArgumentException
     */
    public function get($name): array
    {
        $key = 'ecms_general_setting_group_' . $name;
        $data = Factory::cache()->get($key);

        if ($data) {
            return $data;
        }

        $data = Factory::table()->setting->where('group', $name)->column('value', 'name');

        $data && Factory::cache()->set($key, $data);

        return $data ?: [];
    }

    /**
     * @return void
     */
    public function clear($group)
    {
        if ($group === 'all') {
            $groups = ['login', 'smtp', 'database', 'upload', 'website', 'wechat'];
        } else {
            $groups = [$group];
        }

        foreach ($groups as $group) {
            try {
                $key = 'ecms_general_setting_group_' . $group;
                Factory::cache()->delete($key);
            } catch (InvalidArgumentException $e) {

            }
        }
    }

    /**
     * @param $name
     * @return array|null
     */
    public function __get($name)
    {
        try {
            $value = $this->get($name) ?? null;
        } catch (\Psr\Cache\InvalidArgumentException $e) {
            return null;
        }

        return is_array($value) ? $value : null;
    }
}
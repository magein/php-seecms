<?php

declare(strict_types=1);

namespace magein\seecms\extension;

use magein\seecms\db\SeeDbException;
use magein\seecms\Factory;
use magein\seecms\library\Auth;
use magein\seecms\library\Menu;
use magein\seecms\library\Operate;
use magein\seecms\library\UserPermission;
use magein\seecms\SeeLang;
use magein\utils\Client;
use magein\utils\Result;
use magein\utils\Tree;
use Psr\Cache\InvalidArgumentException;

class User
{

    /**
     * @return bool
     */
    public function isRoot(): bool
    {
        return $this->uuid() === 1;
    }

    /**
     * @return int
     */
    public function uuid(): int
    {
        return intval(self::info()['id'] ?? 0);
    }

    /**
     * 设置登录标记
     * @param $user
     * @return string
     * @throws InvalidArgumentException|SeeDbException
     */
    public function makeToken($user): string
    {
        $token = md5(uniqid());
        // 设置登录标识
        setcookie('token', $token, 0, '/');
        // 记录登录信息
        Factory::cache()->set($token, $user, 86400);

        // 更新登录信息
        $data['last_login_ip'] = Client::ip();
        $data['last_login_time'] = date('Y-m-d H:i:s');
        Factory::table()->user->update($data, ['id' => $user['id']]);

        return $token;
    }

    /**
     * 设置或者清空登录信息
     * @param $data
     * @return array|bool
     */
    public function info($data = null)
    {
        $token = $_COOKIE['token'] ?? '';
        if (empty($token)) {
            return [];
        }

        if (empty($data)) {
            return Factory::cache()->get($token, []) ?: [];
        }

        try {
            Factory::cache()->set($token, $data);
        } catch (InvalidArgumentException $exception) {

        }

        return true;
    }

    public function logout()
    {
        try {
            $token = $_COOKIE['token'] ?? '';
            Factory::cache()->delete($token);
            setcookie('token', '', time() - 1);
        } catch (\Psr\SimpleCache\InvalidArgumentException $e) {
        }

    }

    /**
     * 获取用户权限
     * @param string|null $type
     * @return array|mixed|null
     */
    public function permission(string $type = null)
    {
        $permission = UserPermission::init($this->uuid());
        $record = $permission->get();
        if ($type !== null) {
            return $record ? $record[$type] : [];
        }
        return $record;
    }

    /**
     * 清除权限缓存
     * @param $type
     * @return bool
     */
    public function clearCachePermission($type): bool
    {
        try {
            if ($type === 'all') {
                foreach (['role', 'menu', 'rule'] as $item) {
                    $name = sprintf('current_user_%s_list_%s', $item, $this->uuid());
                    Factory::cache()->delete($name);
                }
            } else {
                $name = sprintf('current_user_%s_list_%s', $type, $this->uuid());
                Factory::cache()->delete($name);
            }
        } catch (\Psr\SimpleCache\InvalidArgumentException $e) {

        }

        return true;
    }

    public function role(): array
    {
        $key = 'current_user_role_list_' . $this->uuid();

        // 从缓存中获取
        $cache_role = Factory::cache()->get($key);
        if ($cache_role) {
            return $cache_role;
        }

        $roles = $this->permission(__FUNCTION__);

        $data = Factory::table()->user_role->where('id', 'in', $roles)->column('name', 'id');

        Factory::cache()->set($key, $data);

        return $data ?: [];
    }

    public function menu(): array
    {
        $key = 'current_user_menu_list_' . $this->uuid();
        // 从缓存中获取
        $cache_menu = Factory::cache()->get($key);
        if ($cache_menu) {
            return $cache_menu;
        }

        if (Auth::user()->isRoot()) {
            $menus = Factory::table()->menu->where('status', Menu::STATUS_OPEN)->order('sort asc')->select();
            if (Factory::config()->develop->switch()) {
                $menus[] = Menu::develop();
            }
        } else {
            $menu_ids = $this->permission(__FUNCTION__) ?: [];
            $menus = Factory::table()->menu->where('id', 'in', $menu_ids)->order('sort asc')->select();
        }

        $tree = Tree::init($menus);
        $tree->setParentId('parent_id');
        $tree->setChild('childlist');
        $console_menu = SeeLang::instance()->get('console_menu');
        $data = $tree->transfer(function ($item) use ($console_menu) {
            $title = $item['title'];
            $url = $item['url'] ?? '';
            if ($console_menu) {
                $_title = $console_menu[$url] ?? '';
                $title = $_title ?: $title;
            }
            $item['title'] = $title;
            if (preg_match('/^http/', $url)) {
                $item['href'] = $url;
            } else {
                $item['href'] = Factory::route($url);
            }

            $item['openType'] = $item['open_type'] ?? '';
            return $item;
        });

        Factory::cache()->set($key, $data);

        return $data ?: [];
    }

    public function rule(): array
    {
        $rule_ids = $this->permission(__FUNCTION__) ?: [];

        return Factory::table()->rule->where('id', 'in', $rule_ids)->column('url');
    }

    public function setPassword(): Result
    {
        $old = Factory::request()->post('old');
        $new = Factory::request()->post('new');
        $confirm = Factory::request()->post('confirm');

        if (empty($old)) {
            return Result::error(SeeLang::user('enter_old_passwd'));
        }

        if (empty($new)) {
            return Result::error(SeeLang::user('old_passwd_error'));
        }

        $len = strlen($new);
        if ($len < 6 || $len > 18) {
            return Result::error(SeeLang::user('enter_new_passwd_format'));
        }

        if (empty($confirm)) {
            return Result::error(SeeLang::user('enter_confirm_passwd'));
        }

        if ($new != $confirm) {
            return Result::error(SeeLang::user('new_not_equal_confirm'));
        }

        $user = Factory::table()->user->find(Auth::user()->uuid());
        if (empty($user)) {
            return Result::error(SeeLang::user('not_found'));
        }

        if (!Auth::checkPassword($old, $user['password'], $user['encrypt'])) {
            return Result::error(SeeLang::user('old_passwd_error'));
        }

        Factory::table()->user->where('id', Auth::user()->uuid())
            ->data('password', $new)
            ->update();

        return Result::success(null, SeeLang::user('reset_passwd_success'));
    }

    /**
     * @return Result
     */
    public function verifyPassword(): Result
    {
        $password = Factory::request()->post('password');

        if (empty($password)) {
            return Result::error(SeeLang::user('enter_passwd'));
        }

        $info = Auth::user()->info();
        if (!Auth::checkPassword($password, $info['password'] ?? '', $info['encrypt'] ?? '')) {
            return Result::error(SeeLang::user('passwd_error'));
        }

        // 记录此次验证信息
        $name = md5($info['id'] . '');
        $token = md5(uniqid());
        Factory::cache()->set($name, $token, 600);

        return Result::success($token);
    }


    /**
     * @return Result
     */
    public function setAvatar(): Result
    {
        $upload = new Upload();
        $filepath = $upload->move();

        Factory::table()->user->where('id', Auth::user()->uuid())
            ->data('avatar', $filepath)
            ->update();

        $data = Auth::user()->info();
        $data['avatar'] = $filepath;
        Auth::user()->info($data);

        return Result::success([
            'url' => $filepath
        ]);
    }


    /**
     * @return Result
     */
    public function updateInfo(): Result
    {
        $data = [];
        $data['token'] = Factory::request()->post('token');
        $data['email'] = Factory::request()->post('email');
        $data['nickname'] = Factory::request()->post('nickname');
        $data['phone'] = Factory::request()->post('phone');
        $data = array_filter($data);

        // 验证token
        $token = $data['token'] ?? '';
        if (empty($token)) {
            return Result::error(SeeLang::user('verify_passwd'));
        }

        if ($token !== Factory::cache()->get(md5(Auth::user()->uuid() . ''))) {
            return Result::error(SeeLang::user('verify_passwd_expired'));
        }

        $user = Factory::table()->user->find(Auth::user()->uuid());
        if (empty($user)) {
            return Result::error(SeeLang::user('not_found'));
        }

        $phone = $data['phone'];
        $phone = trim($phone);
        $sub = substr($phone, 3, 4);
        if ($sub == '****') {
            unset($data['phone']);
        }

        $data['id'] = Auth::user()->uuid();

        Factory::table()->user->save($data);

        $info = Auth::user()->info();
        $info = array_merge($info, $data);
        Auth::user()->info($info);

        return Result::success($data, Operate::saveSuccess());
    }

    public function statusOptions(): array
    {
        return [
            0 => SeeLang::switch('forbid'),
            1 => SeeLang::switch('open')
        ];
    }
}
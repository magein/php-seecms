<?php

declare(strict_types=1);

namespace magein\seecms\extension;

use Jenssegers\Agent\Agent;
use magein\seecms\db\SeeDbException;
use magein\seecms\Factory;
use magein\seecms\library\Auth;
use magein\seecms\SeeLang;
use magein\utils\DataSecurity;
use magein\utils\Result;

class UserLogin
{
    // 账号密码登录
    const TYPE_ACCOUNT = 1;

    // 手机号码+验证码
    const TYPE_PHONE_CODE = 2;

    // 微信授权登录
    const TYPE_SCAN_WX = 3;

    // app扫码登录
    const TYPE_SCAN_APP = 4;

    /**
     * @return string[]
     */
    public static function support(): array
    {
        return [
            self::TYPE_ACCOUNT => '账号+密码+图形验证码',
            self::TYPE_PHONE_CODE => '手机号码+动态验证码',
            self::TYPE_SCAN_WX => '微信授权登录',
            self::TYPE_SCAN_APP => 'app扫码登录',
        ];
    }

    public function doLogin($data): Result
    {
        extract($data);

        if ($type == self::TYPE_PHONE_CODE) {
            // 手机号码+验证码登录
            $result = $this->phone($phone, $code);

        } elseif ($type == self::TYPE_SCAN_WX) {
            // 微信扫码登录
            $result = $this->wx($data);

        } elseif ($type == self::TYPE_SCAN_APP) {
            // app扫码登录
            $result = $this->scan($user_id, $token);

        } else {
            // 账号密码登录
            $result = $this->username($username, $password);
        }

        return $result;
    }

    /**
     * 保持登录
     * @param $data
     * @return bool
     */
    public function keepLogin($data): bool
    {
        if (empty($data)) {
            return true;
        }

        extract($data);

        $keep = 'keep';
        if ($keep_login ?? '') {
            // 获取已经设置的数据
            $data = $this->keepLoginData() ?: [];
            // 重置保持登录的数据
            if (isset($username)) {
                $data['username'] = $username;
            }
            if (isset($password)) {
                $data['password'] = $password;
            }
            if (isset($phone)) {
                $data['phone'] = $phone;
            }
            $security = new DataSecurity();
            setcookie($keep, $security->encrypt(json_encode($data)), 0, '/');
        } else {
            setcookie($keep, '');
        }
        return true;
    }

    /**
     * 保持登录数据
     * @return array
     */
    public function keepLoginData(): array
    {
        $keep = 'keep';
        $data = ($_COOKIE[$keep] ?? null) ?: null;
        if (empty($data)) {
            return [];
        }
        return json_decode((new DataSecurity())->decrypt($data), true);
    }


    /**
     * @param $user
     * @param int|null $status
     * @param string $reason
     * @return bool
     */
    public function log($user, int $status = null, string $reason = ''): bool
    {
        try {
            // 记录登录日志
            $agent = new Agent();
            Factory::table()->user_login->insert([
                'user_id' => $user['id'],
                'ip' => request()->ip(),
                'browser' => $agent->browser(),
                'browser_version' => $agent->version($agent->browser()),
                'device' => $agent->device(),
                'platform' => $agent->platform(),
                'platform_version' => $agent->version($agent->platform()),
                'is_mobile' => $agent->isMobile() ? 1 : 0,
                'status' => $status ? 1 : 0,
                'reason' => $reason,
                'user_agent' => $agent->getUserAgent(),
            ]);
        } catch (SeeDbException $dbException) {

        }

        return true;
    }

    /**
     * 扫码登录
     * @return mixed|string
     */
    public function appScan()
    {
        $token = sha1(uniqid());
        // 十分钟有效
        $expire = time() + 600;

        cache($token, $expire);

        return $token;
    }

    /**
     * @param $username
     * @param $password
     * @return Result
     */
    public function username($username, $password): Result
    {
        if (empty($username) || empty($password)) {
            return Result::error(SeeLang::login('username_password_null'));
        }

        $result = $this->checkSecurity();
        if ($result->getCode()) {
            return $result;
        }

        $user = Factory::table()->user->where('username', $username)->find();
        if (empty($user)) {
            return Result::error(SeeLang::user('not_found'));
        }

        if (!Auth::checkPassword($password, $user['password'], $user['encrypt'])) {
            return $this->fail($user, SeeLang::login('username_password_error'));
        }

        if ($user['status'] == 0) {
            return $this->fail($user, SeeLang::login('username_forbid'));
        }

        return $this->success($user);
    }

    /**
     * @param $phone
     * @param $code
     * @return Result
     */
    public function phone($phone, $code): Result
    {
        if (empty($phone)) {
            return Result::error(SeeLang::login('enter_phone'));
        }

        if (empty($code)) {
            return Result::error(SeeLang::login('enter_code'));
        }

        if (Factory::config()->debug()) {
            return Result::error(SeeLang::login('enter_code_error'));
        }

        $result = $this->checkSecurity();
        if ($result->getCode()) {
            return $result;
        }

        $user = Factory::table()->user->where('phone', $phone)->find();
        if (empty($user)) {
            return Result::error(SeeLang::user('not_found'));
        }

        return $this->success($user);
    }

    public function wx($data): Result
    {
        if ($data) {
            $expire_time = $data['expire_time'] ?? '';
            if ($expire_time < time()) {
                return Result::error(SeeLang::login('qrcode_expire'));
            }
            $user_id = $data['user_id'] ?? '';
            if (empty($user_id)) {
                return Result::error(SeeLang::login('qrcode_expire'));
            }

            $user = Factory::table()->user->find($user_id);
            if (empty($user)) {
                return Result::error(SeeLang::user('not_found'));
            }

            return $this->success($user);
        }

        return Result::error(SeeLang::login('fail'));
    }

    public function scan($user_id, $token): Result
    {
        if (empty($token) || empty($user_id)) {
            return Result::error(SeeLang::login('qrcode_refresh'));
        }
        $value = cache($token);
        if (empty($value) || $value < time()) {
            return Result::error(SeeLang::login('qrcode_expire'));
        }

        $user = Factory::table()->user->find($user_id);

        if (empty($user)) {
            return Result::error(SeeLang::user('not_found'));
        }

        return $this->success($user);
    }

    /**
     * 验证登录方式
     * @return Result
     */
    public function checkSecurity(): Result
    {
        $setting = Factory::extension()->general->login;

        extract($setting);
        if ($mac_list ?? '') {
            $mac_list = explode(',', $mac_list);
            if (!in_array($mac_address, $mac_list)) {
                return Result::error(SeeLang::login('mac_forbid'));
            }
        }

        if ($white_list ?? '') {
            $white_list = explode(',', $white_list);
            if (!in_array(request()->ip(), $white_list)) {
                return Result::error(SeeLang::login('ip_forbid'));
            }
        }

        // 验证是否同时登录
        if ($setting['together'] ?? '') {

        }

        return Result::success();
    }

    /**
     * @param $user
     * @param $reason
     * @return Result
     */
    protected function fail($user, $reason = null): Result
    {
        $this->log($user, 0, $reason);

        return Result::error($reason);
    }

    /**
     * @param $user
     * @return Result
     */
    protected function success($user): Result
    {
        $this->log($user, 1);

        $token = Auth::user()->makeToken($user);

        return Result::success($token);
    }
}
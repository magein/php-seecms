<?php

namespace magein\seecms\library\wechat;

use EasyWeChat\Kernel\Exceptions\InvalidArgumentException;
use EasyWeChat\Kernel\Exceptions\InvalidConfigException;
use EasyWeChat\Kernel\Exceptions\RuntimeException;
use EasyWeChat\Kernel\Messages\Image;
use EasyWeChat\Kernel\Messages\Text;
use EasyWeChat\Kernel\Messages\Video;
use EasyWeChat\Kernel\Messages\Voice;
use magein\seecms\Factory;
use magein\seecms\library\Transfer;

class Message
{
    protected $app = '';

    protected $open_id = '';

    protected $content = [];

    protected $setting = [];

    public function __construct($app, $content)
    {
        $this->app = $app;
        $this->content = $content;

        $this->open_id = $content['FromUserName'];

        $this->setting = Factory::extension()->general->wechat;
    }

    /**
     * 用户发送消息
     */
    public function text()
    {
        $setting = $this->setting['keyword'] ?? '';

        if (empty($setting)) {
            return '';
        }

        $setting = json_decode($setting, true);
        $method = $setting['method'];
        $replay = $setting['replay'];

        if (empty($replay)) {
            return '';
        }

        try {

            $msg_content = $this->content['Content'];

            foreach ($replay as $item) {

                $keyword = $item['keyword'] ?? '';
                if (empty($keyword)) {
                    continue;
                }

                $all = false;
                if ($keyword === 'all') {
                    $all = true;
                }

                // 匹配关键字
                $keyword = explode(',', $keyword);
                if ($all || in_array($msg_content, $keyword)) {
                    $messages = $item['message'];
                    foreach ($messages as $vo) {
                        $type = $vo['type'] ?? '';
                        $content = $vo['content'] ?? '';
                        if (empty($type) || empty($content)) {
                            continue;
                        }
                        $message = null;
                        if ($type === 'text') {
                            $message = new Text(Transfer::wechatReplayContentFilter($content));
                        } elseif ($type === 'image') {
                            $message = new Image($content);
                        } elseif ($type === 'voice') {
                            $message = new Voice($content);
                        } elseif ($type === 'video') {
                            $message = new Video($content);
                        }

                        if (isset($message)) {
                            if ($method == 'response') {
                                return $message;
                            } else {
                                $this->app->customer_service->message($message)->to($this->open_id)->send();
                            }
                        }
                    }
                }
            }
        } catch (InvalidArgumentException|InvalidConfigException|RuntimeException $exception) {

        }

        return '';
    }

    public function image(): string
    {
        return '收到了您的图片';
    }

    /**
     * 地理位置
     * 这里是用户主动上报的地址位置，消息类型为location
     * @return string
     */
    public function location(): string
    {
        $x = $this->content['Location_X'] ?? '';
        $y = $this->content['Location_Y'] ?? '';

        $label = $this->content['Label'];

        return '当前位置:' . "\n" . '   ' . $label . "\n" . '   x,y:' . $x . ' , ' . $y;
    }
}
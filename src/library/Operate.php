<?php

namespace magein\seecms\library;

use magein\seecms\SeeLang;

class Operate
{
    public static function required($param = 'id'): string
    {
        return sprintf('%s : %s cannot be empty', SeeLang::operate('parameter_error'), $param);
    }

    public static function email(): string
    {
        return 'Email format error';
    }

    public static function isArray($param): string
    {
        return sprintf('%s : %s requires an array', SeeLang::operate('parameter_format_error'), $param);
    }

    public static function saveSuccess(): string
    {
        return SeeLang::operate('save_success') ?: '保存成功';
    }

    public static function saveFail(): string
    {
        return SeeLang::operate('save_fail') ?: '保存失败';
    }

    public static function delSuccess(): string
    {
        return SeeLang::operate('del_success') ?: '删除成功';
    }

    public static function delFail(): string
    {
        return SeeLang::operate('del_fail') ?: '删除失败';
    }

    public static function notFoundData(): string
    {
        return SeeLang::operate('not_found_data') ?: '没有找到数组';
    }

    public static function saveParameterNull(): string
    {
        return SeeLang::operate('save_parameter_null') ?: '保存参数为不能为空';
    }
}
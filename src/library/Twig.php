<?php

namespace magein\seecms\library;

use magein\seecms\Factory;
use magein\seecms\SeeLang;
use Twig\Environment;
use Twig\Loader\FilesystemLoader;
use Twig\TwigFunction;

class Twig
{

    protected static $instance = null;

    /**
     * 获取twig实例
     * @param array $config
     * @return Environment
     */
    public static function instance(array $config): Environment
    {
        if (!is_null(self::$instance)) {
            return self::$instance;
        }

        $templates = $config['templates'] ?? null;
        if (is_string($templates)) {
            $templates = [$templates];
        }

        $kernel = __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'view';
        array_unshift($templates, $kernel);

        $loader = new FilesystemLoader($templates);
        $twig = new Environment($loader, $config);

        // 添加读取配置文件函数
        $twig->addFunction(new TwigFunction('config', function ($field) {
            return Factory::config()->value($field);
        }));

        // 添加判断是否是数组的函数
        $twig->addFunction(new TwigFunction('is_array', function ($field) {
            return is_array($field);
        }));

        // 添加字符串的判断
        $twig->addFunction(new TwigFunction('is_string', function ($field) {
            return is_string($field);
        }));

        // 添加in_array函数
        $twig->addFunction(new TwigFunction('in_array', function ($field, $array) {
            return in_array($field, $array);
        }));

        // 添加多语言函数
        $lang = SeeLang::instance();
        $twig->addFunction(new TwigFunction('lang', function ($field) use ($lang) {
            return $lang->get($field);
        }));
        $twig->addFunction(new TwigFunction('lang_default', function () {
            return SeeLang::instance()->getLangSet();
        }));

        // 拼接路由地址
        $twig->addFunction(new TwigFunction('route', function ($route) {
            return Factory::route($route);
        }));

        // 添加引入静态资源函数
        $lib = Factory::config()->vendor->assetLib();
        $cms = Factory::config()->vendor->assetCms();
        $twig->addFunction(new TwigFunction('lib', function ($src) use ($lib) {
            return $lib . '/' . trim($src, '/');
        }));
        $twig->addFunction(new TwigFunction('cms', function ($src) use ($cms) {
            return $cms . '/' . trim($src, '/');
        }));

        return self::$instance = $twig;
    }
}
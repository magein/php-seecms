<?php

namespace magein\seecms\library;

use magein\seecms\SeeLang;

class UserRuleApply
{
    /**
     * 状态:待审核
     */
    const STATUS_PADDING = 0;

    /**
     * 状态:已通过
     */
    const STATUS_PASS = 1;

    /**
     * 状态:已经拒绝
     */
    const STATUS_REFUSE = -1;


    /**
     * @param string|int|null $value
     * @return string|array
     */
    public static function translateStatus($value = null)
    {
        $data = [
            self::STATUS_PADDING => SeeLang::rule('verify_padding'),
            self::STATUS_PASS => SeeLang::rule('verify_pass'),
            self::STATUS_REFUSE => SeeLang::rule('verify_refuse')
        ];

        if ($value !== null) {
            return $data[$value] ?? '';
        }

        return $data;
    }
}
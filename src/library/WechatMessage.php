<?php

namespace magein\seecms\library;

use EasyWeChat\OfficialAccount\Application;
use magein\seecms\Factory;
use magein\seecms\library\wechat\Event;
use magein\seecms\library\wechat\Message;

class WechatMessage
{

    /**
     * @var Application|null
     */
    protected $easy_wechat_app = null;

    public function dispatch()
    {
        $get = json_encode($_GET, JSON_UNESCAPED_UNICODE);
        $now = date('Y-m-d H:i:s');
        $input = file_get_contents('php://input');
        \think\facade\Log::write(<<<EOF

$now:
============================
get:
    $get
input:
    $input
EOF
        );

        // 验证消息是否来之微信
        if (!$this->checkSignature()) {
            echo '';
            exit();
        }

        $echostr = $_GET['echostr'] ?? '';
        if ($echostr) {
            echo $echostr;
            exit();
        }

        $this->easy_wechat_app = Auth::wxOfficialAccount();

        $this->easy_wechat_app->server->push(function ($content) {

            $msg_type = $content['MsgType'];
            if ($msg_type == 'event') {
                $class = new Event($this->easy_wechat_app, $content);
                $method = $content['Event'];
            } else {
                $class = new Message($this->easy_wechat_app, $content);
                $method = $content['MsgType'];
            }

            if (method_exists($class, $method)) {
                $result = $class->$method();
            } else {
                $result = '';
            }

            return $result;
        });

        return $this->easy_wechat_app->server->serve()->send();
    }

    /**
     * 验证微信消息来源
     * @return bool
     */
    protected function checkSignature(): bool
    {
        $signature = $_GET['signature'];
        $timestamp = $_GET['timestamp'];
        $nonce = $_GET['nonce'];

        $wechat = Factory::extension()->general->wechat;

        $token = $wechat['token'];
        $params = array($token, $timestamp, $nonce);
        sort($params, SORT_STRING);
        $new_signature = implode($params);
        $new_signature = sha1($new_signature);

        if ($new_signature == $signature) {
            return true;
        }

        return false;
    }
}
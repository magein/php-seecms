<?php

namespace magein\seecms\library;

use magein\seecms\Factory;
use magein\seecms\SeeLang;

class Menu
{
    /**
     * 菜单类型:目录
     */
    const TYPE_DIRECTORY = 0;

    /**
     * 菜单类型:菜单
     */
    const TYPE_MENU = 1;

    /**
     * 打开类型:目录
     */
    const OPEN_TYPE_IFRAME = '_iframe';

    /**
     * 打开类型:菜单
     */
    const OPEN_TYPE_BLANK = '_blank';

    /**
     * 状态:禁用
     */
    const STATUS_FORBID = 0;

    /**
     * 状态:启用
     */
    const STATUS_OPEN = 1;

    /**
     * @param string|int|null $value
     * @return string|array
     */
    public static function translateType($value = null)
    {
        $data = [
            self::TYPE_DIRECTORY => '目录',
            self::TYPE_MENU => '菜单'
        ];

        if ($value !== null) {
            return $data[$value] ?? '';
        }

        return $data;
    }

    public static function translateOpenType($value = null)
    {
        $data = [
            self::OPEN_TYPE_IFRAME => 'iframe',
            self::OPEN_TYPE_BLANK => 'blank'
        ];

        if ($value !== null) {
            return $data[$value] ?? '';
        }

        return $data;
    }

    /**
     * @param string|int|null $value
     * @return string|array
     */
    public static function translateStatus($value = null)
    {
        $data = [
            self::STATUS_FORBID => SeeLang::switch('forbid'),
            self::STATUS_OPEN => SeeLang::switch('open'),
        ];

        if ($value !== null) {
            return $data[$value] ?? '';
        }

        return $data;
    }

    public static function develop(): array
    {
        return [
            'id' => 1000000,
            'parent_id' => 0,
            'name' => 'develop',
            'title' => '开发工具',
            'icon' => 'iconfont icon-equalizer-line',
            'type' => 1,
            'open_type' => NULL,
            'href' => '/admin/s/develop',
            'spacer' => '',
            'childlist' => [
                [
                    'id' => 1000001,
                    'parent_id' => 1000000,
                    'name' => 'develop.trans',
                    'title' => '加载权限',
                    'icon' => 'iconfont icon-history',
                    'type' => 1,
                    'open_type' => NULL,
                    'href' => Factory::route('s/develop/rules'),
                    'childlist' => []
                ],
                [
                    'id' => 1000005,
                    'parent_id' => 1000000,
                    'name' => 'develop.trans',
                    'title' => '数据表提取',
                    'icon' => 'iconfont icon-history',
                    'type' => 1,
                    'open_type' => NULL,
                    'href' => Factory::route('s/develop/model'),
                    'childlist' => []
                ],
                [
                    'id' => 1000010,
                    'parent_id' => 1000000,
                    'name' => 'develop.mock',
                    'title' => 'Mock',
                    'icon' => 'iconfont icon-history',
                    'type' => 1,
                    'open_type' => NULL,
                    'href' => Factory::route('s/develop/mock'),
                    'childlist' => []
                ],
                [
                    'id' => 1000020,
                    'parent_id' => 1000000,
                    'name' => 'develop.apiPost',
                    'title' => 'ApiPost文档',
                    'icon' => 'iconfont icon-history',
                    'type' => 1,
                    'open_type' => NULL,
                    'href' => Factory::route('s/develop/apiPost'),
                    'childlist' => []
                ],
                [
                    'id' => 1000025,
                    'parent_id' => 1000000,
                    'name' => 'develop.icon',
                    'title' => '图标库',
                    'icon' => 'iconfont icon-history',
                    'type' => 1,
                    'open_type' => NULL,
                    'href' => Factory::route('s/develop/icon'),
                    'childlist' => []
                ]
            ],
        ];
    }
}
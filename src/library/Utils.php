<?php

namespace magein\seecms\library;

use magein\seecms\SeeException;

class Utils
{
    /**
     * 获取mac地址
     * @param $info
     * @return string
     */
    public static function getMacAddress($info): string
    {
        $info = mb_convert_encoding($info, 'UTF-8', 'gb2312');

        if (strpos($info, '物理地址')) {
            preg_match('/([\-0-9a-zA-Z]+)/', $info, $matches);
            return $matches[0];
        }

        return '';
    }

    /**
     * ck = create and check
     * @throws SeeException
     */
    public static function ckDir(...$_)
    {
        $dir = array_reduce($_, function ($dir, $item) {
            return $dir . trim($item, '/') . '/';
        });

        if (!is_dir($dir) && !mkdir($dir, 0777, true)) {
            throw new SeeException('Failed to create directory:' . $dir);
        }

        if (!is_writeable($dir)) {
            throw new SeeException('The target directory is not writable:' . $dir);
        }

        return $dir;
    }
}
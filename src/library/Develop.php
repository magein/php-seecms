<?php

declare(strict_types=1);

/**
 * @user magein
 * @date 2024/1/18 17:30
 */

namespace magein\seecms\library;

use magein\seecms\db\SeeDbException;
use magein\seecms\Factory;
use magein\utils\Variable;

class Develop
{

    /**
     *
     * @param string $table
     * @return array
     */
    public function parseTable(string $table): array
    {
        $columns = [];
        $dictionary = [];

        try {
            $attrs = Factory::db()->query("show full columns from $table");

            foreach ($attrs as $attr) {
                $field = $attr['Field'];
                $comment = $attr['Comment'];
                if (in_array($field, ['updated_at', 'deleted_at'])) {
                    continue;
                }
                if ($comment) {
                    $comments = explode(' ', $comment);
                    $dictionary[$field] = current($comments);
                }

                $columns[] = $field;
            }

        } catch (SeeDbException|\Throwable $exception) {

        }

        return compact('columns', 'dictionary');
    }


    /**
     * 列格式化
     * @param $columns
     * @param string $sp
     * @return string
     */
    public function columnFormatter($columns, string $sp = ''): string
    {
        $formatter = '';
        foreach ($columns as $item) {
            $formatter .= "$sp'{$item}',\n";
        }
        return $formatter;
    }

    /**
     * @param $dictionary
     * @param $table_name
     * @return array
     */
    public function columnComment($dictionary, $table_name): array
    {
        $zh_cn = "//zh-cn\n '__$table_name' => [ \n";
        foreach ($dictionary as $key => $item) {
            $zh_cn .= "    '$key' => '{$item}',\n";
        }
        $zh_cn .= '],';

        $en_us = "//en-us\n '__$table_name' => [ \n";
        foreach ($dictionary as $key => $item) {
            $en_us .= "    '$key' => '{$key}',\n";
        }
        $en_us .= ']';

        return compact('zh_cn', 'en_us');
    }

    /**
     * @param array $attrs
     * @return string
     */
    public function columnToAttrs(array $attrs): string
    {
        $property = '/**';
        $property .= "\n";

        foreach ($attrs as $attr) {
            $field = $attr['Field'];
            $type = $attr['Type'];
            if (in_array($field, ['id', 'created_at', 'updated_at', 'deleted_at'])) {
                continue;
            }
            if (preg_match('/int/', $type)) {
                $type = 'integer';
            } elseif (preg_match('/decimal/', $type)) {
                $type = 'float';
            } else {
                $type = 'string';
            }
            $property .= " * @property $type $" . $field;
            $property .= "\n";
        }
        $property = trim($property, "\n");
        $property .= "\n */";

        return $property;
    }

    /**
     * @param $columns
     * @param $table_name
     * @return string
     */
    public function pageBuilder($columns, $table_name): string
    {
        $columns = $this->columnFormatter($columns, '            ');
        $columns = trim($columns, "\n");
        $columns = trim($columns);

        $controls = str_replace("'created_at',", '', $columns);
        $controls = trim($controls, "\n");
        $controls = trim($controls);

        $class_name = Variable::ins()->pascal($table_name);
        $dictionary = Variable::ins()->underline($table_name);
        return <<<EOF
<?php

namespace app\admin\page;

use magein\seecms\page\Builder;

class {$class_name}Page extends Build
{
    protected \$title = '';
    
    protected \$table_name = '';

    protected \$dictionary = '$dictionary';
    
    public function getColumns(): array
    {
        return [
            $columns
        ];
    }
    
    public function getControls(): array
    {
        return [
            $controls
        ];
    }
}
EOF;
    }
}
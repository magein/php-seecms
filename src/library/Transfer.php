<?php

declare(strict_types=1);

namespace magein\seecms\library;

use magein\seecms\page\unit\query\BetweenQuery;
use magein\seecms\page\unit\query\Query;
use magein\seecms\page\unit\query\TextQuery;
use magein\seecms\SeeLang;

class Transfer
{
    /**
     * @return string[]
     */
    public static function wechatMaterialType(): array
    {
        return [
            'image' => SeeLang::wechat('image'),
            'thumb' => SeeLang::wechat('thumb'),
            'voice' => SeeLang::wechat('voice'),
            'video' => SeeLang::wechat('video'),
        ];
    }

    public static function wechatReplayContentFilter($content)
    {
        return str_replace('\n', "\n", $content);
    }

    /**
     * @param $filesize
     * @return string
     */
    public static function filesizeFormatter($filesize): string
    {
        $filesize = intval($filesize);
        if ($filesize <= 0) {
            return '';
        }

        if ($filesize < 1024) {
            return $filesize . 'byte';
        }

        if ($filesize > 1024 * 1024) {
            return sprintf('%.2fmb', $filesize / (1024 * 1024));
        }

        return sprintf('%.2fkb', $filesize / 1024);
    }

    /**
     * @param $query
     * @param $params
     * @return array
     */
    public static function pageQuery($query, $params): array
    {
        $where = [];
        if ($query) {

            foreach ($query as $item) {
                if (!$item instanceof Query) {
                    $item = TextQuery::init($item);
                }

                $name = trim($item->getName());
                $express = $item->getExpress();

                if ($item instanceof BetweenQuery) {
                    $min = $params[$name . '_min'] ?? '';
                    $max = $params[$name . '_max'] ?? '';
                    if ($min > $max) {
                        list($min, $max) = [$max, $min];
                    }

                    if ($min === '') {
                        $min = 0;
                    }

                    if ($max === '') {
                        $max = 0;
                    }

                    $value = [$min, $max];
                } else {
                    $value = $params[$name] ?? null;
                    if (is_string($value)) {
                        $value = trim($value);
                    }
                }

                if ($value !== null && $value !== '') {
                    if ($express === 'date') {
                        $express = 'between';
                        if (strpos($value, '~') !== false) {
                            // 区间
                            $range = explode('~', $value);
                            $start = trim($range[0] ?? '');
                            $end = trim($range[1] ?? '');
                            $end = $end ?: $start;
                            if ($end <= 10) {
                                $end = date('Y-m-d', strtotime($end . ' +1 day'));
                            }
                            $value = [$start, $end];
                        } else {
                            $value = [$value, date('Y-m-d', strtotime($value . ' +1 day'))];
                        }
                    } elseif ($express === 'like') {
                        $value = "%$value%";
                    } elseif ($express == 'in') {
                        $value = explode(',', $value);
                    }
                    $where[$name] = [$express, $value];
                }
            }
        }

        return $where;
    }
}
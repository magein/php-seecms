<?php

namespace magein\seecms\library;

use magein\seecms\extension\General;
use magein\seecms\extension\PageProviderInterface;
use magein\seecms\extension\Upload;
use magein\seecms\extension\User;
use magein\seecms\extension\UserLogin;
use magein\seecms\SeeConfig;
use magein\seecms\SeeException;

/**
 * @property User $user
 * @property UserLogin $user_login
 * @property Upload $upload
 * @property General $general
 * @property PageProviderInterface $page_provider
 */
class Extension
{

    /**
     * @throws SeeException
     */
    public function __get($property)
    {
        return $this->getExtensionClass($property);
    }

    /**
     * @param $name
     * @return mixed|null
     * @throws SeeException
     */
    protected function getExtensionClass($name)
    {
        $extensions = [
            // 用户登录
            'user_login' => UserLogin::class,
            // 用户信息
            'user' => User::class,
            // 通用设置
            'general' => General::class,
            // 上传设置
            'upload' => Upload::class
        ];

        $extensions = array_merge($extensions, SeeConfig::instance()->extension->config);

        $namespace = $extensions[$name] ?? '';

        if (empty($namespace)) {
            SeeException::json('尚未配置' . $name . '扩展');
        }

        $class = null;
        $message = '';
        try {
            if (class_exists($namespace)) {
                $class = new $namespace();
            }
        } catch (\Exception $exception) {
            $message = $exception->getMessage();
        }

        if ($message) {
            SeeException::json($message);
        }

        return $class;
    }
}
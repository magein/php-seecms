<?php

declare(strict_types=1);

namespace magein\seecms\library;

use EasyWeChat\OfficialAccount\Application;
use magein\seecms\extension\User;
use magein\seecms\Factory;

class Auth
{

    public static function user(): User
    {
        return Factory::extension()->user;
    }

    /**
     * 密码加密
     * @param $password
     * @param $encrypt
     * @return string
     */
    public static function password($password, $encrypt): string
    {
        $password = trim($password);
        $encrypt = trim($encrypt);

        return sha1($encrypt . $password);
    }

    /**
     * @param $password
     * @param string $user_password
     * @param string $user_encrypt
     * @return bool
     */
    public static function checkPassword($password, string $user_password, string $user_encrypt): bool
    {
        if (empty($password) || empty($user_password) || empty($user_encrypt)) {
            return false;
        }

        if ($user_password !== self::password(trim($password), $user_encrypt)) {
            return false;
        }

        return true;
    }

    /**
     * @param array $config
     * @return Application
     */
    public static function wxOfficialAccount(array $config = []): Application
    {
        $wechat = Factory::extension()->general->wechat;

        if (empty($config['app_id'] ?? '')) {
            $config['app_id'] = $wechat['app_id'] ?? '';
        }

        if (empty($config['secret'] ?? '')) {
            $config['secret'] = $wechat['app_secret'] ?? '';
        }

        return \EasyWeChat\Factory::officialAccount($config);
    }
}
<?php

namespace magein\seecms\library;

use magein\seecms\Factory;
use magein\seecms\response\HtmlResponse;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

class View
{
    /**
     * @param $name
     * @param array $vars
     * @return HtmlResponse
     */
    public static function fetch($name, array $vars = []): HtmlResponse
    {
        $name .= '.twig';

        try {
            $html = Factory::twig()->render($name, $vars);
        } catch (LoaderError|RuntimeError|SyntaxError $error) {
            $class = get_class($error);
            $line = $error->getLine();
            $file = $error->getFile();
            $message = $error->getMessage();
            $html = <<<EOF
<div style="margin-top: 30px"></div>
<h1>$message</h1>
<div style="margin-top: 30px"></div>
<h3>$class in  $file line $line </h3>
EOF;
        }

        return new HtmlResponse($html);
    }
}
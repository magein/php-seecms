<?php

namespace magein\seecms\db;

use Exception;

class SeeDbException extends Exception
{
    // 默认错误码
    const ERROR = 10000;

    /**
     * 常规错误
     */
    const TYPE_NOT_ALLOW = 10101;
    // DB链接失败
    const LINK_FAILURE = 10103;
    // 未设置查询的表
    const TABLE_NULL_FOUND = 10105;
    // 参数绑定失败
    const BIND_VALUE_ERROR = 10300;

    /**
     * 解析where条件失败
     */
    const PARSE_WHERE_ERROR = 10201;
    // 多维查询条件只支持索引数组
    const PARSE_WHERE_EXPRESS_ERROR = 10203;
    // 查询条件为空
    const WHERE_IS_NULL = 10205;
    // 查询语句为空
    const SQL_IS_NULL = 10207;
    // 场景查询不存在
    const SCOPE_NULL_FOUND = 10209;

    /**
     * 插入事件
     * 插入数据为空
     */
    const INSERT_DATA_NULL = 10301;


    /**
     * 事件回调异常
     */
    const EVENT_CALLBACK_FAILED = 10400;

    /**
     * PDO异常，因为pdo返回的code有可能是字符串，所以这里统一使用一个错误码，可通过获取异常实例的 getPrevious() 方法获取PDO异常实例，再进行获取code
     */
    const PDO_EXCEPTION = 40000;

    /**
     * 链接实例
     *
     * @var SeeDbConnection
     */
    protected $connection;

    /**
     * 构造方法
     *
     * @param string $message
     * @param integer $code
     * @param SeeDbConnection $connection
     * @param Exception $previous
     */
    public function __construct($message, $code = self::ERROR, $connection = null, $previous = null)
    {
        parent::__construct($message, $code, $previous);
        $this->connection = $connection;
    }

    /**
     * 获取SQL
     *
     * @return SeeDbConnection
     */
    public function getConnection(): ?SeeDbConnection
    {
        return $this->connection;
    }
}

<?php

namespace magein\seecms\db;


use magein\seecms\Factory;

/**
 * @property SeeDbQuery $attachment
 * @property SeeDbQuery $database_backup
 * @property SeeDbQuery $menu
 * @property SeeDbQuery $rule
 * @property SeeDbQuery $setting
 * @property SeeDbQuery $user_login
 * @property SeeDbQuery $user_message
 * @property SeeDbQuery $user_permission
 * @property SeeDbQuery $user_role
 * @property SeeDbQuery $user_rule_apply
 * @property SeeDbQuery $user
 * @property SeeDbQuery $wechat_material
 */
class SeeDbTable
{
    public function __get($name)
    {
        return SeeDb::connect()->table(Factory::config()->database->prefix() . $name);
    }
}
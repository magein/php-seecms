<?php

declare(strict_types=1);

namespace magein\seecms;

use magein\seecms\config\BackupConfig;
use magein\seecms\config\CacheConfig;
use magein\seecms\config\DatabaseConfig;
use magein\seecms\config\DevelopConfig;
use magein\seecms\config\ExtensionConfig;
use magein\seecms\config\LangConfig;
use magein\seecms\config\MessageConfig;
use magein\seecms\config\PageConfig;
use magein\seecms\config\PermissionConfig;
use magein\seecms\config\RightToolbarConfig;
use magein\seecms\config\TwigConfig;
use magein\seecms\config\UploadConfig;
use magein\seecms\config\VendorConfig;
use magein\seecms\config\WechatConfig;
use magein\utils\Variable;

/**
 * 配置使用单列模式
 * @property BackupConfig $backup
 * @property CacheConfig $cache
 * @property DatabaseConfig $database
 * @property DevelopConfig $develop
 * @property ExtensionConfig $extension
 * @property LangConfig $lang
 * @property MessageConfig $message
 * @property PageConfig $page
 * @property PermissionConfig $permission
 * @property RightToolbarConfig $right_toolbar
 * @property TwigConfig $twig
 * @property UploadConfig $upload
 * @property VendorConfig $vendor
 * @property WechatConfig $wechat
 */
class SeeConfig
{
    /**
     * @var SeeConfig|null
     */
    protected static $instance = null;

    /**
     * @var array
     */
    protected $config = [];

    protected function __construct()
    {

    }

    protected function __clone()
    {

    }

    /**
     * @return SeeConfig
     */
    public static function instance(): SeeConfig
    {
        if (is_null(self::$instance)) {
            self::$instance = new self();
            // 加载默认配置
            self::$instance->loadDefaultConfig();
        }

        return self::$instance;
    }

    private function loadDefaultConfig()
    {
        $config = [];

        $files = glob(__DIR__ . '/../config/*.php');

        if ($files) {
            foreach ($files as $file) {
                $config = array_merge($config, include $file);
            }
        }

        $this->config = $config;
    }

    /**
     * @param $config
     * @return void
     */
    public function setConfig($config)
    {
        if ($config && is_array($config)) {
            $this->config = array_merge($this->config, $config);
        }
    }

    /**
     * 获取配置文件的值
     * @param $name
     * @return array|mixed
     */
    public function value($name)
    {
        return $this->config[$name] ?? [];
    }

    /**
     * @return bool
     */
    public function debug(): bool
    {
        $debug = boolval($this->config['debug'] ?? false);

        return $debug ?: false;
    }

    public function __get($name)
    {
        $namespace = Variable::ins()->pascal($name) . 'Config';
        $class = 'magein\seecms\config\\' . $namespace;
        if (class_exists($class)) {
            return new $class($this->config[$name] ?? []);
        }
    }
}
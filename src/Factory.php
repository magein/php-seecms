<?php

declare(strict_types=1);

namespace magein\seecms;

use magein\seecms\db\SeeDb;
use magein\seecms\db\SeeDbConnection;
use magein\seecms\db\SeeDbTable;
use magein\seecms\library\Extension;
use magein\seecms\library\Twig;
use Twig\Environment;

class Factory
{

    /**
     * 获取缓存文件
     * @return SeeCache
     */
    public static function cache(): SeeCache
    {
        return SeeCache::instance();
    }

    /**
     * 获取配置文件
     * @return SeeConfig
     */
    public static function config(): SeeConfig
    {
        return SeeConfig::instance();
    }

    /**
     * 获取参数
     * @return SeeRequest
     */
    public static function request(): SeeRequest
    {
        return SeeRequest::instance();
    }

    /**
     * 获取twig实例
     * @return Environment
     */
    public static function twig(): Environment
    {
        static $twig = null;

        if (!is_null($twig)) {
            return $twig;
        }

        $config = self::config()->twig->config;

        return Twig::instance($config);
    }

    /**
     * 获取扩展文件
     * @return Extension
     */
    public static function extension(): Extension
    {
        return new Extension();
    }

    /**
     * 获取模型实例
     */
    public static function db(): SeeDbConnection
    {
        return SeeDb::connect();
    }

    /**
     * @return SeeDbTable
     */
    public static function table(): SeeDbTable
    {
        return new SeeDbTable();
    }


    /**
     * 拼接路由
     * @param $route
     * @return string
     */
    public static function route($route): string
    {
        $callable = SeeConfig::instance()->value('route');

        if (is_callable($callable)) {
            return $callable($route);
        }

        return '/admin/' . trim($route, '/');
    }
}
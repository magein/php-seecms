# Seecms

php构建的内容管理系统

[gitee](https://gitee.com/magein/php-seecms) [packagist](https://gitee.com/magein/seecms)

### 简介

项目灵感来源： [YznCMS 后台开发框架](https://gitee.com/ken678/YZNCMS)

基础页面参考YznCMS，后端逻辑全部重构

1. 后端不依赖于框架
2. 支持composer安装
3. 支持多语言
4. curd页面快速构建
5. 使用twig模版进行渲染
6. 可以集成到thinkphp、laravel等框架里面

### 基础功能

1. 登录验证
2. 站点配置
3. smtp服务配置
4. 上传配置
5. 短信配置
6. 附件管理
7. 微信公众号配置
8. 数据库备份(mysql、mariadb)
9. 用户相关、菜单、分组、权限

### 框架扩展


[think-seecms](https://gitee.com/magein/think-seecms)


[laravel-seecms](https://gitee.com/magein/laravel-seecms)
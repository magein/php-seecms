## 常见问题

### 菜单

参见：http://www.pearadmin.com/doc/

前段菜单字段说明：

    id: 菜单数据的唯一标识
    title: 界面中所显示的菜单标题
    icon: 图标
    type : 菜单类型 0: 目录（父级菜单） 1: 菜单
    openType: 当 type 为 1 时，openType 生效，_iframe 正常打开 _blank 新建浏览器标签页
    href: 菜单类型下访问的页面
    children: 目录类型下，该目录下菜单的数组数据

#### 修改首页的tab参数无效

> 需要清空浏览器缓存

### 富文本

#### tinymce

语言包：https://www.tiny.cloud/get-tiny/language-packages/

采用弹出框形式展示的时候，功能样式会被覆盖(替换的文件已经删除，等待重新测试)：

参考：https://gitee.com/chick1993/layui-tinymce#q1-%E5%9C%A8%E5%BC%B9%E7%AA%97%E9%87%8C%E9%9D%A2%E4%BD%BF%E7%94%A8%E6%97%B6%E8%8F%9C%E5%8D%95%E5%AE%9A%E4%BD%8D%E5%9C%A8%E5%BC%B9%E7%AA%97%E4%B8%8B%E9%9D%A2%E6%80%8E%E4%B9%88%E5%8A%9E

修改public/vendor/icms/libs/plugins/tinymce/skins/ui/oxide/skin.min.css中的z-index值

使用正则替换：

```
# 表达式
z-index:(\d+)
# 替换的内容
z-index:$100000
```

      

      
<?php

return [
    /*
    |--------------------------------------------------------------------------
    | seecms page
    |--------------------------------------------------------------------------
    |
    | 构建页面配置
    |
    */
    'page' => [
        // 表单控件配置
        'column' => [
            // 字段自动识别成图片
            'img' => ['head', 'avatar', 'icon', 'img', 'image', 'pic', 'picture', 'photo', 'thumb'],
            // 识别成开关
            'switch' => ['status', 'is_hot', 'is_recommend', 'is_publish']
        ],
        // 表单控件配置
        'control' => [
            // 字段自动识别成图片
            'img' => ['head', 'avatar', 'icon', 'img', 'image', 'pic', 'picture', 'photo', 'thumb'],
            // 字段识别成密码框
            'password' => ['password', 'confirm_password'],
            // 字段识别成文件
            'file' => ['file', 'document'],
            // 识别成隐藏域的字段 默认值：['id', 'user_id']
            'hidden' => ['id'],
        ],
    ],
];
<?php

return [
    /*
    |--------------------------------------------------------------------------
    | seecms editor
    |--------------------------------------------------------------------------
    |
    | 构建页面配置
    |
    */
    'editor' => [
        'tinymce' => [
            /**
             * 插件：可选值
             * accordion accordion advlist anchor autolink autoresize autosave charmap code codesample directionality emoticons fullscreen image importcss insertdatetime link lists media nonbreaking pagebreak preview quickbars save searchreplace table template visualblocks visualchars wordcount
             */
            'plugins' => 'accordion accordion advlist anchor autolink autoresize charmap code codesample directionality emoticons fullscreen image importcss insertdatetime link lists media nonbreaking pagebreak preview quickbars save searchreplace table template visualblocks visualchars wordcount',
            'toolbar' => 'image fullscreen',
        ],
    ],
];
-- ----------------------------
-- Records of seecms_menu
-- ----------------------------
INSERT INTO `seecms_menu` VALUES (1, 0, '用户管理', 'iconfont icon-user-settings-line', '', '', 0, '_iframe', '', 20, 1, '2024-03-09 02:37:53', '2024-08-15 14:55:09', NULL);
INSERT INTO `seecms_menu` VALUES (2, 1, '用户管理', 'iconfont icon-user-settings-line', 'p/user/page', '', 1, '_iframe', '', 10, 1, '2024-03-09 02:37:53', '2024-03-09 02:37:53', NULL);
INSERT INTO `seecms_menu` VALUES (3, 1, '角色管理', 'iconfont icon-user-shared-2-line', 'p/userRole/page', '', 1, '_iframe', '', 20, 1, '2024-03-09 02:37:53', '2024-03-09 02:37:53', NULL);
INSERT INTO `seecms_menu` VALUES (4, 1, '菜单管理', 'iconfont icon-other', 's/permission/menu', '', 1, '_iframe', '', 30, 1, '2024-03-09 02:37:53', '2024-03-09 02:37:53', NULL);
INSERT INTO `seecms_menu` VALUES (5, 1, '权限规则', 'iconfont icon-user-settings-line', 'p/rule/page', '', 1, '_iframe', '', 40, 1, '2024-03-09 02:37:53', '2024-03-09 02:37:53', NULL);
INSERT INTO `seecms_menu` VALUES (6, 1, '权限申请', 'iconfont icon-unlock', 'p/userRuleApply/page', '', 1, '_iframe', '', 50, 1, '2024-03-09 02:37:53', '2024-09-12 11:07:44', NULL);
INSERT INTO `seecms_menu` VALUES (7, 1, '登录日志', 'iconfont icon-history', 'p/userLogin/page', '', 1, '_iframe', '', 999, 1, '2024-03-09 02:37:53', '2024-03-09 02:37:53', NULL);
INSERT INTO `seecms_menu` VALUES (8, 0, '常规管理', 'iconfont icon-setup', '', '', 0, '_iframe', '', 10, 1, '2024-03-09 02:37:53', '2024-08-20 20:48:47', NULL);
INSERT INTO `seecms_menu` VALUES (9, 8, '系统配置', 'iconfont icon-manage', 's/general/setting', '', 1, '_iframe', '', 100, 1, '2024-03-09 02:37:53', '2024-09-12 10:59:17', NULL);
INSERT INTO `seecms_menu` VALUES (10, 8, '附件管理', 'iconfont icon-picture', 'p/attachment/page', '', 1, '_iframe', '', 200, 1, '2024-03-09 02:37:53', '2024-09-12 11:00:21', NULL);
INSERT INTO `seecms_menu` VALUES (11, 8, '数据库备份', 'iconfont icon-database-2-line', 's/database/setting', '', 1, '_iframe', '', 300, 1, '2024-03-09 02:37:53', '2024-09-12 11:03:30', NULL);
INSERT INTO `seecms_menu` VALUES (12, 8, '微信设置', 'iconfont icon-wechat-2-fill', 's/wechat/setting', '', 1, '_iframe', '', 400, 1, '2024-09-02 20:09:36', '2024-09-12 10:55:52', NULL);
INSERT INTO `seecms_menu` VALUES (13, 8, '帮助中心', 'iconfont icon-oschina', 'https://gitee.com/magein/seecms', '', 1, '_blank', '', 973, 1, '2024-03-09 02:37:53', '2024-09-12 15:58:33', NULL);


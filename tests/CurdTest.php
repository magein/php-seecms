<?php

namespace magein\seecms\tests;

use magein\seecms\Factory;
use magein\seecms\library\Auth;
use magein\utils\Faker;
use PHPUnit\Framework\TestCase;

class CurdTest extends TestCase
{
    public function testInsert()
    {
        test_container(function () {

            $password = '123456';
            $encrypt = rand(1000, 99999) . '';
            $data['username'] = 'test_' . rand(1000000, 9999999);
            $data['nickname'] = 'php_unit_test_' . rand(1000, 9999);
            $data['phone'] = Faker::phone();
            $data['email'] = Faker::email();
            $data['encrypt'] = $encrypt;
            $data['password'] = Auth::password($password, $encrypt);

            $res = Factory::table()->user->insert($data);
            $this->assertIsInt($res);

        }, self::class, __FUNCTION__);
    }

    public function testInsertAll()
    {
        test_container(function () {

            $list = [];
            $password = '123456';
            for ($i = 0; $i < 3; $i++) {
                $encrypt = rand(1000, 99999) . '';
                $data = [];
                $data['username'] = 'batch_test_' . rand(1000000, 9999999);
                $data['nickname'] = 'php_unit_test_' . rand(1000, 9999);
                $data['phone'] = Faker::phone();
                $data['email'] = Faker::email();
                $data['encrypt'] = $encrypt;
                $data['password'] = Auth::password($password, $encrypt);
                $data['created_at'] = $data['updated_at'] = date('Y-m-d H:i:s');
                $list[] = $data;
            }

            $res = Factory::table()->user->insertAll($list);

            $this->assertIsInt($res);

        }, self::class, __FUNCTION__);
    }

    public function testFind()
    {
        test_container(function () {

            $result = Factory::table()->user->find();
            $this->assertNull($result);

            $result = Factory::table()->user->order('id desc')->find();
            $this->assertIsArray($result);
            $user_id = $result['id'] ?? '';
            $this->assertNotEmpty($user_id);
            $this->assertIsInt($user_id);

            $result = Factory::table()->user->find($user_id);
            $this->assertIsArray($result);
            $this->assertEquals($result['id'], $user_id);

        }, self::class, __FUNCTION__);
    }

    public function testUpdate()
    {
        test_container(function () {

            $result = Factory::table()->user->order('id desc')->find();
            $this->assertIsArray($result);
            $user_id = $result['id'] ?? '';
            $this->assertNotEmpty($user_id);

            $status = $result['status'];

            $result = Factory::table()->user->update(['status' => $status ? 0 : 1], ['id' => $user_id]);
            $this->assertEquals(1, $result);

            $result = Factory::table()->user->where('id', $user_id)->update(['last_login_time' => date('Y-m-d H:i:s')]);
            $this->assertEquals(1, $result);

        }, self::class, __FUNCTION__);
    }

    public function testSelect()
    {
        test_container(function () {

            $result = Factory::table()->user->order('id desc')->select();
            $this->assertIsArray($result);

            $total = Factory::table()->user->count();
            $this->assertEquals(count($result), $total);

            $current = current($result);
            $end = end($result);
            $max = Factory::table()->user->max('id');
            $this->assertEquals($max, $current['id']);

            $min = Factory::table()->user->min('id');
            $this->assertEquals($min, $end['id']);

        }, self::class, __FUNCTION__);
    }

    public function testPaginate()
    {
        test_container(function () {

            $result = Factory::table()->user->order('id desc')->paginate();
            $this->assertIsArray($result);
            $this->assertCount(15, $result['data'] ?? []);

        }, self::class, __FUNCTION__);
    }

    public function testColumn()
    {
        test_container(function () {

            $result = Factory::table()->user->order('id desc')->column('nickname', 'id');
            $this->assertIsArray($result);

            $sum = Factory::table()->user->sum('id');
            $this->assertEquals(intval($sum), array_sum(array_keys($result)));

        }, self::class, __FUNCTION__);
    }
}
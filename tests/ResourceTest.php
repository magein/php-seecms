<?php

namespace magein\seecms\tests;

use magein\seecms\controller\Resource;
use magein\seecms\Factory;
use magein\seecms\page\Builder;
use magein\seecms\page\builder\AttachmentPage;
use magein\seecms\page\builder\MenuPage;
use magein\seecms\page\builder\RulePage;
use magein\seecms\page\builder\UserLoginPage;
use magein\seecms\page\builder\UserPage;
use magein\seecms\page\builder\UserRolePage;
use magein\seecms\page\builder\UserRuleApplyPage;
use PHPUnit\Framework\TestCase;

/**
 * 资源路由单元测试
 */
class ResourceTest extends TestCase
{

    protected function builder(Builder $builder)
    {
        Factory::request()->set('get', ['page_id' => 1, 'page_size' => 1]);
        Factory::request()->set('post', []);

        test_container(function () use ($builder) {

            $operate_assert = function ($response) {
                $data = $response->data;
                $this->assertIsArray($data);
                $this->assertArrayHasKey('code', $data);
                $this->assertEquals(1, $data['code'] ?? 0);
            };

            $resource = new Resource($builder);
            $response = $resource->index();

            $data = $response->data;
            $this->assertIsArray($data);

            // 读取
            $response = $resource->read();
            $this->assertEquals('html', $response->type);

            // 编辑
            $response = $resource->edit();
            $this->assertEquals('html', $response->type);

            // 执行更新
            $response = $resource->update();
            $operate_assert($response);

            // 新增
            $response = $resource->create();
            $this->assertEquals('html', $response->type);

            // 保存
            $response = $resource->save();
            $operate_assert($response);

            // 删除
            $response = $resource->delete();
            $operate_assert($response);
        }, self::class, $builder->getPageName());
    }

    public function testAttachment()
    {
        $this->builder(new AttachmentPage());
    }

    public function testMenu()
    {
        $this->builder(new MenuPage());
    }

    public function testRule()
    {
        $this->builder(new RulePage());
    }

    public function testUserLogin()
    {
        $this->builder(new UserLoginPage());
    }

    public function testUser()
    {
        $this->builder(new UserPage());
    }

    public function testUserRole()
    {
        $this->builder(new UserRolePage());
    }

    public function testUserApply()
    {
        $this->builder(new UserRuleApplyPage());
    }
}
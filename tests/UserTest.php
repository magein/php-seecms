<?php

namespace magein\seecms\tests;

use PHPUnit\Framework\TestCase;

class UserTest extends TestCase
{
    /**
     * 请求登录页面
     * @return void
     */
    public function testIndex()
    {
        $response = get('admin/index');
        $this->assertEquals(200, $response->getStatusCode());
    }

    /**
     * 用户登录
     * @return void
     */
    public function testLogin()
    {
        $username = 'root';
        $password = '123456';

        $response = post('admin/u/user/login', compact('username', 'password'));
        $this->assertEquals(200, $response->getStatusCode());

        $response->getBody()->rewind();
        $content = $response->getBody()->getContents();
        $content = json_decode($content, true);
        $this->assertNotEmpty($content['data'] ?? '');
    }

    public function testLogout()
    {
        $response = post('admin/u/user/logout');
        $this->assertEquals(200, $response->getStatusCode());
    }
}
<?php

namespace magein\seecms\tests;

use magein\seecms\controller\Profile;
use magein\seecms\Factory;
use PHPUnit\Framework\TestCase;

class ProfileTest extends TestCase
{
    public function testIndex()
    {
        test_container(function () {
            $profile = new Profile();
            $response = $profile->index();
            $this->assertEquals('html', $response->type);
        }, self::class, __FUNCTION__);
    }

    public function testLoginLog()
    {
        test_container(function () {
            $profile = new Profile();
            $response = $profile->login();
            $this->assertEquals('json', $response->type);
        }, self::class, __FUNCTION__);
    }

    public function testSetPassword()
    {
        test_container(function () {
            $profile = new Profile();

            $old = '1234567';
            $new = '1234';
            $confirm = '3214';
            Factory::request()->set('post', compact('old', 'new', 'confirm'));
            $response = $profile->setPassword();

            $this->assertEquals('json', $response->type);
            $this->assertEquals('新密码长度需要6~18位', $response->data['msg']);

            $new = '654321';
            $confirm = '65432';
            Factory::request()->set('post', compact('old', 'new', 'confirm'));
            $response = $profile->setPassword();
            $this->assertEquals('json', $response->type);
            $this->assertEquals('新密码与确认密码不一致', $response->data['msg']);

            $confirm = '654321';
            Factory::request()->set('post', compact('old', 'new', 'confirm'));
            $response = $profile->setPassword();

            $this->assertEquals('json', $response->type);
            $this->assertEquals('用户不存在', $response->data['msg']);

        }, self::class, __FUNCTION__);
    }

    public function testVerifyPassword()
    {
        test_container(function () {

            $profile = new Profile();
            $response = $profile->verifyPassword();
            $this->assertEquals('json', $response->type);
            $this->assertEquals('请输入您的密码', $response->data['msg']);

            Factory::request()->set('post', ['password' => '1234']);

            $response = $profile->verifyPassword();
            $this->assertEquals('json', $response->type);
            $this->assertEquals('您的密码错误', $response->data['msg']);
        }, self::class, __FUNCTION__);
    }

    public function testUpdateInfo()
    {
        test_container(function () {

            $profile = new Profile();
            $response = $profile->updateInfo();
            $this->assertEquals('json', $response->type);
            $this->assertEquals('请先验证密码', $response->data['msg']);

            Factory::request()->set('post', ['token' => '1234']);
            $response = $profile->updateInfo();
            $this->assertEquals('json', $response->type);
            $this->assertEquals('验证信息已经失效，请重新验证', $response->data['msg']);
        }, self::class, __FUNCTION__);
    }
}
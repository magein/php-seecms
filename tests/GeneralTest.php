<?php

namespace magein\seecms\tests;

use magein\seecms\controller\General;
use magein\seecms\Factory;
use PHPUnit\Framework\TestCase;

class GeneralTest extends TestCase
{
    public function testIndex()
    {
        test_container(function () {
            $profile = new General();
            $response = $profile->setting();
            $this->assertEquals('html', $response->type);
        }, self::class, __FUNCTION__);
    }

    public function testSaveSetting()
    {
        test_container(function () {

            Factory::request()->set('post',
                [
                    'group' => 'login',
                    'setting' => [
                        'effective_hour' => rand(8, 24)
                    ],
                ]
            );

            $profile = new General();
            $response = $profile->saveSetting();
            $this->assertEquals('json', $response->type);
            $this->assertEquals('保存成功', $response->data['msg']);
        }, self::class, __FUNCTION__);
    }
}
<?php


use magein\seecms\Factory;
use magein\seecms\SeeException;
use PHPUnit\Framework\ExpectationFailedException;

$database = [
    'type' => 'mysql',
    'hostname' => '127.0.0.1',
    'database' => 'seecms',
    'username' => 'root',
    'password' => '',
    'hostport' => '3306',
    'params' => [],
    'charset' => 'utf8mb4',
    'prefix' => 'seecms_',
];

\magein\seecms\Factory::config()->setConfig(compact('database'));
\magein\seecms\Factory::db();

function requestUrl($url): string
{
    $domain = 'http://seecms.com/';

    return $domain . trim($url, '/');
}

function post($url, $params = []): ?\GuzzleHttp\Psr7\Response
{
    $http = new \magein\utils\Http();
    $http->formParams(requestUrl($url), $params);
    return $http->getResponse();
}

function get($url): ?\GuzzleHttp\Psr7\Response
{
    $http = new \magein\utils\Http();
    $http->get(requestUrl($url));
    return $http->getResponse();
}

function test_container(callable $callable, ...$params)
{
    try {

        Factory::request()->set('post', []);
        Factory::request()->set('get', []);

        $callable();
    } catch (SeeException $exception) {

        echo $exception->getTraceAsString();

        echo "\n";
        echo "==============================\n";

        echo 'Resource test error' . "\n";

        exit();
    } catch (ExpectationFailedException $exception) {

        throw $exception;

    } catch (\Throwable|\Exception $exception) {

        echo "\n";
        echo "Trace:";
        $traces = $exception->getTrace();
        $traces = array_slice($traces, 0, 4);

        foreach ($traces as $trace) {
            if ($trace['file'] ?? '') {
                echo "\n";
                echo sprintf("  In file %s, line %s,function %s", $trace['file'], $trace['line'], $trace['function']);
            }
        }

        if ($params) {
            echo "\n";
            echo "\n";
            echo "==============================\n";
            foreach ($params as $param) {
                echo $param;
                echo "\n";
            }
        }

        echo "\n";
        echo sprintf("An error occurred in the %s file,in line %s: \n %s", $exception->getFile(), $exception->getLine(), $exception->getMessage());
        exit();
    }
}
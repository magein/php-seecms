<?php

namespace magein\seecms\tests;

use magein\seecms\controller\Permission;
use magein\seecms\Factory;
use PHPUnit\Framework\TestCase;

class MenuTest extends TestCase
{
    public function testIndex()
    {
        test_container(function () {
            $permission = new Permission();
            $response = $permission->menu();
            $this->assertEquals('html', $response->type);
        }, self::class, __FUNCTION__);
    }

    public function testSetMenuSort()
    {
        test_container(function () {

            Factory::request()->set('post', ['id' => 12, 'sort' => rand(900, 999)]);

            $permission = new Permission();
            $response = $permission->setMenuSort();
            $this->assertEquals('json', $response->type);
            $this->assertEquals(0, $response->data['code']);
        }, self::class, __FUNCTION__);
    }

    public function testSetMenuStatus()
    {
        test_container(function () {

            Factory::request()->set('post', ['id' => 12, 'status' => rand(0, 1)]);

            $permission = new Permission();
            $response = $permission->setMenuStatus();
            $this->assertEquals('json', $response->type);
            $this->assertEquals(0, $response->data['code']);
        }, self::class, __FUNCTION__);
    }
}